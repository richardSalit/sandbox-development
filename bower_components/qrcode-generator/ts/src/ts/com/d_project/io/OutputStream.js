'use strict';
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var io;
        (function (io) {
            /**
             * OutputStream
             * @author Kazuhiko Arase
             */
            var OutputStream = (function () {
                function OutputStream() {
                }
                OutputStream.prototype.writeBytes = function (bytes) {
                    for (var i = 0; i < bytes.length; i += 1) {
                        this.writeByte(bytes[i]);
                    }
                };
                OutputStream.prototype.flush = function () {
                };
                OutputStream.prototype.close = function () {
                    this.flush();
                };
                return OutputStream;
            }());
            io.OutputStream = OutputStream;
        })(io = d_project.io || (d_project.io = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=OutputStream.js.map