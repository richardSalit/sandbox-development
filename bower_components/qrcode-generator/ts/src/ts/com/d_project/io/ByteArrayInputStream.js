/// <reference path="InputStream.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var io;
        (function (io) {
            /**
             * ByteArrayInputStream
             * @author Kazuhiko Arase
             */
            var ByteArrayInputStream = (function (_super) {
                __extends(ByteArrayInputStream, _super);
                function ByteArrayInputStream(bytes) {
                    _super.call(this);
                    this.bytes = bytes;
                    this.pos = 0;
                }
                ByteArrayInputStream.prototype.readByte = function () {
                    if (this.pos < this.bytes.length) {
                        var b = this.bytes[this.pos];
                        this.pos += 1;
                        return b;
                    }
                    return -1;
                };
                return ByteArrayInputStream;
            }(io.InputStream));
            io.ByteArrayInputStream = ByteArrayInputStream;
        })(io = d_project.io || (d_project.io = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=ByteArrayInputStream.js.map