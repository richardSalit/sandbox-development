/// <reference path="InputStream.ts"/>
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var io;
        (function (io) {
            /**
             * Base64DecodeInputStream
             * @author Kazuhiko Arase
             */
            var Base64DecodeInputStream = (function (_super) {
                __extends(Base64DecodeInputStream, _super);
                function Base64DecodeInputStream(istream) {
                    _super.call(this);
                    this.istream = istream;
                    this.buffer = 0;
                    this.buflen = 0;
                }
                Base64DecodeInputStream.prototype.readByte = function () {
                    while (this.buflen < 8) {
                        var c = this.istream.readByte();
                        if (c == -1) {
                            if (this.buflen == 0) {
                                return -1;
                            }
                            throw 'unexpected end of file./' + this.buflen;
                        }
                        else if (c == '='.charCodeAt(0)) {
                            this.buflen = 0;
                            return -1;
                        }
                        else if (Base64DecodeInputStream.isWhitespace(c)) {
                            // ignore if whitespace.
                            continue;
                        }
                        this.buffer = (this.buffer << 6) |
                            Base64DecodeInputStream.decode(c);
                        this.buflen += 6;
                    }
                    var n = (this.buffer >>> (this.buflen - 8)) & 0xff;
                    this.buflen -= 8;
                    return n;
                };
                Base64DecodeInputStream.isWhitespace = function (c) {
                    return c == '\v'.charCodeAt(0) ||
                        c == '\t'.charCodeAt(0) ||
                        c == '\r'.charCodeAt(0) ||
                        c == '\n'.charCodeAt(0);
                };
                Base64DecodeInputStream.decode = function (c) {
                    if ('A'.charCodeAt(0) <= c && c <= 'Z'.charCodeAt(0)) {
                        return c - 'A'.charCodeAt(0);
                    }
                    else if ('a'.charCodeAt(0) <= c && c <= 'z'.charCodeAt(0)) {
                        return c - 'a'.charCodeAt(0) + 26;
                    }
                    else if ('0'.charCodeAt(0) <= c && c <= '9'.charCodeAt(0)) {
                        return c - '0'.charCodeAt(0) + 52;
                    }
                    else if (c == '+'.charCodeAt(0)) {
                        return 62;
                    }
                    else if (c == '/'.charCodeAt(0)) {
                        return 63;
                    }
                    else {
                        throw 'c:' + c;
                    }
                };
                return Base64DecodeInputStream;
            }(io.InputStream));
            io.Base64DecodeInputStream = Base64DecodeInputStream;
        })(io = d_project.io || (d_project.io = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=Base64DecodeInputStream.js.map