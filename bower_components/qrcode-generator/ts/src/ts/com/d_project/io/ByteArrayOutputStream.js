/// <reference path="OutputStream.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var io;
        (function (io) {
            /**
             * ByteArrayOutputStream
             * @author Kazuhiko Arase
             */
            var ByteArrayOutputStream = (function (_super) {
                __extends(ByteArrayOutputStream, _super);
                function ByteArrayOutputStream() {
                    _super.call(this);
                    this.bytes = [];
                }
                ByteArrayOutputStream.prototype.writeByte = function (b) {
                    this.bytes.push(b);
                };
                ByteArrayOutputStream.prototype.toByteArray = function () {
                    return this.bytes;
                };
                return ByteArrayOutputStream;
            }(io.OutputStream));
            io.ByteArrayOutputStream = ByteArrayOutputStream;
        })(io = d_project.io || (d_project.io = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=ByteArrayOutputStream.js.map