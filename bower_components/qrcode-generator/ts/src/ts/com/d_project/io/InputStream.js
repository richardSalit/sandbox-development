'use strict';
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var io;
        (function (io) {
            /**
             * InputStream
             * @author Kazuhiko Arase
             */
            var InputStream = (function () {
                function InputStream() {
                }
                InputStream.prototype.close = function () {
                };
                return InputStream;
            }());
            io.InputStream = InputStream;
        })(io = d_project.io || (d_project.io = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=InputStream.js.map