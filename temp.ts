module xyz.dashboard {

    class SomethingPhaseCountController {

        public static $inject = [];

        public title: string;
       
        public chartType: string;
        
        ///// /* @ngInject */
        constructor() {
            this.title = 'Patient Phase Count';
            
            console.log(this.chartType);
            this.$onInit();
        }

        public $onInit(): void {
            console.log(this);
        };

    }

    class SomethingPhaseCount implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                chartType: '@'
            };
            this.controller = SomethingPhaseCountController;
            this.templateUrl = 'app/dashboard/patientPhaseCount/patientPhaseCount.component.html';
        }
    }
}
