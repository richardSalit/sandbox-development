(function () {
    function ShellCtrl($rootScope) {
        var ctrl = this;
        ctrl.title = 'Orthosensor IQ Playground';
        ctrl.name = $rootScope.currentUser;
        ctrl.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
        ctrl.sitePrefix = $rootScope.sitePrefix;
        // console.log($rootScope.sitePrefix);
        ctrl.logoutCall = $rootScope.sitePrefix + '/secur/logout.jsp';
        if ($rootScope.systemMode === undefined || $rootScope.systemMode === null) {
            ctrl.systemMode = 'production';
        }
        else {
            ctrl.systemMode = $rootScope.systemMode;
        }
        ctrl.messages = 2;
    }
    ShellCtrl.$inject = ['$rootScope'];
    angular
        .module('orthosensor')
        .controller('ShellCtrl', ShellCtrl);
})();
