var app;
(function (app) {
    angular
        .module('orthosensor')
        .component('topNav', {
        templateUrl: "app/layout/topnav.html",
        controller: function () {
            var ctrl = this;
            ctrl.title = 'Dashboard';
        },
        //controllerAs: 'vm',
        bindings: {
            title: '@'
        }
    });
})(app || (app = {}));
