(function () {
    angular
        .module('orthosensor')
        .run(runBlock);
    runBlock.$inject = ['$state', '$rootScope', 'InitFactory', '$window', 'UserService', 'DashboardService', 'PatientListFactory', 'HospitalService'];
    function runBlock($state, $rootScope, InitFactory, $window, UserService, DashboardService, PatientListFactory, HospitalService) {
        InitFactory.isUserAdmin()
            .then(function (data) {
            console.log(data);
            $rootScope.isUserAdmin = data;
            UserService.isAdmin = data;
        }, function (error) {
        });
        InitFactory.getCurrentUser()
            .then(function (data) {
            console.log(data);
            $rootScope.currentUser = data;
            var sfUser = data;
            var user = UserService.convertSFToObject(sfUser);
            UserService.user = user;
            //let startPage = 'dashboard';
            var startPage = 'patientList';
            var hospitalPracticeId = '';
            // change if we get user info!
            switch (UserService.user.userType) {
                case 'Surgeon Partner Community User':
                    console.log('surgeon Account');
                    DashboardService.loadHospitalPracticeSurgeons(UserService.user.accountId);
                    console.log(DashboardService.surgeons);
                    DashboardService.loadHospitalByPractice(UserService.user.accountId);
                    startPage = 'patientList';
                    break;
                case 'Hospital Partner Community User':
                    hospitalPracticeId = user.accountId;
                    console.log('hospital Account');
                    DashboardService.loadHospitalPractices(hospitalPracticeId);
                    DashboardService.loadHospitalPracticeSurgeons(UserService.user.accountId);
                    DashboardService.loadHospitalByPractice(UserService.user.accountId);
                    startPage = 'dashboard';
                    break;
                case 'Partner Community User':
                    hospitalPracticeId = user.accountId;
                    console.log('practice Account');
                    DashboardService.loadHospitalPractices(hospitalPracticeId);
                    DashboardService.loadHospitalPracticeSurgeons(UserService.user.accountId);
                    DashboardService.loadHospitalByPractice(UserService.user.accountId);
                    startPage = 'patientList';
                    break;
                default:
                    console.log('Admin Account');
                    DashboardService.loadHospitals();
                    console.log(DashboardService.hospitals);
                    DashboardService.loadPractices(hospitalPracticeId);
                    DashboardService.loadAllSurgeons();
            }
            $state.go(startPage);
        }, function (error) {
        });
        PatientListFactory.loadHospitals()
            .then(function (data) {
            // console.log(data);
            HospitalService.hospitals = data;
        }, function (error) {
            console.log(error);
        });
        PatientListFactory.loadPractices()
            .then(function (data) {
            // console.log(data);
            HospitalService.practices = data;
        }, function (error) {
            console.log(error);
        });
        DashboardService.setSurveys();
        // this assumes that the dashboard will be eventually be the start up module
        // also, we may want to move these to a different service that would be
        // shared for all modules (e.g. DefaultService)
        //move this to a service at some point!
        $rootScope.dateformat = dateformat;
        function dateformat(milliseconds) {
            console.log('- using date format in run js!!! -');
            if (!milliseconds) {
                console.log('Not milliseconds');
                return '--';
            }
            else {
                var dateValue = moment.utc(milliseconds).format('LL');
                console.log(dateValue);
                return dateValue;
            }
        }
    }
})();
//# sourceMappingURL=run.js.map