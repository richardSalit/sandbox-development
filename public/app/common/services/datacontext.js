(function () {
    'use strict';
    // Factory name is handy for logging
    var serviceId = 'datacontext';
    // Define the factory on the module.
    // Inject the dependencies. 
    // Point to the factory definition function.
    angular
        .module('orthosensor.services')
        .factory(serviceId, datacontext);
    function datacontext() {
        // Define the functions and properties to reveal.
        var service = {
            getPatientList: getPatientList,
            getPracticeList: getPracticeList,
            getSurgeonList: getSurgeonList,
            //getPatientActivity: getPatientActivity,
            getPatientActivity1: getPatientActivity1,
            getPatientActivity2: getPatientActivity2,
            getCaseActivityChart: getCaseActivityChart,
            getAllPatientActivity: getAllPatientActivity
        };
        return service;
        function getPatientList() {
            var list = [
                {
                    "id": "109092",
                    "lastName": "Mertz",
                    "firstName": "Fred",
                    "name": "Mertz, Fred"
                },
                {
                    "id": "109093",
                    "lastName": "Ricardo",
                    "firstName": "Lucy",
                    "name": "Ricardo, Lucy"
                },
                {
                    "id": "109094",
                    "lastName": "Ricardo",
                    "firstName": "Ricky",
                    "name": "Ricardo, Ricky"
                },
                {
                    "id": "109095",
                    "lastName": "Masterson",
                    "firstName": "Bat",
                    "name": "Masterson, Bat"
                }
            ];
            return list;
        }
        function getPracticeList() {
            var list = [
                {
                    "id": "0",
                    "practiceName": "All"
                },
                {
                    "id": "109092",
                    "practiceName": "Mertz Orthopedic"
                },
                {
                    "id": "109067",
                    "practiceName": "van Dyke Orthopedic"
                },
                {
                    "id": "109093",
                    "practiceName": "Crosby & Nash Orthopedic"
                },
                {
                    "id": "109094",
                    "practiceName": "Sullivan Systems"
                },
                {
                    "id": "109095",
                    "practiceName": "Podsky Surgical"
                },
                {
                    "id": "109096",
                    "practiceName": "Helium Bormsky"
                },
                {
                    "id": "109098",
                    "practiceName": "Smith Hone and Brown"
                }
            ];
            return list;
        }
        function getSurgeonList() {
            var list = [
                {
                    "id": "0",
                    "surgeonName": "All"
                },
                {
                    "id": "109092",
                    "surgeonName": "Patrick Meere"
                },
                {
                    "id": "109067",
                    "surgeonName": "Martin Roche"
                },
            ];
            return list;
        }
        function getPatientActivity2(practiceId, dateRange) {
            var data = [];
            var practiceName = getPracticeName(practiceId);
            if (practiceName === "All") {
                //data.push(["Pre-op", 123]);
                data.push(["Consultations", 185]);
                data.push(["Scheduled", 125]);
                //data.push(["In post-acute follow-up / bundle (1wk – 90) ", 56]);
                //data.push(["In long term follow up", 265]);
                data.push(["Inactive", 102]);
            }
            else if (practiceName === "Mertz Orthopedic") {
                //data.push(["Pre-op", 10]);
                data.push(["Consultations", 24]);
                data.push(["Scheduled", 30]);
                //data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                //data.push(["In long term follow up", 54]);
                data.push(["Inactive", 9]);
            }
            else if (practiceName === "van Dyke Orthopedic") {
                //data.push(["Pre-op", 45]);
                data.push(["Consultations", 80]);
                data.push(["cheduled", 85]);
                //data.push(["In post-acute follow-up / bundle (1wk – 90) ", 43]);
                //data.push(["In long term follow up", 76]);
                data.push(["Inactive", 14]);
            }
            else if (practiceName === "Crosby, Still & Nash Orthopedic") {
                //data.push(["Pre-op", 10]);
                data.push(["Consultations", 30]);
                data.push(["Scheduled", 45]);
                //data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                //data.push(["In long term follow up", 54]);
                data.push(["Inactive", 9]);
            }
            else {
                //data.push(["Pre-op", 4]);
                data.push(["Consultations", 23]);
                data.push(["Scheduled", 185]);
                //data.push(["In post-acute follow-up / bundle (1wk – 90) ", 6]);
                //data.push(["In long term follow up", 5]);
                data.push(["Inactive", 3]);
            }
            return data;
        }
        function getPatientActivity1(practiceId, dateRange) {
            var data = [];
            var practiceName = getPracticeName(practiceId);
            if (practiceName === "All") {
                data.push(["Pre-op", 123]);
                //data.push(["Booked / Scheduled", 185]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 56]);
                data.push(["In long term follow up", 265]);
            }
            else if (practiceName === "Mertz Orthopedic") {
                data.push(["Pre-op", 10]);
                //data.push(["Booked / Scheduled", 30]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                data.push(["In long term follow up", 54]);
            }
            else if (practiceName === "van Dyke Orthopedic") {
                data.push(["Pre-op", 45]);
                //data.push(["Booked / Scheduled", 20]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 43]);
                data.push(["In long term follow up", 76]);
            }
            else if (practiceName === "Crosby, Still & Nash Orthopedic") {
                data.push(["Pre-op", 10]);
                //data.push(["Booked / Scheduled", 30]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                data.push(["In long term follow up", 54]);
            }
            else {
                data.push(["Pre-op", 4]);
                //data.push(["Booked / Scheduled", 3]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 6]);
                data.push(["In long term follow up", 5]);
            }
            return data;
        }
        function getCaseActivityChart(practiceId, surgeonId, dateRange) {
            var data = [];
            var practiceName = getPracticeName(practiceId);
            if (practiceName === "All") {
                data.push(["Pre-op", 123]);
                //data.push(["Booked / Scheduled", 185]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 56]);
                data.push(["In long term follow up", 265]);
            }
            else if (practiceName === "Mertz Orthopedic") {
                data.push(["Pre-op", 10]);
                //data.push(["Booked / Scheduled", 30]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                data.push(["In long term follow up", 54]);
            }
            else if (practiceName === "van Dyke Orthopedic") {
                data.push(["Pre-op", 45]);
                //data.push(["Booked / Scheduled", 20]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 43]);
                data.push(["In long term follow up", 76]);
            }
            else if (practiceName === "Crosby, Still & Nash Orthopedic") {
                data.push(["Pre-op", 10]);
                //data.push(["Booked / Scheduled", 30]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                data.push(["In long term follow up", 54]);
            }
            else {
                data.push(["Pre-op", 4]);
                //data.push(["Booked / Scheduled", 3]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 6]);
                data.push(["In long term follow up", 5]);
            }
            return data;
        }
        function getPracticeName(practiceId) {
            return "All";
        }

        function getCaseActivityData() {
            return [
                // { Name = Procedure, Year = null, Month = null, Total = 1 },
                // { Name = Procedure, Year = 1943, Month = 5, Total = 2 },
                // { Name = Procedure, Year = 2001, Month = 12, Total = 1 },
                // { Name = Procedure, Year = 2016, Month = 5, Total = 1 },
                // { Name = Procedure, Year = 2016, Month = 7, Total = 1 },
                // { Name = Procedure, Year = 2016, Month = 8, Total = 3 },
                // { Name = Procedure, Year = 2016, Month = 9, Total = 2 },
                // { Name = Procedure, Year = 2016, Month = 10, Total = 3 },
                // { Name = Procedure, Year = 2016, Month = 11, Total = 14 },
                // { Name = Procedure, Year = 2016, Month = 12, Total = 6 }
            ];
        }
            




        // function getPatientActivity1(practiceName, dateRange) {
        //     let data = []
        //     //alert(practiceName);
        //     if (practiceName === "All") {
        //         data = [
        //             { "id": "1", "practiceName": "All", "category": "Pre-op", "count": 123, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "All", "category": "Booked / Scheduled", "count": 185, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "All", "category": "In post-acute follow-up / bundle (1wk – 90)", "count": 320, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "All", "category": "In long term follow up", "count": 265, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "All", "category": "Inactive", "count": 18, "procedureDate": "6/7/2016" },
        //         ];
        //     }
        //     else if (practiceName === "Mertz Orthopedic") {
        //         data = [
        //             { "id": "1", "practiceName": "Mertz Orthopedic", "category": "Pre-op", "count": 12, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Mertz Orthopedic", "category": "Booked / Scheduled", "count": 18, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Mertz Orthopedic", "category": "In post-acute follow-up / bundle (1wk – 90)", "count": 20, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Mertz Orthopedic", "category": "In long term follow up", "count": 65, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Mertz Orthopedic", "category": "Inactive", "count": 15, "procedureDate": "6/7/2016" },
        //         ];
        //     }
        //     else if (practiceName === "van Dyke Orthopedic") {
        //         data = [
        //             { "id": "1", "practiceName": "van Dyke Orthopedic", "category": "Pre-op", "count": 3, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "van Dyke Orthopedic", "category": "Booked / Scheduled", "count": 8, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "van Dyke Orthopedic", "category": "In post-acute follow-up / bundle (1wk – 90)", "count": 10, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "van Dyke Orthopedic", "category": "In long term follow up", "count": 5, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "van Dyke Orthopedic", "category": "Inactive", "count": 1, "procedureDate": "6/7/2016" },
        //         ];
        //     }
        //     else if (practiceName === "Crosby, Still & Nash Orthopedic") {
        //         data = [
        //             { "id": "1", "practiceName": "Crosby, Still & Nash Orthopedic", "category": "Pre-op", "count": 15, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Crosby, Still & Nash Orthopedic", "category": "Booked / Scheduled", "count": 12, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Crosby, Still & Nash Orthopedic", "category": "In post-acute follow-up / bundle (1wk – 90)", "count": 10, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Crosby, Still & Nash Orthopedic", "category": "In long term follow up", "count": 6, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Crosby, Still & Nash Orthopedic", "category": "Inactive", "count": 5, "procedureDate": "6/7/2016" },
        //         ];
        //     }
        //     else {
        //         data = [
        //             { "id": "1", "practiceName": "Joe Shmoe Podiatry", "category": "Pre-op", "count": 1, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Joe Shmoe Podiatry", "category": "Booked / Scheduled", "count": 1, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Joe Shmoe Podiatry", "category": "In post-acute follow-up / bundle (1wk – 90)", "count": 2, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Joe Shmoe Podiatry", "category": "In long term follow up", "count": 15, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Joe Shmoe Podiatry", "category": "Inactive", "count": 1, "procedureDate": "6/7/2016" },
        //         ];
        //     }
        //     return data;
        // }
        // function getPatientActivity2(practiceId, dateRange) {
        //     let data = [];
        //     let allData = getAllPatientActivity();
        //     let practiceData = filterByPractice(practiceId, allData);
        //     //let filteredDateData = filterByDate(dateRange, practiceData);
        //     let aggregatedData = aggregateData(practiceData);
        //     return practiceData;
        // }
        function aggregateData(filteredData) {
            // data.push(["Pre-op", 123]);
            //     data.push(["Booked / Scheduled", 185]);
            //     data.push(["In post-acute follow-up / bundle (1wk – 90) ", 56]);
            //     data.push(["In long term follow up", 265]);
            //     data.push(["Inactive", 165]);
            var preop = 0;
            var booked = 0;
            var postAcuteFollowUp = 0;
            var longTermFollowUp = 0;
            var inactive = 0; //how do we determine???
            for (var i = 0; i < filteredData.length; i++) {
                // need to make sure the procedure date is a date!
                if (filteredData[i].procedureDate < getDate()) {
                    preop++;
                }
                if (filteredData[i].procedureDate < getDate()) {
                    booked++;
                }
            }
        }
        function filterByDate(dateRange, practiceData) {
            var data = [];
            for (var i = 0; i < practiceData.length; i++) {
                if (dateRange === "1 week") {
                    data.push(practiceData[i]);
                }
                else {
                    data.push(practiceData[i]);
                }
            }
            return data;
        }
        function filterByPractice(practiceId, allData) {
            if (practiceId === "0") {
                return allData;
            }
            else {
                var practiceData = [];
                for (var i = 0; i < allData.length; i++) {
                    if (allData[i].practiceId === practiceId) {
                        practiceData.push(allData[i]);
                    }
                }
                return practiceData;
            }
        }
        function getAllPatientActivity() {
            var data = [
                {
                    "id": "1",
                    "practiceId": "109092",
                    "patientLastName": "Smith",
                    "patientFirst": "James",
                    "procedureDate": "6/7/2016"
                },
                {
                    "id": "2",
                    "practiceId": "109067",
                    "patientLastName": "Smith",
                    "patientFirst": "James",
                    "procedureDate": "4/7/2016"
                },
                {
                    "id": "3",
                    "practiceId": "109092",
                    "patientLastName": "Jones",
                    "patientFirst": "James",
                    "procedureDate": "11/7/2016"
                },
            ];
            return data;
        }
    }
})();
