 angular.module('app')
.factory('contactService', function($http: any, $q: any) {
    return {
        getContactDetails: function(quoteId: string) {
            var soql = 'SELECT Name FROM Contact Limit 1';
            var url = 'http://cs18.salesforce.com/services/data/v30.0/query?q=' + encodeURIComponent(soql);
            var sessionId = '00D1100000BzLId!ASAAQPOKAZvD05Q1Jy71YcLdkK0Y_tGwK1bqTUcFfYLS8THKJ5DJtC2VXmrswtBWvjjp';
            var deferred = $q.defer();
            $http({url:url,method:'GET',headers:{'Authorization': 'OAuth ' + sessionId}}).success(function(data: any, status: any) {
                deferred.resolve(data);
            }).error(function(err:string, status:any) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;        
        }
    };
});
