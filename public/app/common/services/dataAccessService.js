(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('DataAccessService', DataAccessService);
    //Service.$inject = ['dependency1'];
    function DataAccessService() {
        service = {
            getHospitals: getHospitals,
            getPatientActivity: getPatientActivity
        };
        return service;
        function getHospitals() {
            var hospitals = [
                { "id": "9090", "name": "Albert Einstein Hospital" },
                { "id": "9091", "name": "University Hospital" },
                { "id": "9092", "name": "Holy Cross Hospital" },
                { "id": "9093", "name": "NYU Hospital" },
                { "id": "9094", "name": "Biggets one Hospital" },
                { "id": "9095", "name": "Mayo Clinic" },
            ];
            return hospitals;
        }
        ;
    }
})();
