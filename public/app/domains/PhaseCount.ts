module orthosensor.domains {
   
    export class PhaseCount {
        public id: string;
        public surgeonId: string;
        public phase: string;
        public startDate: Date;
        public startDateString: String;
        public count: number;
        public practiceId: string;
        constructor(
        ) { }
    }
}
