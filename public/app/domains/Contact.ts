module orthosensor.domains {
    export interface IContact {
        Id: string,
        Name: string,
        AccountId: string;
        // Contact.Account.RecordType.Name: string;
        // , Contact.Account.ParentId, Profile.Name
    }
    export class Contact implements IContact {
        constructor(
            public Id: string,
            public Name: string,
            public AccountId: string) {

        }
    }
}    
