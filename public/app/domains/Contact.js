var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Contact = (function () {
            function Contact(Id, Name, AccountId) {
                this.Id = Id;
                this.Name = Name;
                this.AccountId = AccountId;
            }
            return Contact;
        }());
        domains.Contact = Contact;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Contact.js.map