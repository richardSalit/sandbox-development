module orthosensor.domains {
    export interface IPhysician {
        id: string,
        name: string
    }
    export class Physician implements IPhysician {
        constructor(
            public id: string,
            public name: string
        ) {

        }
    }
}    
