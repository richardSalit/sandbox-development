var orthosensor;
(function (orthosensor) {
    var domain;
    (function (domain) {
        var Account = (function () {
            function Account(id, name, recordTypeName, parentId) {
                this.id = id;
                this.name = name;
                this.recordTypeName = recordTypeName;
                this.parentId = parentId;
            }
            return Account;
        }());
        domain.Account = Account;
    })(domain = orthosensor.domain || (orthosensor.domain = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Account.js.map