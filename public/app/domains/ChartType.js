var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var ChartType = (function () {
            function ChartType() {
            }
            return ChartType;
        }());
        domains.ChartType = ChartType;
        var ChartOptions = (function () {
            function ChartOptions() {
            }
            return ChartOptions;
        }());
        domains.ChartOptions = ChartOptions;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=ChartType.js.map