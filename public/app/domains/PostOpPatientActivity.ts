module orthosensor.domain {
    export interface IPostOpPatientActivity {
        id: string,
        lastName: string,
        firstName: string,
        days: number,
        climbed: number,
        walked: number,
        last: Date,
        rangeOfMotion: number,
        postOperativeStatus: string,
        ptEconomic: number
    }
        export class PostOpPatientActivity implements IPostOpPatientActivity {
            constructor(
                public id: string,
                public lastName: string,
                public firstName: string,
                public days: number,
                public climbed: number,
                public walked: number,
                public last: Date,
                public rangeOfMotion: number,
                public postOperativeStatus: string,
                public ptEconomic: number
            ) {
            }
        }
    }
