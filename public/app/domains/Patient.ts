module orthosensor.domains {
    export class Patient {
        id?: string;
        anonymous?: Boolean;
        hl7?: Boolean;
        hospitalId?: string;
        hospital?: any;
        lastName?: string;
        firstName?: string;
        anonymousLabel?: string;
        practiceId?: string;
        practice?: any;
        label?: string;
        medicalRecordNumber?: string;
        email?: string;
        dateofBirth?: Date;
        dateofBirthString?: string;
        birthYear?: string;
        gender?: string;
        socialSecurityNumber?: string;
        patientNumber?: string;
        accountNumber?: string;
        race?: string;
        language?: string;

        constructor(

        ) {

        }
    }

}