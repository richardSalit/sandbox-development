module orthosensor.domains {
    // import { IContact } from './contact';

    export class User {
        public id?: string;
        public name?: string;
        //public lastName: string;
        //public firstName: string;
        public contactId?: string;
        // public contact: orthosensor.domains.IContact;
        public accountName?: string;
        public accountId?: string;
        public recordType?: string;
        public userType?: string;
        public hospitalId?: string;
        public hospitalName?: string;
                
        public anonymousPatients?: boolean;
        
        constructor() {}
    }
}
