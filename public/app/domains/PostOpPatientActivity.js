var orthosensor;
(function (orthosensor) {
    var domain;
    (function (domain) {
        var PostOpPatientActivity = (function () {
            function PostOpPatientActivity(id, lastName, firstName, days, climbed, walked, last, rangeOfMotion, postOperativeStatus, ptEconomic) {
                this.id = id;
                this.lastName = lastName;
                this.firstName = firstName;
                this.days = days;
                this.climbed = climbed;
                this.walked = walked;
                this.last = last;
                this.rangeOfMotion = rangeOfMotion;
                this.postOperativeStatus = postOperativeStatus;
                this.ptEconomic = ptEconomic;
            }
            return PostOpPatientActivity;
        }());
        domain.PostOpPatientActivity = PostOpPatientActivity;
    })(domain = orthosensor.domain || (orthosensor.domain = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=PostOpPatientActivity.js.map