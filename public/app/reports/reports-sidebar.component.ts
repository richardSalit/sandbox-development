module orthosensor.reports {

    class ReportsSidebarController  {

        public static $inject = ['config', 'UserService'];

        public title: string;
        public user: orthosensor.domains.User;
       
        private UserService: any;


        ///// /* @ngInject */
        // tslint:disable-next-line:max-line-length
        constructor(config: any, UserService: any) {
            
            // this.PatientService = PatientListFactory;
            this.title = 'PROM score deltas';
            
          
            this.user = UserService.user;

           
            // console.log(this.user);
            
            // console.log(config);
            this.$onInit();
        }

        public $onInit(): void {
           
        };

        
    }

    angular
        .module('orthosensor')
        .component('osReportsSidebar', {
            controller: ReportsSidebarController,
            controllerAs: '$ctrl',
            templateUrl: 'app/reports/reports-sidebar.component.html',
            bindings: {
              
            }
        });
}

