(function () {
    angular
        .module('orthosensor')
        .component('doctorList', {
        templateUrl: 'app/components/doctorList/doctorList.component.html'
    });
})();
