(function () {
    angular.module('orthosensor', [
        'app',
        'app.core',
        'angular-confirm',
        'orthosensor.services',
        'orthosensor.data',
        'orthosensor.dashboard',
        'orthosensor.admin',
    ]);
    angular.module('app.common', []);
    angular.module('orthosensor.services', [
        'app.core'
    ]);
    angular.module('orthosensor.dashboard', [
        'app.core',
        'orthosensor.services'
    ]);
    angular.module('orthosensor.data', []);
    angular.module('orthosensor.admin', [
        'app.core',
        'orthosensor.data',
        'orthosensor.services'
    ]);
})();
