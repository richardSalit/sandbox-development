
angular
    .module('app.core')
    .component('pageHeader', {
        template: '<h2>{{vm.title}}</h2>',
        bindings: {
            title: '@'
        },
        controller: function () {
            var vm: any = this;
            //vm.title = this.title;
        },
        controllerAs: 'vm'
    });
