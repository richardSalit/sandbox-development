(function () {
    'use strict';
    angular
        .module('orthosensor')
        .controller('AddPatient', AddPatient);
    AddPatient.$inject = ['$rootScope', '$state', '$timeout', '$uibModalInstance', 'PatientListFactory', 'PatientService', 'DashboardService'];
    function AddPatient($rootScope, $state, $timeout, $uibModalInstance, PatientListFactory, PatientService, DashboardService) {
        var $ctrl = this;
        $ctrl.title = 'New Patient';
        $ctrl.patient = [];
        $ctrl.patient.anonymous = $rootScope.currentUser.Contact ? $rootScope.currentUser.Contact.Account.Anonymous_Patients__c : false;
        $ctrl.patient.hl7 = false;
        $ctrl.patient.HospitalId = $rootScope.currentUser.Contact ? $rootScope.currentUser.Contact.AccountId : null; // For Community Users
        $ctrl.patient.hospital = ''; //For Internal Users only - required
        $ctrl.patient.practice = ''; //For Internal Users only - optional
        $ctrl.patient.LastName = '';
        $ctrl.patient.FirstName = '';
        $ctrl.patient.Label = '';
        $ctrl.patient.MedicalRecordNumber = '';
        $ctrl.dateofBirthString = '';
        $ctrl.patient.BirthYear = '';
        $ctrl.patient.Gender = '';
        $ctrl.patient.SocialSecurityNumber = '';
        $ctrl.patient.PatientNumber = '';
        $ctrl.patient.AccountNumber = '';
        $ctrl.patient.Race = '';
        $ctrl.patient.Language = '';
        //methods        
        $ctrl.createNewPatient = createNewPatient;
        $ctrl.open = open;
        $ctrl.cancel = cancel;
        $ctrl.savePatient = savePatient;
        $ctrl.checkAnonymous = checkAnonymous;
        $ctrl.resetPractice = resetPractice;

        activate();

        function activate() {
            $ctrl.hospitals = DashboardService.hospitals;
            $ctrl.practices = DashboardService.practices;
            console.log($ctrl.hospitals);
            console.log($ctrl.practices);

            $ctrl.dateOptions = {
                formatYear: 'yy',
                startingDay: 1,
                showWeeks: false
            };
            $ctrl.birthYears = [];
            for (var i = new Date().getFullYear(); i > 1900; i--) {
                $ctrl.birthYears.push(i);
            }
        }

        //$ctrl.open = { birthDate: false };
        function open($event, whichDate) {
            //$event.preventDefault();
            //$event.stopPropagation();
            $ctrl.open[whichDate] = true;
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        function resetPractice() {
            $ctrl.patient.practice = undefined;
        }

        function checkAnonymous() {
            var accountId = $ctrl.patient.hospital;
            console.log($ctrl.patient.hospital);
            console.log($ctrl.patient.hl7);
            console.log($ctrl.patient);
            if ($ctrl.patient.practice !== undefined && $ctrl.patient.practice.length > 0) {
                accountId = $ctrl.patient.practice;
            }
            PatientListFactory.checkAnonymous(accountId).then(function (data) {
                //console.log(data);
                $ctrl.patient.anonymous = data;
            }, function (error) {
                console.log(error);
            });
        }

        function createNewPatient(pageForm) {
            if (pageForm.$valid) {

                $ctrl.loadingPatient = true;
                $timeout(function () {
                    PatientListFactory.createPatient($ctrl.patient).then(function (data) {
                        $uibModalInstance.dismiss();
                        //$rootScope.patient = data;
                        PatientService.setPatient(data);
                        console.log($ctrl);
                        $state.go('patientDetails');
                        $ctrl.loading = false;
                    }, function (error) {
                        console.log(error);
                    });

                }, 1000);
            }
            else {

                pageForm.submitted = true;
            }
            //$ctrl.loading = false;
        }

        function savePatient(pageForm) {
            if (pageForm.$valid) {
                $ctrl.loadingPatient = true;
                console.log($ctrl.patient);
                $timeout(function () {
                    PatientListFactory.checkDuplicate($ctrl.patient)
                        .then(function (data) {
                            //console.log(data); 
                            if (data != null) {
                                $ctrl.matchedPatient = data;

                            }
                            else {
                                PatientListFactory.createPatient($ctrl.patient).then(function (data) {
                                    //console.log(data);               
                                    $uibModalInstance.dismiss();
                                    PatientService.setPatient(data);
                                    $state.go('patientDetails');
                                }, function (error) {
                                    console.log(error);
                                });
                            }
                        }, function (error) {
                            console.log(error);
                        });

                }, 1000);
            }
            else {
                $ctrl.pageForm.submitted = true;
            }
        }
        $ctrl.createNewCase = function (matchdPatient) {
            $uibModalInstance.dismiss();
            $rootScope.addPatientCase(matchdPatient);
        };
    }
})();
