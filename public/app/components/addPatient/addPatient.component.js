var orthosensor;
(function (orthosensor) {
    var AddPatientController = (function () {
        function AddPatientController($rootScope, $state, $timeout, UserService, PatientListFactory, PatientService, PatientDataService, HospitalService, PatientDetailsFactory, moment) {
            this.$rootScope = $rootScope;
            this.$state = $state;
            this.$timeout = $timeout;
            this.UserService = UserService;
            this.PatientService = PatientService;
            this.PatientDataService = PatientDataService;
            this.HospitalService = HospitalService;
            this.PatientDetailsFactory = PatientDetailsFactory;
            this.moment = moment;
            this.title = 'New Patient';
            this.user = UserService.user;
            this.PatientListFactory = PatientListFactory;
        }
        AddPatientController.prototype.$onInit = function () {
            this.user = this.UserService.user;
            console.log(this.user);
            this.isUserAdmin = this.UserService.isAdmin;
            this.hospitals = this.HospitalService.hospitals;
            this.practices = this.HospitalService.practices;
            this.languages = this.PatientDataService.getLanguages();
            this.races = this.PatientDataService.getRaces();
            console.log(this.hospitals);
            console.log(this.practices);
            console.log(this.PatientService.patient);
            if (this.PatientService.patient === null) {
                this.createPatient();
                this.mode = 'Create';
            }
            else {
                this.patient = this.PatientService.patient;
                this.mode = this.patient.hl7 === true ? 'Create' : 'Edit';
                console.log(this.mode);
                var dob = new Date(this.patient.dateofBirth);
                var dob_utc = new Date(dob.getUTCFullYear(), dob.getUTCMonth(), dob.getUTCDate(), dob.getUTCHours(), dob.getUTCMinutes(), dob.getUTCSeconds());
                console.log(dob_utc);
                this.patient.dateofBirthString = dob_utc;
                var maxDate = new Date();
                this.maxBirthDate = new Date(maxDate.getUTCFullYear(), maxDate.getUTCMonth(), maxDate.getUTCDate(), maxDate.getUTCHours(), maxDate.getUTCMinutes(), maxDate.getUTCSeconds());
                console.log(dob_utc);
                //this.patient.dateofBirthString = new Date(this.patient.dateofBirth);
                //this.patient.dateofBirthString = new Date();;
                console.log(this.patient);
                this.title = 'Edit Patient';
                // this.mode = 'Edit';
            }
            var today = new Date();
            var startYear = this.moment().subtract(65, 'year').year();
            var month = this.moment(today).month();
            var day = this.moment(today).day();
            console.log(startYear + ',' + month + ',' + day);
            this.dateOptions = {
                formatYear: 'yy',
                startingDay: 1,
                showWeeks: false,
                initDate: new Date(startYear, month, day),
                maxDate: new Date(),
                minDate: new Date(1920, 1, 1)
            };
            // this.patient.dateofBirthString = new Date(1945, 8, 1);
            this.birthYears = [];
            for (var i = new Date().getFullYear(); i > 1920; i--) {
                this.birthYears.push(i);
            }
        };
        //this.open = { birthDate: false };
        AddPatientController.prototype.open = function ($event, whichDate) {
            $event.preventDefault();
            $event.stopPropagation();
            this.open[whichDate] = true;
        };
        AddPatientController.prototype.cancel = function () {
            this.$state.go('patientList');
        };
        AddPatientController.prototype.resetPractice = function () {
            this.patient.practice = undefined;
        };
        AddPatientController.prototype.checkAnonymous = function () {
            var _this = this;
            var accountId = this.patient.hospital;
            console.log(this.patient.hospital);
            console.log(this.patient.hl7);
            console.log(this.patient);
            if (this.patient.practice !== undefined && this.patient.practice.length > 0) {
                accountId = this.patient.practice;
            }
            this.PatientService.checkAnonymous(accountId)
                .then(function (data) {
                //console.log(data);
                _this.patient.anonymous = data;
            }, function (error) {
                console.log(error);
            });
        };
        AddPatientController.prototype.createPatient = function () {
            this.patient = new orthosensor.domains.Patient();
            this.patient.anonymous = this.UserService.isUserAdmin ? this.user.anonymousPatients : false;
            this.patient.hl7 = false;
            this.patient.hospitalId = this.UserService.isUserAdmin ? this.user.accountId : null;
            this.patient.hospital = ''; //For Internal Users only - required
            this.patient.practice = ''; //For Internal Users only - optional
            this.patient.lastName = '';
            this.patient.firstName = '';
            this.patient.label = '';
            this.patient.medicalRecordNumber = '';
            this.patient.email = '';
            this.patient.dateofBirthString = '';
            this.patient.birthYear = null;
            this.patient.gender = '';
            this.patient.socialSecurityNumber = '';
            this.patient.patientNumber = '';
            this.patient.accountNumber = '';
            this.patient.race = '';
            this.patient.language = '';
        };
        AddPatientController.prototype.createNewPatient = function (pageForm) {
            if (pageForm.$valid) {
                this.loadingPatient = true;
                this.$timeout(function () {
                    var _this = this;
                    this.PatientDataService.createPatient(this.patient)
                        .then(function (data) {
                        console.log(data);
                        _this.PatientService.patient = data;
                        _this.$state.go('patientDetails');
                        _this.loading = false;
                    }, function (error) {
                        console.log(error);
                    });
                }, 1000);
            }
            else {
                pageForm.submitted = true;
            }
            //this.loading = false;
        };
        AddPatientController.prototype.savePatientOnly = function (pageForm) {
            this.savePatient(pageForm, 'patientList');
            this.$state.go('patientList');
        };
        AddPatientController.prototype.savePatientAndAddCase = function (pageForm) {
            this.savePatient(pageForm, 'addPatientCase');
            this.$state.go('addPatientCase');
        };
        AddPatientController.prototype.savePatient = function (pageForm, nextForm) {
            var _this = this;
            console.log(this.mode);
            if (pageForm.$valid) {
                this.loadingPatient = true;
                if (!this.isUserAdmin) {
                    this.patient.hospitalId = this.user.accountId;
                }
                else {
                    console.log(this.patient.practice);
                    this.patient.hospitalId = this.patient.practice;
                }
                console.log(this.patient);
                this.PatientService.patient = this.patient;
                if (this.mode === 'Create') {
                    this.PatientDataService.checkDuplicate(this.patient)
                        .then(function (data) {
                        //console.log(data); 
                        if (data != null) {
                            _this.matchedPatient = data;
                        }
                        else {
                            _this.PatientDataService.createPatient(_this.patient)
                                .then(function (data) {
                                console.log(data);
                                _this.PatientService.patient.id = data.Id;
                                console.log(_this.PatientService.patient);
                                _this.$state.go(nextForm);
                            }, function (error) {
                                console.log(error);
                            });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
                else {
                    // let sfPatient = this.PatientService.getSFObject();
                    console.log(this.patient);
                    this.PatientDataService.updatePatient(this.patient)
                        .then(function (data) {
                        console.log(data);
                        // this.PatientService.patient = data;
                        // $rootScope.patient = data;
                        console.log(data);
                        //PatientService.setPatient(data);
                        _this.$state.go(nextForm);
                    }, function (error) {
                        console.log(error);
                    });
                }
            }
            else {
                pageForm.submitted = true;
            }
        };
        return AddPatientController;
    }());
    AddPatientController.$inject = ['$rootScope', '$state', '$timeout', 'UserService', 'PatientListFactory', 'PatientService', 'PatientDataService', 'HospitalService', 'PatientDetailsFactory', 'moment'];
    var AddPatient = (function () {
        function AddPatient() {
            this.bindings = {};
            this.controller = AddPatientController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/addPatient/addPatient.component.html';
        }
        return AddPatient;
    }());
    angular
        .module('orthosensor')
        .component('osAddPatient', new AddPatient());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=addPatient.component.js.map