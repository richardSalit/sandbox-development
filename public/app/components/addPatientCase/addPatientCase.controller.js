(function () {
    'use strict';

    function AddPatientCase($rootScope, $state, $uibModalInstance, PatientListFactory, PatientService, CaseService, $timeout) {
        var $ctrl = this;
        $ctrl.title = 'New Case';
        //$ctrl.patient = patient;
        $ctrl.createCase = createCase;
        $ctrl.open = open;
        $ctrl.cancel = cancel;

        activate();

        function activate() {
            $ctrl.patient = PatientService.getPatient();
            console.log($ctrl.patient);
            loadCaseTypes();
            loadSurgeons();
            $ctrl.loadingCase = false; //test to see if this helps!
        }

        function loadCaseTypes() {
            PatientListFactory.loadCaseTypes($ctrl.patient.Hospital__c).then(function (data) {
                // console.log(data);
                $ctrl.caseTypes = data;
            }, function (error) {
                console.log(error);
            });
        }

        function loadSurgeons() {
            // PatientListFactory.loadSurgeons(angular.isDefined($ctrl.patient.Hospital__r.ParentId) ? $ctrl.patient.Hospital__r.ParentId : $ctrl.patient.Hospital__c).then(function (data) {
            PatientListFactory.loadSurgeons($ctrl.patient.Hospital__c).then(function (data) {
                console.log(data);
                $ctrl.surgeons = data;
            }, function (error) {
                console.log(error);
            });
        }

        // $ctrl.open = { procDate: false };

        function open($event, whichDate) {
            //$event.preventDefault();
            //$event.stopPropagation();
            $ctrl.open[whichDate] = true;
        }

        $ctrl.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: false
        };

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        function createCase(pageForm) {
            // console.log($ctrl.patient);
            // console.log($ctrl.patientCase);
            if (pageForm.$valid) {
                $ctrl.loadingCase = true;
                
                $timeout(function () {
                    if ($ctrl.patientCase.Laterality === 'Both') {
                        $ctrl.patientCase.Laterality = 'Left';
                        PatientListFactory.createCase($ctrl.patient, $ctrl.patientCase)
                            .then(function (data) {
                                $ctrl.patientCase.Laterality = 'Right';
                                PatientListFactory.createCase($ctrl.patient, $ctrl.patientCase)
                                    .then(function (data) {
                                        $uibModalInstance.close('OK');
                                        $rootScope.case = data;
                                        CaseService.setCurrentCase(data);
                                        $state.go('caseDetails');
                                    }, function (error) {
                                        console.log(error);
                                    });

                            }, function (error) {
                                console.log(error);
                            });
                    } else {
                        PatientListFactory.createCase($ctrl.patient, $ctrl.patientCase)
                            .then(function (data) {
                                //console.log(data);              
                                $uibModalInstance.close('OK');
                                $rootScope.case = data;
                                $state.go('caseDetails');
                            }, function (error) {
                                console.log(error);
                            });
                    }
                    // $ctrl.loadingCase = false;
                }, 1000);
            }
            else {

                pageForm.submitted = true;
            }
            // $ctrl.loadingCase = false;
            // Ladda.stopAll();
        }
        
    }
    angular
        .module('orthosensor')
        .controller('AddPatientCase', AddPatientCase);

    AddPatientCase.$inject = ['$rootScope', '$state', '$uibModalInstance', 'PatientListFactory', 'PatientService', 'CaseService', '$timeout'];
})();
