var orthosensor;
(function (orthosensor) {
    var AddPatientCaseController = (function () {
        function AddPatientCaseController($rootScope, $state, UserService, PatientListFactory, PatientService, CaseService, $timeout) {
            this.$rootScope = $rootScope;
            this.$state = $state;
            this.UserService = UserService;
            this.PatientListFactory = PatientListFactory;
            this.PatientService = PatientService;
            this.CaseService = CaseService;
            this.$timeout = $timeout;
            this.title = 'New Patient Case';
            //this.patient = patient;
            this.dateOptions = {
                formatYear: 'yy',
                startingDay: 1,
                showWeeks: false
            };
            this.multipleCaseTypes = false;
        }
        AddPatientCaseController.prototype.$onInit = function () {
            this.UserService = this.UserService;
            this.user = this.UserService.user;
            this.patient = this.PatientService.patient;
            console.log(this.patient);
            var today = new Date();
            var today_utc = new Date(today.getUTCFullYear(), today.getUTCMonth(), today.getUTCDate(), today.getUTCHours(), today.getUTCMinutes(), today.getUTCSeconds());
            console.log(today_utc);
            //this.patient.dateofBirthString = dob_utc;
            this.patientCase = {
                CaseTypeId: '',
                ProcedureDateString: today_utc,
                laterality: 'Left',
                SurgeonID: ''
            };
            console.log(this.patientCase.ProcedureDateString);
            if (this.patient) {
                this.loadCaseTypes();
                this.loadSurgeons();
                this.loadingCase = false; //test to see if this helps!
            }
        };
        AddPatientCaseController.prototype.$onChanges = function (patient) {
            console.log(patient);
            console.log(this.patient);
            if (this.patient) {
                this.loadCaseTypes();
                this.loadSurgeons();
                this.loadingCase = false; //test to see if this helps!
            }
        };
        AddPatientCaseController.prototype.loadCaseTypes = function () {
            var _this = this;
            this.PatientListFactory.loadCaseTypes(this.patient.hospitalId)
                .then(function (data) {
                console.log(data);
                _this.caseTypes = data;
                _this.multipleCaseTypes = _this.caseTypes.length > 1;
                if (_this.caseTypes.length == 1) {
                    _this.caseTypeDescription = _this.caseTypes[0].Case_Type__r.Description__c;
                    _this.patientCase.CaseTypeId = _this.caseTypes[0].Case_Type__c;
                }
            }, function (error) {
                console.log(error);
            });
        };
        AddPatientCaseController.prototype.loadSurgeons = function () {
            var _this = this;
            // PatientListFactory.loadSurgeons(angular.isDefined(this.patient.Hospital__r.ParentId) ? this.patient.Hospital__r.ParentId : this.patient.Hospital__c).then(function (data) {
            this.PatientListFactory.loadSurgeons(this.patient.hospitalId)
                .then(function (data) {
                console.log(data);
                _this.surgeons = data;
            }, function (error) {
                console.log(error);
            });
        };
        // this.open = { procDate: false };
        AddPatientCaseController.prototype.open = function ($event, whichDate) {
            //$event.preventDefault();
            //$event.stopPropagation();
            this.open[whichDate] = true;
        };
        AddPatientCaseController.prototype.cancel = function () {
            // this.$uibModalInstance.dismiss('cancel');
        };
        AddPatientCaseController.prototype.createCase = function (pageForm) {
            var _this = this;
            console.log(this.patient);
            console.log(this.patientCase);
            if (pageForm.$valid) {
                this.loadingCase = true;
                this.$timeout(function () {
                    if (_this.patientCase.laterality === 'Both') {
                        _this.patientCase.laterality = 'Left';
                        _this.PatientListFactory.createCase(_this.patient, _this.patientCase)
                            .then(function (data) {
                            _this.patientCase.laterality = 'Right';
                            _this.PatientListFactory.createCase(_this.patient, _this.patientCase)
                                .then(function (data) {
                                _this.$rootScope.case = data;
                                _this.CaseService.setCurrentCase(data);
                                _this.$state.go('caseDetails');
                            }, function (error) {
                                console.log(error);
                            });
                        }, function (error) {
                            console.log(error);
                        });
                    }
                    else {
                        _this.PatientListFactory.createCase(_this.patient, _this.patientCase)
                            .then(function (data) {
                            // console.log(data);              
                            // this.$uibModalInstance.close('OK');
                            _this.$rootScope.case = data;
                            _this.$state.go('caseDetails');
                        }, function (error) {
                            console.log(error);
                        });
                    }
                    // this.loadingCase = false;
                }, 1000);
            }
            else {
                pageForm.submitted = true;
            }
            // this.loadingCase = false;
            // Ladda.stopAll();
        };
        return AddPatientCaseController;
    }());
    AddPatientCaseController.$inject = ['$rootScope', '$state', 'UserService', 'PatientListFactory', 'PatientService', 'CaseService', '$timeout'];
    var AddPatientCase = (function () {
        function AddPatientCase() {
            this.bindings = {
                patient: '<',
            };
            this.controller = AddPatientCaseController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/addPatientCase/addPatientCase.component.html';
        }
        return AddPatientCase;
    }());
    angular
        .module('orthosensor')
        .component('osAddPatientCase', new AddPatientCase());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=addPatientCase.component.js.map