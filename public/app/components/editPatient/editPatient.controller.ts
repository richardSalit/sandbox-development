(function () {
    'use strict';
    angular
        .module('orthosensor')
        .controller('editPatientController', editPatientController);
    editPatientController.$inject = ['$scope', '$rootScope', '$state', '$uibModalInstance', 'PatientDetailsFactory', 'PatientService'];
    function editPatientController($scope, $rootScope, $state, $uibModalInstance, PatientDetailsFactory, PatientService) {
        var patient = PatientService.getSFObject();
        console.log(patient);
        var dob = new Date(patient.Date_Of_Birth__c);
        var dob_utc = new Date(dob.getUTCFullYear(), dob.getUTCMonth(), dob.getUTCDate(), dob.getUTCHours(), dob.getUTCMinutes(), dob.getUTCSeconds());
        $scope.patient = [];
        $scope.patient.Id = patient.Id;
        $scope.patient.anonymous = patient.Anonymous__c;
        $scope.patient.Label = patient.Anonymous_Label__c ? patient.Anonymous_Label__c : '';
        $scope.patient.BirthYear = patient.Anonymous_Year_of_Birth__c ? patient.Anonymous_Year_of_Birth__c : '';
        $scope.patient.LastName = patient.Last_Name__c ? patient.Last_Name__c : '';
        $scope.patient.FirstName = patient.First_Name__c ? patient.First_Name__c : '';
        $scope.patient.MedicalRecordNumber = patient.Medical_Record_Number__c ? patient.Medical_Record_Number__c : '';
        $scope.patient.DateofBirthString = dob_utc;
        $scope.patient.Gender = patient.Gender__c ? patient.Gender__c : '';
        $scope.patient.SocialSecurityNumber = patient.SSN__c ? patient.SSN__c : '';
        $scope.patient.PatientNumber = patient.Source_Record_Id__c ? patient.Source_Record_Id__c : '';
        $scope.patient.AccountNumber = patient.Account_Number__c ? patient.Account_Number__c : '';
        $scope.patient.Race = patient.Race__c ? patient.Race__c : '';
        $scope.patient.Language = patient.Language__c ? patient.Language__c : '';
        $scope.open = { birthDate: false };
        $scope.open = function ($event, whichDate) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.open[whichDate] = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: false
        };
        $scope.birthYears = [];
        for (var i = new Date().getFullYear(); i > 1900; i--) {
            $scope.birthYears.push(i);
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.updatePatient = function () {
            if ($scope.pageForm.$valid) {
                PatientDetailsFactory.UpdatePatient($scope.patient).then(function (data) {
                    console.log(data);
                    $uibModalInstance.dismiss();
                    PatientService.patient = data;
                    //$rootScope.patient = data;
                    console.log(data);
                    //PatientService.setPatient(data);
                    $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                Ladda.stopAll();
                $scope.pageForm.submitted = true;
            }
        };
    }
})();
