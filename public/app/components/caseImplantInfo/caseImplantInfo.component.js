(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osCaseImplantInfo', {
        templateUrl: 'app/components/caseImplantInfo/caseImplantInfo.component.html',
        controller: CaseImplantInfoController,
        bindings: {
            event: '<'
        }
    });
    CaseImplantInfoController.$inject = ['CaseDetailsFactory', '$confirm'];
    
    function CaseImplantInfoController(CaseDetailsFactory, $confirm) {
        var ctrl = this;
        ctrl.implantEntryType = 'barcode';
        ctrl.selectedProduct = '';

        ctrl.onSelectProduct = onSelectProduct;
        ctrl.loadProducts = loadProducts;
        ctrl.checkAndSaveImplantBarcode = checkAndSaveImplantBarcode;
        ctrl.addImplant = addImplant;
        ctrl.isEnterKeyPressed = isEnterKeyPressed;
        ctrl.deleteImplant = deleteImplant;
        
        
        ////////////////
        ctrl.$onInit = function () {
            loadProducts();
        };
        ctrl.$onChanges = function (changesObj) { };
        ctrl.$onDestory = function () { };
    
        //Implants
        function loadProducts() {
            CaseDetailsFactory.LoadProducts().then(function (data) {
                //console.log(data);
                ctrl.products = data;
            }, function (error) {
            });
        }

        function onSelectProduct($item, $model, $label) {
            ctrl.selectedProduct = $item;
        }
    
        function checkAndSaveImplantBarcode() {
            console.log(ctrl.implantEntryType);
            if (ctrl.implantEntryType === 'barcode') {
                console.log(ctrl.firstbar);
                if ((angular.isDefined(ctrl.firstbar) && ctrl.firstbar.indexOf('/') !== -1) || (angular.isDefined(ctrl.firstbar) && ctrl.firstbar.length > 25)) {
                    ctrl.secondbar = '';
                    ctrl.month = '';
                    ctrl.year = '';
                    ctrl.addImplant();
                }
                else if (angular.isDefined(ctrl.firstbar) && ctrl.firstbar.length > 0 &&
                    angular.isDefined(ctrl.secondbar) && ctrl.secondbar.length > 0 &&
                    angular.isDefined(ctrl.month) && ctrl.month != null &&
                    angular.isDefined(ctrl.year) && ctrl.year != null) {
                    ctrl.addImplant();
                }
                else {
                    Ladda.stopAll();
                }
            }
            else if (ctrl.implantEntryType === 'manual') {
                addImplant();
            }
        }
        function addImplant() {
            console.log('called?');
            var l = Ladda.create(document.querySelector('.implant-btn'));
            l.start();
            if (ctrl.implantEntryType === 'barcode') {
                CaseDetailsFactory.AddImplant(ctrl.firstbar, ctrl.secondbar, ctrl.month, ctrl.year, ctrl.procedure.Id).then(function (data) {
                    //console.log(data);
                    refreshEvents();

                    ctrl.firstbar = '';
                    ctrl.secondbar = '';
                    ctrl.month = 'null';
                    ctrl.year = 'null';
                    angular.element(document.querySelector('#firstbar'))[0].focus();
                    Ladda.stopAll();
                }, function (error) {
                    console.log(error);
                    Ladda.stopAll();
                });
            }
            else if (ctrl.implantEntryType === 'manual') {
                CaseDetailsFactory.AddImplantManually(ctrl.selectedProduct.Id, ctrl.lotNumber, ctrl.month, ctrl.year, ctrl.procedure.Id).then(function (data) {
                    //console.log(data);
                    refreshEvents();
                    ctrl.selectedProduct = '';
                    ctrl.lotNumber = '';
                    ctrl.month = 'null';
                    ctrl.year = 'null';
                    angular.element(document.querySelector('#ProductNumber'))[0].focus();
                    Ladda.stopAll();
                }, function (error) {
                    console.log(error);
                    Ladda.stopAll();
                });
            }
        }
    
        function isEnterKeyPressed(event) {
            if (event.keyCode === 13) {
                angular.element(document.querySelector('#secondbar'))[0].focus();
            }
        }
    
        function deleteImplant(implant) {
            $confirm({ text: 'Are you sure you want to delete this implant (' + implant.Product_Catalog_Id__r.Name + ')?', title: 'Delete Implant' })
                .then(function () {
                    CaseDetailsFactory.DeleteImplant(implant.Id).then(function (data) {
                        refreshEvents();
                        // $state.go($state.current, {}, { reload: true });
                    }, function (error) {
                        console.log(error);
                    });
                });
        };
    }
})();
