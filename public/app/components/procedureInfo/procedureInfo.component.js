(function() {
'use strict';

    // Usage:
    // 
    // Creates:
    // 

    angular
        .module('orthosensor')
        .component('osProcedureInfo', {
            
            templateUrl: 'app/components/procedureInfo/procedureInfo.component.html',
            controller: ProcedureInfoController,
            bindings: {
                procedure: '<',
                clinicalData: '<'
            },
        });

    ProcedureInfoController.inject = [];
    function ProcedureInfoController() {
        var ctrl = this;
        

        ////////////////

        ctrl.onInit = function() { };
        ctrl.onChanges = function(changesObj) { };
        ctrl.onDestory = function() { };
    }
})();