var orthosensor;
(function (orthosensor) {
    var PatientDetailInfoController = (function () {
        function PatientDetailInfoController(PatientService, $state, $uibModal) {
            this.PatientService = PatientService;
            this.$state = $state;
            this.$uibModal = $uibModal;
            this.$onInit = function () {
                var x = this.PatientService.patient;
                // console.log(x);
                this.patient = this.PatientService.getSFObject();
                // console.log(this.patient);
            };
            this.$onChanges = function (changesObj) { };
            this.$onDestroy = function () { };
            this.patientDetailsVisible = false;
        }
        PatientDetailInfoController.prototype.togglePatientDetailsDrawer = function () {
            this.patientDetailsVisible = !this.patientDetailsVisible;
        };
        PatientDetailInfoController.prototype.editPatient = function (patient) {
            // this.PatientService.setPatientFromSFObject(patient);
            this.$state.go('addPatient');
            // this.$uibModal.open({
            //     templateUrl: 'app/components/editPatient/editPatient.component.html',
            //     controller: 'editPatientController',
            //     size: 'large'
            // });
        };
        return PatientDetailInfoController;
    }());
    PatientDetailInfoController.$inject = ['PatientService', '$state', '$uibModal'];
    var PatientDetailInfo = (function () {
        function PatientDetailInfo() {
            this.bindings = {};
            this.controller = PatientDetailInfoController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/patientDetailInfo/patientDetailInfo.component.html';
        }
        return PatientDetailInfo;
    }());
    angular
        .module('orthosensor')
        .component('osPatientDetailInfo', new PatientDetailInfo());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientDetailInfo.component.js.map