// (function () {
//     'use strict';

//     // Usage:
//     // 
//     // Creates:
//     // 

//     angular
//         .module('orthosensor')
//         .component('osPatientDetailBasicInfo', {
//             templateUrl: 'app/components/patientDetailInfo/patientDetailBasicInfo.component.html',
//             controller: PatientDetailBasicInfoController,
//             bindings: {
//                 // Binding: '=',
//             },
//         });

//     PatientDetailBasicInfoController.$inject = ['PatientService'];
//     function PatientDetailBasicInfoController(PatientService) {
//         var $ctrl = this;
//         ////////////////

//         $ctrl.$onInit = function () {
//             this.patient = PatientService.getPatient();
//         };
//         $ctrl.$onChanges = function (changesObj) { };
//         $ctrl.$onDestory = function () { };

//     }
// })();
