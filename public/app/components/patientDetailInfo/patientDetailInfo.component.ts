module orthosensor {

    class PatientDetailInfoController {
        public static $inject = ['PatientService', '$state', '$uibModal'];
        public patientDetailsVisible: Boolean;
        public patient: orthosensor.domains.sfPatient;
        constructor(public PatientService: orthosensor.services.PatientService, public $state: ng.ui.IStateService, public $uibModal: ng.ui.bootstrap.IModalService) {

            this.patientDetailsVisible = false;

        }

        $onInit = function () {
            let x = this.PatientService.patient;
            // console.log(x);
            this.patient = this.PatientService.getSFObject();
            // console.log(this.patient);
        };

        $onChanges = function (changesObj) { };
        $onDestroy = function () { };

        togglePatientDetailsDrawer() {
            this.patientDetailsVisible = !this.patientDetailsVisible;
        }
        editPatient(patient: any) {
            // this.PatientService.setPatientFromSFObject(patient);
            this.$state.go('addPatient');
            // this.$uibModal.open({
            //     templateUrl: 'app/components/editPatient/editPatient.component.html',
            //     controller: 'editPatientController',
            //     size: 'large'
            // });
        }
    }
    class PatientDetailInfo implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {

            };
            this.controller = PatientDetailInfoController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/patientDetailInfo/patientDetailInfo.component.html';
        }

    }
    angular
        .module('orthosensor')
        .component('osPatientDetailInfo', new PatientDetailInfo());
}