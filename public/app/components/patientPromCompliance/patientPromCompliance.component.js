(function () {
    angular
        .module("orthosensor.dashboard")
        .component("patientPromCompliance", {
        templateUrl: 'app/components/patientPromCompliance/patientPromCompliance.component.html',
        //controllerAs: 'vm',
        controller: PatientPromComplianceController
    });
    //PatientPromComplianceController.$inject['datacontext'];
    function PatientPromComplianceController(datacontext) {
        var ctrl = this;
        ctrl.$onInit = function () {
            ctrl.practices = datacontext.getPracticeList();
            ctrl.surgeons = datacontext.getSurgeonList();
            ctrl.timePeriods = [
                { "id": 1, "period": "1 week" },
                { "id": 2, "period": "4 weeks" },
                { "id": 3, "period": "60 days" },
                { "id": 4, "period": "90 days" },
                { "id": 5, "period": "6 months" }
            ];
            ctrl.timePeriod = ctrl.timePeriods[0];
            //getPracticeActivity("0", "1 week");
        };
        ctrl.isHospital = isHospital;
        ctrl.chart = {};
        ctrl.chart.type = "ColumnChart";
        ctrl.chart.data = {
            "cols": [
                { id: "t", label: "Topping", type: "string" },
                { id: "s", label: "Score", type: "number" }
            ], "rows": [
                {
                    c: [
                        { v: "pre-procedure" },
                        { v: 3 },
                    ]
                },
                {
                    c: [
                        { v: "post 4 week" },
                        { v: 8 },
                    ]
                }, {
                    c: [
                        { v: "post 6 months" },
                        { v: 6 },
                    ]
                },
                {
                    c: [
                        { v: "1 year" },
                        { v: 2 },
                    ]
                }
            ]
        };
        ctrl.chart.options = {
            'title': 'Patient PROM Compliance'
        };
        function isHospital() {
            return true;
        }
    }
})();
