(function() {
'use strict';

    // Usage:
    // 
    // Creates:
    // 

    angular
        .module('orthosensor')
        .component('osCaseSensorItems', {
            
            templateUrl: 'app/components/caseSensorInfo/caseSensorItems.component.html',
            controller: CaseSensorItemsController,
            bindings: {
                event: '<',
            },
        });

    CaseSensorItemsController.inject = ['CaseDetailsFactory', '$confirm'];
    function CaseSensorItemsController(CaseDetailsFactory, $confirm) {
        var ctrl = this;
        ctrl.deleteSensor = deleteSensor;

        ////////////////

        ctrl.onInit = function () {
            console.log(ctrl.event);
        };
        ctrl.onChanges = function(changesObj) { };
        ctrl.onDestroy = function () { };
        
        function deleteSensor(device) {
            $confirm({ text: 'Are you sure you want to delete this sensor (' + device.OS_Device__r.Device_ID__c + ')?', title: 'Delete Sensor' })
                .then(function () {
                CaseDetailsFactory.DeleteSensor(device.OS_Device__r.Id, ctrl.procedure.Id).then(function (data) {
                    //console.log(data);
                    // $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        };
    }
})();