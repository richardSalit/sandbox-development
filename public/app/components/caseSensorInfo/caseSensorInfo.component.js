(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osCaseSensorInfo', {
            bindings: {
                event: '<'
            },
            templateUrl: 'app/components/caseSensorInfo/caseSensorInfo.component.html',
            controller: CaseSensorInfoController
        });
    CaseSensorInfoController.$inject = ['CaseDetailsFactory', 'SensorService'];
    function CaseSensorInfoController(CaseDetailsFactory, SensorService) {
        var ctrl = this;
        ctrl.sensorEntryType = 'barcode';
        ctrl.onSelectRef = onSelectRef;
        ctrl.addSensor = addSensor;
        ctrl.loadSensorRefs = loadSensorRefs;
        ctrl.isEnterKeyPressedSensor = isEnterKeyPressedSensor;
        ////////////////
        this.$onInit = function () {
            console.log(ctrl.event);
            this.sensorEntryType = 'barcode';
            loadSensorRefs();
        };
        this.$onChanges = function (changesObj) { };
        this.$onDestory = function () { };
        //Sensors
        function loadSensorRefs() {
            CaseDetailsFactory.loadSensorRefs()
                .then(function (data) {
                    //console.log(data);
                    ctrl.sensorRefs = data;
                }, function (error) {
                });
        }
        function onSelectRef($item, $model, $label) {
            ctrl.selectedRef = $item;
            setTimeout(function () {
                angular.element(document.querySelector('#serialNumber'))[0].focus();
            }, 200);
        }

        function addSensor() {
            var l = Ladda.create(document.querySelector('.sensor-btn'));
            l.start();
            if (ctrl.sensorEntryType === 'barcode') {
                SensorService.AddSensor(ctrl.sensorCode, ctrl.procedure.Id)
                    .then(function (data) {
                        //console.log(data);
                        //loadCaseEvents();
                        ctrl.sensorCode = '';
                        angular.element(document.querySelector('#sensorCode'))[0].focus();
                        Ladda.stopAll();
                    }, function (error) {
                        Ladda.stopAll();
                    });
            }
        }

        function isEnterKeyPressedSensor(event) {
            if (event.keyCode === 13) {
                addSensor();
            }
        }
    }
})();
