(function() {
'use strict';

    // Usage:
    // 
    // Creates:
    // 

    angular
        .module('orthosensor')
        .component('0sSurveyOutcomeData', {
            
            templateUrl: 'app/component.surveyOutcomeData/surveyOutcomeData.component.html',
            controller: SurveyOutcomeDataController,
            bindings: {
                chartCategories: '<',
                chartData: '<'
            },
        });

    SurveyOutcomeDataController.inject = ['dependency1'];
    function SurveyOutcomeDataController(dependency1) {
        var ctrl = this;
        ctrl.toggleValue = 'chart';
        ctrl.toggleOutcomes = toggleOutcomes;
        ////////////////

        ctrl.onInit = function() { };
        ctrl.onChanges = function(changesObj) { };
        ctrl.onDestory = function () { };
        
        function toggleOutcomes(viewID) {
            if (ctrl.toggleValue === 'data') {
                ctrl.toggleValue = 'chart';
            }
            else {
                ctrl.toggleValue = 'data';
            }
        }
    }
})();