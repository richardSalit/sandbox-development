(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osCaseNotes', {
        templateUrl: 'app/components/caseNotes/caseNotes.component.html',
        controller: CaseNotesController,
        bindings: {
            procedure: '<',
            notes: '<'
        }
    });
    CaseNotesController.$inject = ['$state', '$uibModal', '$confirm', 'CaseDetailsFactory', 'CaseService', 'LogFactory'];
    function CaseNotesController($state, $uibModal, $confirm, CaseDetailsFactory, CaseService, LogFactory) {
        var ctrl = this;
        ctrl.addNewCaseNote = addNewCaseNote;
        // ctrl.editCaseNote = editCaseNote;
        var procedureId = '';
        ////////////////
        ctrl.$onInit = function () {
            console.log(ctrl.procedure);
            console.log(ctrl.notes);
            ctrl.title = this.procedure.Id;
            procedureId = this.procedure.Id;
            CaseService.setProcedure(ctrl.procedure);
        };
        ctrl.$onChanges = function (changesObj) { };
        ctrl.$onDestory = function () { };
        //case note methods        
        function addNewCaseNote() {
            $uibModal.open({
                template: '<os-add-case-note></os-add-casenote>',
                //app/components/caseNotes/ addCaseNote.component.html',
                // controller: 'AddCaseNoteController',
                //  controllerAs: '$ctrl',
                size: 'medium'
            });
        }
       
        function editCaseNote(note) {
            $uibModal.open({
                templateUrl: 'app/components/caseNotes/editCaseNote.component.html',
                controller: 'EditCaseNoteController',
                controllerAs: '$ctrl',
                size: 'medium'
            });
        }
    }
})();
