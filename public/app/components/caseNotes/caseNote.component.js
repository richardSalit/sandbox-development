(function() {
'use strict';

    // Usage: individual notes
    // 
    // Creates:
    // 

    angular
        .module('orthosensor')
        .component('osCaseNote', {
         
            templateUrl: 'app/components/caseNotes/caseNote.component.html',
            controller: CaseNoteController,
            bindings: {
                note: '=',
            },
        });

    CaseNoteController.inject = ['$uibModal',  'CaseDetailsFactory', '$confirm'];
    function CaseNoteController($uibModal,  CaseDetailsFactory, $confirm) {
        var ctrl = this;
        ctrl.editCaseNote = editCaseNote;
        ctrl.deleteCaseNote = deleteCaseNote;
        

        ////////////////

        ctrl.onInit = function() { };
        ctrl.onChanges = function(changesObj) { };
        ctrl.onDestory = function () { };
        
        function editCaseNote(note) {
            $uibModal.open({
                templateUrl: 'app/components/caseNotes/editCaseNote.component.html',
                controller: function ($state, $uibModalInstance, CaseDetailsFactory) {
                    var $ctrl = this;
                    $ctrl.caseTitle = note.Title;
                    $ctrl.caseNote = note.Body;
                    $ctrl.noteId = note.Id;
                    $ctrl.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $ctrl.updateNote = function () {
                        CaseDetailsFactory.UpdateNote($ctrl.caseTitle, $ctrl.caseNote, $ctrl.noteId).then(function (data) {
                            //console.log(data);
                            $uibModalInstance.dismiss();
                            $state.go($state.current, {}, { reload: true });
                        }, function (error) {
                            console.log(error);
                        });
                    };
                },
                size: 'large'
            });
        }
        function deleteCaseNote(note) {
            $confirm({ text: 'Are you sure you want to delete this note?', title: 'Delete Note' })
                .then(function () {
                CaseDetailsFactory.DeleteNote(note.Id).then(function (data) {
                    //console.log(data);
                    $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        }
    }
})();