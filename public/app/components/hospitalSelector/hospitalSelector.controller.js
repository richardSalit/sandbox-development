(function () {
    'use strict';
    function HospitalSelector($rootScope, $uibModalInstance, UserService, PatientListFactory, PatientService, HospitalService) {
        var $ctrl = this;
        $ctrl.practice = null;
        $ctrl.hospital = null;
        $ctrl.setHospitalFilter = setHospitalFilter;
        $ctrl.cancel = cancel;
        activate();
        function activate() {
            $ctrl.hospitals = HospitalService.hospitals;
            console.log(HospitalService.hospitals);
            $ctrl.isUserAdmin = UserService.isAdmin;
            if (!$ctrl.isUserAdmin) {
                $ctrl.hospital = UserService.accountId;
            }
            console.log($ctrl.isUserAdmin);
            console.log($ctrl.hospital);
            $ctrl.practices = HospitalService.practices;
            setModalTitle();
        }
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
        function setModalTitle() {
            if ($ctrl.isUserAdmin) {
                $ctrl.title = 'Select Hospital /Practice';
            }
            else {
                $ctrl.title = 'Select Practice';
            }
        }
        function setHospitalFilter() {
            console.log($ctrl.hospital);
            console.log($ctrl.practice);
            if ($ctrl.hospital) {
                // $rootScope.filterHospitalId = $ctrl.hospital.Id;
                HospitalService.filterHospitalId = $ctrl.hospital.Id;
                HospitalService.currentFilterId = $ctrl.hospital.Id;
                // $rootScope.filterPracticeId = undefined;
                HospitalService.filterPracticeId = undefined;
            }
            if ($ctrl.practice) {
                console.log($ctrl.practice);
                console.log('Getting practice info');
                // $rootScope.filterHospitalId = $ctrl.practice;
                HospitalService.filterHospitalId = $ctrl.hospital.Id;
                HospitalService.filterPracticeId = $ctrl.practice;
                HospitalService.currentFilterId = $ctrl.practice;
            }
            $uibModalInstance.close('Ok');
            //$state.go($state.current, {}, { reload: true });
        }
    }
    HospitalSelector.$inject = ['$rootScope', '$uibModalInstance', 'UserService', 'PatientListFactory', 'PatientService', 'HospitalService'];
    angular
        .module('orthosensor')
        .controller('HospitalSelector', HospitalSelector);
})();
//# sourceMappingURL=hospitalSelector.controller.js.map