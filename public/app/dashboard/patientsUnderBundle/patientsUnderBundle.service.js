var orthosensor;
(function (orthosensor) {
    var service;
    (function (service) {
        'use strict';
        var PatientsUnderBundleService = (function () {
            function PatientsUnderBundleService(DashboardDataService, UserService) {
                this.DashboardDataService = DashboardDataService;
            }
            PatientsUnderBundleService.prototype.getPatientsUnderBundle = function (practiceId) {
                this.DashboardDataService.getPatientsUnderBundle(practiceId)
                    .then(function (data) {
                    console.log(data);
                    return data;
                }, function (error) {
                    console.log(error);
                    return error;
                });
            };
            return PatientsUnderBundleService;
        }());
        PatientsUnderBundleService.inject = ['DashboardDataService', 'UserService'];
        service.PatientsUnderBundleService = PatientsUnderBundleService;
        angular
            .module('orthosensor.services')
            .service('PatientsUnderBundleService', PatientsUnderBundleService);
    })(service = orthosensor.service || (orthosensor.service = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientsUnderBundle.service.js.map