namespace orthosensor.service {
    'use strict';

    export interface IPatientsUnderBundleService {
    }
    export class PatientsUnderBundleService implements IPatientsUnderBundleService {
        static inject: Array<string> = ['DashboardDataService', 'UserService'];
        protected DashboardDataService: any;

        constructor(DashboardDataService: any, UserService: any) {
            this.DashboardDataService = DashboardDataService;
        }
        getPatientsUnderBundle(practiceId: string): any {
            this.DashboardDataService.getPatientsUnderBundle(practiceId)
                .then((data: any) => {
                    console.log(data);
                    return data;
                }, (error: any) => {
                    console.log(error);
                    return error;
                });

        }
    }
    angular
        .module('orthosensor.services')
        .service('PatientsUnderBundleService', PatientsUnderBundleService);
}
