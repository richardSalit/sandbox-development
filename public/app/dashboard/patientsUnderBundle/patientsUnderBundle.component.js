var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PatientsUnderBundleController = (function (_super) {
            __extends(PatientsUnderBundleController, _super);
            function PatientsUnderBundleController(DashboardService, DashboardDataService, PatientsUnderBundleService, PatientService, UserService, $state, $timeout) {
                var _this = _super.call(this, UserService) || this;
                _this.DashboardService = DashboardService;
                _this.PatientService = PatientService;
                _this.UserService = UserService;
                _this.$timeout = $timeout;
                _this.DashboardDataService = DashboardDataService;
                _this.PatientsUnderBundleService = PatientsUnderBundleService;
                _this.$state = $state;
                _this.title = 'Patients Under Bundle';
                return _this;
            }
            PatientsUnderBundleController.prototype.$onInit = function () {
                var _this = this;
                var practiceId = this.UserService.user.accountId;
                console.log(practiceId);
                //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
                if (!this.isUserAdmin) {
                    this.DashboardDataService.getPatientsUnderBundle(practiceId)
                        .then(function (data) {
                        _this.patients = data;
                        console.log(_this.patients);
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
            };
            PatientsUnderBundleController.prototype.patientDetails = function (patient) {
                var _this = this;
                console.log(patient);
                this.PatientService.setPatientId(patient.patientId);
                this.$timeout(function () {
                    _this.$state.go('patientDetails');
                }, 1000);
            };
            return PatientsUnderBundleController;
        }(dashboard.DashboardBaseController));
        PatientsUnderBundleController.$inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state', '$timeout'];
        var PatientsUnderBundle = (function () {
            function PatientsUnderBundle() {
                this.bindings = {
                    practiceId: '@',
                };
                this.controller = PatientsUnderBundleController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/dashboard/patientsUnderBundle/patientsUnderBundle.component.html';
            }
            return PatientsUnderBundle;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osPatientsUnderBundle', new PatientsUnderBundle());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientsUnderBundle.component.js.map