module orthosensor.dashboard {

    class PromDeltaListController extends DashboardChartBaseController {

        public static $inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'moment', 'config', 'DashboardDataService', 'PromDeltaService'];

        public title: string;
        public user: orthosensor.domains.User;
        public hospitals: orthosensor.domains.Hospital[];
        public hospitalPracticeLabel: string;
        public surgeonLabel: string;
        public chartService: orthosensor.services.ChartService;
        public DashboardDataService: orthosensor.services.DashboardDataService;
        public PromDeltaService: orthosensor.dashboard.PromDeltaService;
        public promDeltaChart: orthosensor.domains.ChartType;
        // public surgeons: orthosensor.domains.Surgeon[];
        public chart: orthosensor.domains.ChartType;
        // public surgeon: orthosensor.domains.Surgeon;
        public hospital: orthosensor.domains.Hospital;
        public practices: orthosensor.domains.Practice[];
        public practice: orthosensor.domains.Practice;
        public options: any;
        public data: any[];
        public monthsToChart: string;
        public chartHeight: number;
        public startDate: any;
        public endDate: any;
        public startMonth: number;
        public startYear: number;
        public monthDiff: number;
        public chartData: any[];
        public promDeltas: orthosensor.domains.PromDelta[];
        public surgeonSelectClass: string;
        public practiceSelectClass: string;
        public surveys: any[];
        
        public allData: orthosensor.domains.KneeBalanceAvg[];
        public chartType: string;
        // private PatientService: any;

        public dateFormat: string = 'MM/DD/YYYY';

        ///// /* @ngInject */
        // tslint:disable-next-line:max-line-length
        constructor(public DashboardService: any, public ChartService: any, PatientListFactory: any, public UserService: any, moment: any, config: any, DashboardDataService: orthosensor.services.DashboardDataService, PromDeltaService: any) {
            super(DashboardService, ChartService, PatientListFactory, UserService, moment, config);
            // this.PatientService = PatientListFactory;
            this.title = 'PROM score deltas';
            this.DashboardDataService = DashboardDataService;
            this.PromDeltaService = PromDeltaService;
            this.hospitals = [];
            this.practices = [];
            this.practice = new orthosensor.domains.Practice();
            this.surgeonLabel = 'Surgeon';
            this.surgeons = [];
            this.chartHeight = DashboardService.chartHeight;
            // this.user = UserService.user;

            // console.log(this.user);
            
            // console.log(config);
            this.$onInit();
        }

        public $onInit(): void {
            super.$onInit();
            
            // this.surgeons = this.DashboardService.surgeons;
            console.log(this.surgeons);
            // this.proms = this.getProms();
            // this.chartData = [];
            this.surveys = this.DashboardService.surveys;
            this.promDeltas = [];
            this.setDateParams();
            this.initChartParams();
            this.initData();

        };

        public showPracticeList(): boolean {
            return this.isUserAdmin || this.user.userType === 'Hospital Partner Community User';
        }

        public showSurgeonList(): boolean {
            return this.isUserAdmin || this.user.userType === 'Surgeon Partner Community User';
        }

        public initChartParams(): void {
            // this.initChartLookups();
           
        }

        // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
        public initData() {

            switch (this.user.userType) {
                case 'Surgeon Partner Community User':
                    // console.log('surgeon Account');
                    this.setSurgeonPartner();
                    break;

                case 'Hospital Partner Community User':
                    // console.log('hospital Account');
                    this.setHospitalPartner();
                    break;

                default:
                    // console.log('Admin Account');
                    this.setAdmin();
            }
        }

        public setSurgeonPartner() {
            if (this.surgeons !== undefined) {
                for (let i = 0; i < this.surgeons.length; i++) {
                    if (this.surgeons[i].parentId === this.practice.id) {
                        this.surgeon = this.surgeons[i];
                        this.data = this.getSurgeonPromDeltas();
                        break;
                    }
                }
            }
        }

        public setHospitalPartner() {
            // console.log(this.practices);
            if (this.practices !== undefined) {
                if (this.practices.length > 0) {
                    this.practice = this.practices[0];
                    this.data = this.getPracticePromDeltas();

                }
            }
        }

        public setAdmin() {
            if (this.hospitals !== undefined) {
                if (this.hospitals.length > 0) {
                    this.hospital = this.hospitals[0];
                    if (this.practices !== undefined) {
                        if (this.practices.length > 0) {
                            this.practice = this.practices[0];
                            // this.data = this.getPracticeKneeBalanceAvg();
                            this.getPracticePromDeltas(); // for testing!!
                        }
                    }
                }
            }
        }

        

        // called from page
        public getChart(surgeon: any): any[] {
            switch (this.user.userType) {
                case 'Surgeon Partner Community User':
                    return this.getSurgeonData();

                case 'Hospital Partner Community User':
                    console.log('trying to change the chart');    
                    return this.getPracticeData();

                default:
                    return this.getSurgeonData();
            }
        }

        public getSurgeonData(): any[] {
            return this.getSurgeonPromDeltas();
        }

        public getPracticeData(): any[] {
            return this.getPracticePromDeltas();
        }

        public getMonthLabel(date: Date): string {
            let dateLabel: string = this.formatDate(date);
            return dateLabel;
        }

        public getData(): any[] {

            console.log(this.promDeltas);
            this.chartData = [];
            // let chartData: any[] = [];

            this.data = this.chartData;
            return this.chartData;
        }

        getPracticePromDeltas(): orthosensor.domains.PromDelta[] {
            let practice: string = this.practice.id;

            // console.log('Getting knee balance data');
            // loop through months
            console.log(this.monthDiff);
            let startMonth = moment(this.startDate).month() + 1;
            let startYear = moment(this.startDate).year();
            //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
            let endMonth = moment(this.endDate).month() + 1;
            let endYear = moment(this.endDate).year();
            console.log(this.practice);
            this.DashboardDataService.getPromDeltasByPractice(this.practice.id, startMonth, startYear, endMonth, endYear)
                .then((data: any) => {
                    let result = data;
                    console.log(result);
                    if (result !== null) {
                        this.promDeltas = this.PromDeltaService.convertDataToObjects(result);
                        console.log(this.promDeltas);
                        //this.getData();
                    }
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return this.promDeltas;
        }

        getSurgeonPromDeltas(): orthosensor.domains.PromDelta[] {
            // let surgeon: string = this.surgeon.id;
            console.log(this.monthDiff);
            let startMonth = moment(this.startDate).month() + 1;
            let startYear = moment(this.startDate).year();
            //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
            let endMonth = moment(this.endDate).month() + 1;
            let endYear = moment(this.endDate).year();
            console.log(this.surgeon);
            if (this.surgeon !== undefined) {
                let surgeon = this.surgeon.id;
                this.DashboardDataService.getPromDeltasBySurgeon(this.surgeon.id, startMonth, startYear, endMonth, endYear)
                    .then((data: any) => {
                        let result = data;
                        console.log(result);
                        if (result !== null) {
                            this.promDeltas = this.PromDeltaService.convertDataToObjects(result);
                            console.log(this.promDeltas);
                            this.getData();
                        }
                    }, (error: any) => {
                        console.log(error);
                        return error;
                    });
            }
            return this.promDeltas;

        }
    }

    angular
        .module('orthosensor.dashboard')
        .component('osPromDeltaList', {
            controller: PromDeltaListController,
            controllerAs: '$ctrl',
            templateUrl: 'app/dashboard/promDeltaList/promDeltaList.component.html',
            bindings: {
              
            }
        });
}

class PromDeltaList {
    constructor(public name: string,
        public displayName: string,
        public color: string,
    ) {
    }
}
