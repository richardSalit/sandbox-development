var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PromDeltaListController = (function (_super) {
            __extends(PromDeltaListController, _super);
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function PromDeltaListController(DashboardService, ChartService, PatientListFactory, UserService, moment, config, DashboardDataService, PromDeltaService) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, moment, config) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                _this.UserService = UserService;
                // private PatientService: any;
                _this.dateFormat = 'MM/DD/YYYY';
                // this.PatientService = PatientListFactory;
                _this.title = 'PROM score deltas';
                _this.DashboardDataService = DashboardDataService;
                _this.PromDeltaService = PromDeltaService;
                _this.hospitals = [];
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.surgeons = [];
                _this.chartHeight = DashboardService.chartHeight;
                // this.user = UserService.user;
                // console.log(this.user);
                // console.log(config);
                _this.$onInit();
                return _this;
            }
            PromDeltaListController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                // this.surgeons = this.DashboardService.surgeons;
                console.log(this.surgeons);
                // this.proms = this.getProms();
                // this.chartData = [];
                this.surveys = this.DashboardService.surveys;
                this.promDeltas = [];
                this.setDateParams();
                this.initChartParams();
                this.initData();
            };
            ;
            PromDeltaListController.prototype.showPracticeList = function () {
                return this.isUserAdmin || this.user.userType === 'Hospital Partner Community User';
            };
            PromDeltaListController.prototype.showSurgeonList = function () {
                return this.isUserAdmin || this.user.userType === 'Surgeon Partner Community User';
            };
            PromDeltaListController.prototype.initChartParams = function () {
                // this.initChartLookups();
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            PromDeltaListController.prototype.initData = function () {
                switch (this.user.userType) {
                    case 'Surgeon Partner Community User':
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 'Hospital Partner Community User':
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            PromDeltaListController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonPromDeltas();
                            break;
                        }
                    }
                }
            };
            PromDeltaListController.prototype.setHospitalPartner = function () {
                // console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticePromDeltas();
                    }
                }
            };
            PromDeltaListController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                // this.data = this.getPracticeKneeBalanceAvg();
                                this.getPracticePromDeltas(); // for testing!!
                            }
                        }
                    }
                }
            };
            // called from page
            PromDeltaListController.prototype.getChart = function (surgeon) {
                switch (this.user.userType) {
                    case 'Surgeon Partner Community User':
                        return this.getSurgeonData();
                    case 'Hospital Partner Community User':
                        console.log('trying to change the chart');
                        return this.getPracticeData();
                    default:
                        return this.getSurgeonData();
                }
            };
            PromDeltaListController.prototype.getSurgeonData = function () {
                return this.getSurgeonPromDeltas();
            };
            PromDeltaListController.prototype.getPracticeData = function () {
                return this.getPracticePromDeltas();
            };
            PromDeltaListController.prototype.getMonthLabel = function (date) {
                var dateLabel = this.formatDate(date);
                return dateLabel;
            };
            PromDeltaListController.prototype.getData = function () {
                console.log(this.promDeltas);
                this.chartData = [];
                // let chartData: any[] = [];
                this.data = this.chartData;
                return this.chartData;
            };
            PromDeltaListController.prototype.getPracticePromDeltas = function () {
                var _this = this;
                var practice = this.practice.id;
                // console.log('Getting knee balance data');
                // loop through months
                console.log(this.monthDiff);
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                console.log(this.practice);
                this.DashboardDataService.getPromDeltasByPractice(this.practice.id, startMonth, startYear, endMonth, endYear)
                    .then(function (data) {
                    var result = data;
                    console.log(result);
                    if (result !== null) {
                        _this.promDeltas = _this.PromDeltaService.convertDataToObjects(result);
                        console.log(_this.promDeltas);
                        //this.getData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.promDeltas;
            };
            PromDeltaListController.prototype.getSurgeonPromDeltas = function () {
                var _this = this;
                // let surgeon: string = this.surgeon.id;
                console.log(this.monthDiff);
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                console.log(this.surgeon);
                if (this.surgeon !== undefined) {
                    var surgeon = this.surgeon.id;
                    this.DashboardDataService.getPromDeltasBySurgeon(this.surgeon.id, startMonth, startYear, endMonth, endYear)
                        .then(function (data) {
                        var result = data;
                        console.log(result);
                        if (result !== null) {
                            _this.promDeltas = _this.PromDeltaService.convertDataToObjects(result);
                            console.log(_this.promDeltas);
                            _this.getData();
                        }
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
                return this.promDeltas;
            };
            return PromDeltaListController;
        }(dashboard.DashboardChartBaseController));
        PromDeltaListController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'moment', 'config', 'DashboardDataService', 'PromDeltaService'];
        angular
            .module('orthosensor.dashboard')
            .component('osPromDeltaList', {
            controller: PromDeltaListController,
            controllerAs: '$ctrl',
            templateUrl: 'app/dashboard/promDeltaList/promDeltaList.component.html',
            bindings: {}
        });
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
var PromDeltaList = (function () {
    function PromDeltaList(name, displayName, color) {
        this.name = name;
        this.displayName = displayName;
        this.color = color;
    }
    return PromDeltaList;
}());
//# sourceMappingURL=promDeltaList.component.js.map