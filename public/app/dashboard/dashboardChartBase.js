var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var DashboardChartBaseController = (function (_super) {
            __extends(DashboardChartBaseController, _super);
            function DashboardChartBaseController(DashboardService, ChartService, PatientListFactory, UserService, moment, config) {
                var _this = _super.call(this, UserService) || this;
                _this.DashboardService = DashboardService;
                _this.UserService = UserService;
                _this.dateFormat = 'MM/DD/YYYY';
                _this.PatientService = PatientListFactory;
                return _this;
            }
            DashboardChartBaseController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                this.hospitals = this.DashboardService.hospitals;
                this.monthsToChart = '3';
                // handle surgeon or practice staff
                if (this.user.userType === 'Surgeon Partner Community User') {
                    this.hospital = this.DashboardService.hospital;
                    this.practice.id = this.user.accountId;
                    this.practice.name = this.user.accountName;
                }
                else {
                    console.log(this.hospitals);
                    this.practices = this.DashboardService.practices;
                    console.log(this.practices);
                }
                this.surgeons = this.DashboardService.surgeons;
                if (this.surgeons && this.surgeons.length) {
                    this.surgeon = this.surgeons[0];
                }
                this.surgeonSelectClass = this.getSurgeonPartnerSurgeonSelectClass();
                console.log(this.surgeonSelectClass);
                this.practiceSelectClass = this.getHospitalPartnerPracticeSelectClass();
            };
            DashboardChartBaseController.prototype.setDateParams = function () {
                var today = moment();
                // console.log(today);
                var endDate = moment(today).add((Number(this.monthsToChart) + 1), 'months');
                //  console.log(endDate);
                var sEndDate = moment(endDate).format(this.dateFormat);
                // console.log(sEndDate);
                var startMonth = moment(today).subtract((Number(this.monthsToChart) - 1), 'months');
                var startDate = moment(startMonth).startOf('month');
                this.monthDiff = moment(endDate).diff(startDate, 'months');
                // console.log(this.monthDiff);
                this.startDate = moment(startDate).format(this.dateFormat);
                this.endDate = moment(endDate).format(this.dateFormat);
                // console.log(this.startDate);
            };
            DashboardChartBaseController.prototype.showPracticeList = function () {
                return this.isUserAdmin || this.user.userType === 'Hospital Partner Community User';
            };
            DashboardChartBaseController.prototype.getSurgeonPartnerSurgeonSelectClass = function () {
                if (this.user.userType === 'Surgeon Partner Community User') {
                    return "col-md-4 col-sm-6 col-xs-10";
                }
                else {
                    return "col-md-2 col-sm-3 col-xs-6";
                }
            };
            DashboardChartBaseController.prototype.getHospitalPartnerPracticeSelectClass = function () {
                if (this.user.userType === 'Hospital Partner Community User') {
                    return "col-md-4 col-sm-6 col-xs-10";
                }
                else {
                    return "col-md-2 col-sm-3 col-xs-6";
                }
            };
            DashboardChartBaseController.prototype.setHospitalPartner = function () {
                console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticeData();
                    }
                }
            };
            DashboardChartBaseController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                // this.data = this.getPracticeKneeBalanceAvg();
                                this.getPracticeData(); // for testing!!
                            }
                        }
                    }
                }
            };
            DashboardChartBaseController.prototype.formatDate = function (data) {
                // console.log(data);
                var dateLabel;
                dateLabel = moment(data).add(1, 'd').toDate();
                var month = moment(dateLabel).month() + 1;
                var formattedDate = moment(dateLabel).year() + '-' + month + '-02';
                // console.log(formattedDate);
                return formattedDate;
            };
            DashboardChartBaseController.prototype.ascii = function (a) { return String.fromCharCode(a); };
            return DashboardChartBaseController;
        }(dashboard.DashboardBaseController));
        DashboardChartBaseController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'moment', 'config'];
        dashboard.DashboardChartBaseController = DashboardChartBaseController;
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboardChartBase.js.map