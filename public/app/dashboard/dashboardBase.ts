module orthosensor.dashboard {

    export abstract class DashboardBaseController {

        public static $inject = ['DashboardService',  'PatientListFactory', 'UserService', 'config'];

        protected PatientService: any;
       
        public user: orthosensor.domains.User;
        
        public isUserAdmin: boolean;
       
        public dateFormat: string = 'MM/DD/YYYY';
        
        constructor(public UserService: orthosensor.services.UserService) {
            this.user = UserService.user;        
            this.isUserAdmin = UserService.isAdmin;
        }

        public showPracticeList(): boolean {
            return this.isUserAdmin || this.user.userType === 'Hospital Partner Community User' || this.user.userType === 'Partner Community User';
        }

        public showSurgeonList(): boolean {
            return this.isUserAdmin || this.user.userType === 'Surgeon Partner Community User';
        }
        $onInit(): void {
            this.user = this.UserService.user;    
            console.log(this.user);
            this.isUserAdmin = this.UserService.isAdmin;
         }
    }
}
