var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var CaseActivityChartController = (function (_super) {
            __extends(CaseActivityChartController, _super);
            // public isHospital(): boolean;
            // public $onInit(): void;
            ///// /* @ngInject */
            function CaseActivityChartController(DashboardService, ChartService, PatientListFactory, UserService, moment, config) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, moment, config) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                _this.PatientService = PatientListFactory;
                _this.title = 'Case Activity Chart';
                _this.hospitals = [];
                _this.hospitalPracticeLabel = 'Practices';
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.surgeons = [];
                _this.monthsToChart = '3';
                _this.chartHeight = DashboardService.chartHeight;
                _this.user = UserService.user;
                console.log(_this.user);
                _this.isUserAdmin = UserService.isUserAdmin;
                console.log(_this.isUserAdmin);
                return _this;
                //this.$onInit();
            }
            CaseActivityChartController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                this.hospitals = this.DashboardService.hospitals;
                // handle surgeon or practice staff
                if (this.user.userType === 'Surgeon Partner Community User') {
                    this.hospital = this.DashboardService.hospital;
                    // console.log(this.hospital);
                    this.practice.id = this.user.accountId;
                    this.practice.name = this.user.accountName;
                }
                else {
                    this.practices = this.DashboardService.practices;
                    // console.log(this.practices);
                }
                this.surgeons = this.DashboardService.surgeons;
                // console.log(this.surgeons);
                this.setDateParams();
                this.initChartParams();
                this.initData();
            };
            ;
            CaseActivityChartController.prototype.initChartParams = function () {
                console.log('Initializing chart');
                this.initChartLookups();
                this.caseActivityChart = new orthosensor.domains.ChartType();
                //let data: orthosensor.domains.ChartDataElement[];
                var service = this.ChartService;
                this.caseActivityChart = service.chart;
                // this.caseActivityChart.options.title = 'Case Activity Chart';
                this.options = this.ChartService.getMultiBarChartOptions();
                this.ChartService.chartTitle = 'Case Activity Chart';
                this.ChartService.xAxisLabel = 'Months';
                this.ChartService.xAxisLabel = 'Procedure Count';
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            CaseActivityChartController.prototype.initData = function () {
                switch (this.user.userType) {
                    case 'Surgeon Partner Community User':
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 'Hospital Partner Community User':
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    case 'Partner Community User':
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            CaseActivityChartController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getData();
                            break;
                        }
                    }
                }
            };
            CaseActivityChartController.prototype.setHospitalPartner = function () {
                console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getData();
                    }
                }
            };
            CaseActivityChartController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                if (this.surgeons !== undefined) {
                                    for (var i = 0; i < this.surgeons.length; i++) {
                                        if (this.surgeons[i].parentId === this.practice.id) {
                                            this.surgeon = this.surgeons[i];
                                            this.data = this.getData();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
            CaseActivityChartController.prototype.getPracticeData = function () {
                return [];
            };
            CaseActivityChartController.prototype.getData = function () {
                var data = [];
                var averageData = this.getAverageData();
                console.log(averageData);
                var averageChartData;
                averageChartData = {
                    "key": ["Average"],
                    "color": "#b1b9bd",
                    "values": averageData
                };
                data.push(averageChartData);
                if (this.user.userType !== 'Hospital Partner Community User' && this.user.userType !== 'Partner Community User') {
                    var surgeonChartData = void 0;
                    var surgeonData = this.getSurgeonData();
                    console.log(surgeonData);
                    surgeonChartData = {
                        "key": [this.surgeon.name],
                        "color": "#109bd6",
                        "values": surgeonData
                    };
                    data.push(surgeonChartData);
                }
                console.log(data);
                this.data = data;
                return data;
            };
            CaseActivityChartController.prototype.getAverages = function () {
                // console.log(this.practice);
                return this.getPracticeData();
            };
            CaseActivityChartController.prototype.getChart = function () {
                // console.log(surgeon);
                // console.log(this.surgeon);
                this.setDateParams();
                this.getSurgeonData();
            };
            // ctrl.$onChanges = function(changesObj) { };
            // ctrl.$onDestory = function() { };
            CaseActivityChartController.prototype.setPractice = function (practiceId) {
                if (this.user.userType === 'Hospital Partner Community User') {
                    this.getPracticeData();
                }
            };
            CaseActivityChartController.prototype.initChartLookups = function () {
                // get hospitals, surgeon 
                // get hospitals
            };
            CaseActivityChartController.prototype.validate = function () {
                return true;
            };
            CaseActivityChartController.prototype.getHospitals = function () {
                this.hospitals = this.DashboardService.getHospitals();
                return this.hospitals;
            };
            CaseActivityChartController.prototype.getHospitalPractices = function (hospitalId) {
                this.practices = this.DashboardService.practices(hospitalId);
                console.log(this.practices);
                return this.practices;
            };
            CaseActivityChartController.prototype.getSurgeons = function (hospital) {
                if (this.hospitals.length > 0) {
                    // this.surgeons = this.DashboardService.getSurgeons(hospital.Id);
                }
            };
            CaseActivityChartController.prototype.getSurgeonData = function () {
                var _this = this;
                // console.log(surgeonId);
                var items = [];
                var item;
                item = new orthosensor.domains.ChartDataElement();
                // items.push(item);
                var chartData = [];
                this.PatientService.getCaseActivityBySurgeon(this.surgeon.id, this.startDate, this.endDate)
                    .then(function (data) {
                    // console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        var dateLabel = _this.formatDate(data[i]);
                        item = { label: dateLabel, value: data[i].Total };
                        items.push(item);
                        chartData.push([dateLabel, data[i].Total]);
                    }
                    // console.log(items);
                    // this.caseActivityChart.data = chartData;
                    // console.log(this.caseActivityChart);
                    return items;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return items;
            };
            CaseActivityChartController.prototype.formatDate = function (data) {
                // console.log(data);
                // use the 2nd day of the month for a weird d3 issue
                var formattedDate = data.Year + '-' + data.Month + '-02';
                // console.log(formattedDate);
                return formattedDate;
            };
            CaseActivityChartController.prototype.getAverageData = function () {
                var _this = this;
                console.log(this.practice);
                var items = [];
                var item;
                item = new orthosensor.domains.ChartDataElement();
                // items.push(item);
                var chartData = [];
                this.PatientService.getCaseActivityByPractice(this.practice.id, this.startDate, this.endDate)
                    .then(function (data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        var dateLabel = _this.formatDate(data[i]);
                        item = { label: dateLabel, value: data[i].Total };
                        items.push(item);
                        chartData.push([dateLabel, data[i].Total]);
                    }
                    // console.log(items);
                    // this.caseActivityChart.data = chartData;
                    // console.log(this.caseActivityChart);
                    return items;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return items;
            };
            return CaseActivityChartController;
        }(dashboard.DashboardChartBaseController));
        CaseActivityChartController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'moment', 'config'];
        var CaseActivityChart = (function () {
            function CaseActivityChart() {
                this.bindings = {};
                this.controller = CaseActivityChartController;
                this.templateUrl = 'app/dashboard/caseActivityChart/caseActivityChart.component.html';
            }
            return CaseActivityChart;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osCaseActivityChart', new CaseActivityChart());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseActivityChart.component.js.map