module orthosensor.dashboard {

    interface ChartController {
        title: string;
        hospitals: orthosensor.domains.Hospital[];
        hospitalPracticeLabel: string;
        surgeonLabel: string;
        surgeons: orthosensor.domains.Surgeon[];
        chart: orthosensor.domains.ChartType;
        //   isHospital(): boolean;
        //   $onInit(): void;

    }

    class CaseActivityChartController extends DashboardChartBaseController implements ChartController {

        public static $inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'moment', 'config'];

        public title: string;
        public user: orthosensor.domains.User;
        public hospitals: orthosensor.domains.Hospital[];
        public hospitalPracticeLabel: string;
        public surgeonLabel: string;
        public chartService: orthosensor.services.ChartService;
        public caseActivityChart: orthosensor.domains.ChartType;
        public surgeons: orthosensor.domains.Surgeon[];
        public chart: orthosensor.domains.ChartType;
        public surgeon: orthosensor.domains.Surgeon;
        public hospital: orthosensor.domains.Hospital;
        public practices: orthosensor.domains.Practice[];
        public practice: orthosensor.domains.Practice;
        public options: any;
        public data: any[];
        public monthsToChart: string;
        public chartHeight: number;
        public startDate: any;
        public endDate: any;
        public isUserAdmin: boolean;
        public PatientService: any;

        // public isHospital(): boolean;
        // public $onInit(): void;

        ///// /* @ngInject */
        constructor(public DashboardService: any, public ChartService: any, PatientListFactory: any, UserService: any, moment: any, config: any) {
            super(DashboardService, ChartService, PatientListFactory, UserService, moment, config);
            this.PatientService = PatientListFactory;
            this.title = 'Case Activity Chart';
            this.hospitals = [];
            this.hospitalPracticeLabel = 'Practices';
            this.practices = [];
            this.practice = new orthosensor.domains.Practice();
            this.surgeonLabel = 'Surgeon';
            this.surgeons = [];
            this.monthsToChart = '3';
            this.chartHeight = DashboardService.chartHeight;
            this.user = UserService.user;
            console.log(this.user);
            this.isUserAdmin = UserService.isUserAdmin;
            console.log(this.isUserAdmin);
            //this.$onInit();
        }

        public $onInit(): void {
            super.$onInit();
            this.hospitals = this.DashboardService.hospitals;

            // handle surgeon or practice staff
            if (this.user.userType === 'Surgeon Partner Community User') {
                this.hospital = this.DashboardService.hospital;
                // console.log(this.hospital);
                this.practice.id = this.user.accountId;
                this.practice.name = this.user.accountName;
            } else {
                this.practices = this.DashboardService.practices;
                // console.log(this.practices);
            }

            this.surgeons = this.DashboardService.surgeons;
            // console.log(this.surgeons);
            this.setDateParams();
            this.initChartParams();
            this.initData();
        };

        public initChartParams(): void {
            console.log('Initializing chart');
            this.initChartLookups();
            this.caseActivityChart = new orthosensor.domains.ChartType();
            //let data: orthosensor.domains.ChartDataElement[];
            let service = this.ChartService;
            this.caseActivityChart = service.chart;
            // this.caseActivityChart.options.title = 'Case Activity Chart';
            this.options = this.ChartService.getMultiBarChartOptions();
            this.ChartService.chartTitle = 'Case Activity Chart';
            this.ChartService.xAxisLabel = 'Months';
            this.ChartService.xAxisLabel = 'Procedure Count';
        }

        // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
        public initData() {
            switch (this.user.userType) {
                case 'Surgeon Partner Community User':
                    // console.log('surgeon Account');
                    this.setSurgeonPartner();
                    break;

                case 'Hospital Partner Community User':
                    // console.log('hospital Account');
                    this.setHospitalPartner();
                    break;

                case 'Partner Community User':
                    // console.log('hospital Account');
                    this.setHospitalPartner();
                    break;

                default:
                    // console.log('Admin Account');
                    this.setAdmin();
            }
        }

        public setSurgeonPartner() {
            if (this.surgeons !== undefined) {
                for (let i = 0; i < this.surgeons.length; i++) {
                    if (this.surgeons[i].parentId === this.practice.id) {
                        this.surgeon = this.surgeons[i];
                        this.data = this.getData();
                        break;
                    }
                }
            }
        }

        public setHospitalPartner() {
            console.log(this.practices);
            if (this.practices !== undefined) {
                if (this.practices.length > 0) {
                    this.practice = this.practices[0];
                    this.data = this.getData();
                }
            }
        }

        public setAdmin() { 
            if (this.hospitals !== undefined) {
                if (this.hospitals.length > 0) {
                    this.hospital = this.hospitals[0];
                    if (this.practices !== undefined) {
                        if (this.practices.length > 0) {
                            this.practice = this.practices[0];
                            if (this.surgeons !== undefined) {
                                for (let i = 0; i < this.surgeons.length; i++) {
                                    if (this.surgeons[i].parentId === this.practice.id) {
                                        this.surgeon = this.surgeons[i];
                                        this.data = this.getData();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        getPracticeData(): any[] {
            return [];
        }        
        public getData(): any[] {
            let data: any[] = [];
            let averageData: any[] = this.getAverageData();
            console.log(averageData);
            let averageChartData: {};
            averageChartData = {
                "key": ["Average"],
                "color": "#b1b9bd",
                "values": averageData
            }
            data.push(averageChartData);
            if (this.user.userType !== 'Hospital Partner Community User' && this.user.userType !== 'Partner Community User' ) {
                let surgeonChartData: {};
                let surgeonData: any[] = this.getSurgeonData();
                console.log(surgeonData);
                surgeonChartData = {
                    "key": [this.surgeon.name],
                    "color": "#109bd6",
                    "values": surgeonData
                };
                data.push(surgeonChartData);
            }
            console.log(data);
            this.data = data;
            return data;

        }

        public getAverages() {
            // console.log(this.practice);
            return this.getPracticeData();
        }

        public getChart() {
            // console.log(surgeon);
            // console.log(this.surgeon);
            this.setDateParams();
            this.getSurgeonData();
        }
        // ctrl.$onChanges = function(changesObj) { };
        // ctrl.$onDestory = function() { };


        public setPractice(practiceId: string) {
            if (this.user.userType === 'Hospital Partner Community User') {
                this.getPracticeData();
            }
        }

        private initChartLookups(): void {
            // get hospitals, surgeon 
            // get hospitals
        }

        private validate(): boolean {

            return true;
        }

        private getHospitals(): orthosensor.domains.Hospital[] {
            this.hospitals = this.DashboardService.getHospitals();
            return this.hospitals;
        }

        private getHospitalPractices(hospitalId: string): orthosensor.domains.Practice[] {
            this.practices = this.DashboardService.practices(hospitalId);
            console.log(this.practices);
            return this.practices;
        }

        private getSurgeons(hospital: orthosensor.domains.Hospital) {
            if (this.hospitals.length > 0) {
                // this.surgeons = this.DashboardService.getSurgeons(hospital.Id);
            }
        }

        getSurgeonData(): any[] {
            // console.log(surgeonId);
            let items: orthosensor.domains.ChartDataElement[] = [];
            let item: orthosensor.domains.ChartDataElement;
            item = new orthosensor.domains.ChartDataElement();

            // items.push(item);
            let chartData: any[] = [];

            this.PatientService.getCaseActivityBySurgeon(this.surgeon.id, this.startDate, this.endDate)
                .then((data: any) => {
                    // console.log(data);

                    for (let i = 0; i < data.length; i++) {
                        let dateLabel: string = this.formatDate(data[i]);
                        item = { label: dateLabel, value: data[i].Total };
                        items.push(item);
                        chartData.push([dateLabel, data[i].Total]);
                    }
                    // console.log(items);
                    // this.caseActivityChart.data = chartData;
                    // console.log(this.caseActivityChart);
                    return items;
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return items;
        }

        protected formatDate(data: any): string {
            // console.log(data);
            // use the 2nd day of the month for a weird d3 issue
            let formattedDate = data.Year + '-' + data.Month + '-02';
            // console.log(formattedDate);
            return formattedDate;
        }

        private getAverageData(): any[] {
            console.log(this.practice);
            let items: orthosensor.domains.ChartDataElement[] = [];
            let item: orthosensor.domains.ChartDataElement;
            item = new orthosensor.domains.ChartDataElement();

            // items.push(item);
            let chartData: any[] = [];

            this.PatientService.getCaseActivityByPractice(this.practice.id, this.startDate, this.endDate)
                .then((data: any) => {
                     console.log(data);

                    for (let i = 0; i < data.length; i++) {
                        let dateLabel: string = this.formatDate(data[i]);
                        item = { label: dateLabel, value: data[i].Total };
                        items.push(item);
                        chartData.push([dateLabel, data[i].Total]);
                    }
                    // console.log(items);

                    // this.caseActivityChart.data = chartData;
                    // console.log(this.caseActivityChart);
                    return items;
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return items;
        }

    }
    class CaseActivityChart implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                // textBinding: '@',
                // dataBinding: '<',
                // functionBinding: '&'
            };
            this.controller = CaseActivityChartController;
            this.templateUrl = 'app/dashboard/caseActivityChart/caseActivityChart.component.html';
        }

    }
    angular
        .module('orthosensor.dashboard')
        .component('osCaseActivityChart', new CaseActivityChart());
}
