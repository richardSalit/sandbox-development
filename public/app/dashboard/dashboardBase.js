var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var DashboardBaseController = (function () {
            function DashboardBaseController(UserService) {
                this.UserService = UserService;
                this.dateFormat = 'MM/DD/YYYY';
                this.user = UserService.user;
                this.isUserAdmin = UserService.isAdmin;
            }
            DashboardBaseController.prototype.showPracticeList = function () {
                return this.isUserAdmin || this.user.userType === 'Hospital Partner Community User' || this.user.userType === 'Partner Community User';
            };
            DashboardBaseController.prototype.showSurgeonList = function () {
                return this.isUserAdmin || this.user.userType === 'Surgeon Partner Community User';
            };
            DashboardBaseController.prototype.$onInit = function () {
                this.user = this.UserService.user;
                console.log(this.user);
                this.isUserAdmin = this.UserService.isAdmin;
            };
            return DashboardBaseController;
        }());
        DashboardBaseController.$inject = ['DashboardService', 'PatientListFactory', 'UserService', 'config'];
        dashboard.DashboardBaseController = DashboardBaseController;
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboardBase.js.map