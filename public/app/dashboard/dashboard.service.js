var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var DashboardService = (function () {
            /* @ngInject */
            function DashboardService(PatientListFactory, SurveyService, $q) {
                this.PatientService = PatientListFactory;
                this.SurveyService = SurveyService;
                this.hospitals = this.getHospitals();
                this.chartHeight = 280;
                this.$q = $q;
                this.surveys = new Array();
                // this.dashboardService = IQ_DashboardRepository;
            }
            Object.defineProperty(DashboardService.prototype, "practices", {
                // private dashboardService: any;
                get: function () {
                    return this._practices;
                },
                set: function (newPractices) {
                    this._practices = newPractices;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "surgeons", {
                get: function () {
                    return this._surgeons;
                },
                set: function (newSurgeons) {
                    this._surgeons = newSurgeons;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "hospitals", {
                get: function () {
                    return this._hospitals;
                },
                set: function (newHospitals) {
                    this._hospitals = newHospitals;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "hospital", {
                get: function () {
                    return this._hospital;
                },
                set: function (newHospital) {
                    this._hospital = newHospital;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "surveys", {
                get: function () {
                    return this._surveys;
                },
                set: function (newSurvey) {
                    this._surveys = newSurvey;
                },
                enumerable: true,
                configurable: true
            });
            DashboardService.prototype.getHospitals = function () {
                var _this = this;
                // console.log(this.hospitals);
                if (this.hospitals !== undefined && this.hospitals.length > 0) {
                    return this.hospitals;
                }
                else {
                    return this.PatientService.loadHospitals()
                        .then(function (data) {
                        _this.hospitals = data;
                        // console.log(this.hospitals);
                        return _this.hospitals;
                    }, function (error) {
                        console.log(error);
                    });
                }
            };
            DashboardService.prototype.loadHospitals = function () {
                var _this = this;
                this.PatientService.loadHospitals()
                    .then(function (data) {
                    _this._hospitals = _this.convertSFHospitalToObject(data);
                    console.log(_this.hospitals);
                }, function (error) {
                    console.log(error);
                });
            };
            DashboardService.prototype.loadPractices = function (hospitalId) {
                var _this = this;
                this.PatientService.loadPractices(hospitalId)
                    .then(function (data) {
                    console.log(data);
                    _this._practices = _this.convertSFPracticeToObject(data);
                    console.log(_this.practices);
                    // return this.practices;
                }, function (error) {
                    console.log(error);
                });
                // return this.practices;
            };
            DashboardService.prototype.loadHospitalPractices = function (hospitalId) {
                var _this = this;
                this.PatientService.getHospitalPractices(hospitalId)
                    .then(function (data) {
                    console.log(data);
                    _this.practices = _this.convertSFPracticeToObject(data);
                    console.log(_this.practices);
                    // return this.practices;
                }, function (error) {
                    console.log(error);
                });
                // return this.practices;
            };
            DashboardService.prototype.getSurgeons = function (hospitalId) {
                return this.surgeons;
            };
            DashboardService.prototype.setSurgeons = function (hospitalId) {
                var _this = this;
                var surgeonData;
                // console.log(hospitalId);
                if (hospitalId !== null) {
                    this.PatientService.loadSurgeons(hospitalId)
                        .then(function (data) {
                        var surg = data;
                        // console.log(surg);
                        for (var i = 0; i < surg.length; i++) {
                            var surgeonR = surg[i].Surgeon__r;
                            var surgeonD = void 0;
                            // surgeonD = surgeonModule.Surgeon;
                            // console.log(surgeonR);
                            // surgeonD.id = surg[i].Surgeon__r.Id;
                            // surgeonD.name = surgeonR.Name;
                            // surgeonData.push(surgeonD);
                        }
                        _this.surgeons = surgeonData;
                    }, function (error) {
                        console.log(error);
                    });
                    // return surgeonData;
                }
            };
            DashboardService.prototype.loadAllSurgeons = function () {
                var _this = this;
                this.PatientService.loadAllSurgeons()
                    .then(function (data) {
                    // console.log(data);
                    _this.surgeons = _this.convertSFSurgeonToObject(data);
                }, function (error) {
                    console.log(error);
                });
                // return surgeonData;
            };
            DashboardService.prototype.loadHospitalPracticeSurgeons = function (hospitalPracticeId) {
                var _this = this;
                // let surgeonData: orthosensor.domains.Surgeon[];
                // console.log(hospitalPracticeId);
                if (hospitalPracticeId !== null) {
                    this.PatientService.loadHospitalPracticeSurgeons(hospitalPracticeId)
                        .then(function (data) {
                        _this.surgeons = _this.convertSFSurgeonToObject(data);
                        console.log(_this.surgeons);
                    }, function (error) {
                        console.log(error);
                    });
                    // return surgeonData;
                }
            };
            ;
            DashboardService.prototype.loadHospitalByPractice = function (hospitalPracticeId) {
                var _this = this;
                // let surgeonData: orthosensor.domains.Surgeon[];
                // console.log(hospitalPracticeId);
                if (hospitalPracticeId !== null) {
                    this.PatientService.getHospitalByPractice(hospitalPracticeId)
                        .then(function (data) {
                        // console.log(data);
                        _this.hospital = {
                            id: data.Id,
                            name: data.Name,
                            recordTypeId: 'HospitalAccount'
                        };
                        // console.log(this.hospital);
                    }, function (error) {
                        console.log(error);
                    });
                    // return surgeonData;
                }
            };
            DashboardService.prototype.getCaseActivityByPractice = function (practiceId, startDate, endDate) {
                this.PatientService.getCaseActivityByPractice(practiceId, startDate, endDate)
                    .then(function (data) {
                    var results = data;
                    // console.log(results);
                    return results;
                    // this.surgeons = surgeonData;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return [];
            };
            DashboardService.prototype.setSurveys = function () {
                var _this = this;
                this.surveys = [];
                this.SurveyService.loadSurveys()
                    .then(function (data) {
                    for (var i = 0; i <= data.length; i++) {
                        if (data[i] !== undefined) {
                            var survey = new orthosensor.domains.Survey();
                            survey.id = data[i].Id;
                            survey.name = data[i].Name__c;
                            // console.log(survey);
                            _this.surveys.push(survey);
                        }
                    }
                }, function (error) {
                    console.log(error);
                    // return error;
                });
            };
            DashboardService.prototype.convertSFHospitalToObject = function (data) {
                var hospitals = [];
                // console.log(data.length);
                for (var i = 0; i < data.length; i++) {
                    var hospital = {
                        id: data[i].Id,
                        name: data[i].Name,
                    };
                    // console.log(practice);
                    hospitals.push(hospital);
                }
                // console.log(hospitals);
                return hospitals;
            };
            DashboardService.prototype.convertSFPracticeToObject = function (data) {
                var practices;
                practices = [];
                // console.log(data.length);
                for (var i = 0; i < data.length; i++) {
                    var practice = {
                        id: data[i].Id,
                        name: data[i].Name,
                        parentId: data[i].ParentId,
                        parentName: '' //data[i].Parent.Name
                    };
                    // console.log(practice);
                    practices.push(practice);
                }
                // console.log(practices);
                return practices;
            };
            DashboardService.prototype.convertSFSurgeonToObject = function (data) {
                var surgeons;
                surgeons = [];
                for (var i = 0; i < data.length; i++) {
                    var surgeon = {
                        id: data[i].Surgeon__r.AccountId,
                        name: data[i].Surgeon__r.Name,
                        parentId: data[i].Hospital__c,
                        parentName: '' //data[i].Parent.Name //do we need this?
                    };
                    surgeons.push(surgeon);
                }
                // console.log(surgeons);
                return surgeons;
            };
            return DashboardService;
        }());
        services.DashboardService = DashboardService;
        DashboardService.$inject = ['PatientListFactory', 'SurveyService', '$q'];
        angular
            .module('orthosensor.dashboard')
            .service('DashboardService', DashboardService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboard.service.js.map