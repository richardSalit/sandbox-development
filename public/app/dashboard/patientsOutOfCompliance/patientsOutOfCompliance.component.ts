module orthosensor.dashboard {

    class PatientsOutOfComplianceController extends DashboardBaseController {
        public static $inject = ['DashboardService', 'PatientListFactory', 'UserService', 'moment'];
        public title: string;
        // public user: orthosensor.domains.User;
        public hospitals: orthosensor.domains.Hospital[];
        public hospitalPracticeLabel: string;
        public surgeonLabel: string;
        public surgeons: orthosensor.domains.Surgeon[];
        public surgeon: orthosensor.domains.Surgeon;
        public hospital: orthosensor.domains.Hospital;
        public practices: orthosensor.domains.Practice[];
        public practice: orthosensor.domains.Practice;
        public options: any;
        public data: any[];
        
        // public isUserAdmin: boolean;
        public patientsOutOfCompliance: orthosensor.domains.PatientsOutOfCompliance[];
        public PatientService: any;
        
        ///// /* @ngInject */
        constructor(public DashboardService: any, PatientListFactory: any, UserService: any, moment: any, config: any) {
            super( UserService );
            this.PatientService = PatientListFactory;
            this.title = 'Patients out of compliance';
            this.hospitals = [];
            this.hospitalPracticeLabel = 'Practices';
            this.practices = [];
            this.practice = new orthosensor.domains.Practice();
            this.surgeonLabel = 'Surgeon';
            this.surgeons = [];
            this.user = UserService.user;
            console.log(this.user);
            this.isUserAdmin = UserService.isUserAdmin;
        }

        public $onInit(): void {
            super.$onInit();
            // console.log(this);
            // this.user = this.UserService.user;
            console.log(this.user);
            this.hospitals = this.DashboardService.hospitals;

            // handle surgeon or practice staff
            // if (this.user.userType === 'Surgeon Partner Community User') {
            //     this.hospital = this.DashboardService.hospital;
            //     // console.log(this.hospital);
            //     this.practice.id = this.user.accountId;
            //     this.practice.name = this.user.accountName;
            // } else {
            //     // console.log(this.hospitals);
            //     this.practices = this.DashboardService.practices;
            //     // console.log(this.practices);
            // }

            this.surgeons = this.DashboardService.surgeons;
            // console.log(this.surgeons);
            this.patientsOutOfCompliance = [];
            let patient1:
                orthosensor.domains.PatientsOutOfCompliance = new orthosensor.domains.PatientsOutOfCompliance();
            patient1.patientName = 'Fredericka Winfield';
            patient1.phaseName = 'Procedure';
            patient1.issue = 'Incomplete';
            patient1.lastContactDate = new Date();
            this.patientsOutOfCompliance.push(patient1);
            let patient2:
                orthosensor.domains.PatientsOutOfCompliance = new orthosensor.domains.PatientsOutOfCompliance();
            patient2.patientName = 'Jane Hart';
            patient2.phaseName = '4 - 8 week';
            patient2.issue = 'Survey Missing';
            patient2.lastContactDate = new Date();
            this.patientsOutOfCompliance.push(patient2);
            let patient3:
                orthosensor.domains.PatientsOutOfCompliance = new orthosensor.domains.PatientsOutOfCompliance();
            patient3.patientName = 'James Johnson';
            patient3.phaseName = 'Pre-Op';
            patient3.issue = 'Survey Missing';
            patient3.lastContactDate = new Date();
            this.patientsOutOfCompliance.push(patient3);
             let patient4:
                orthosensor.domains.PatientsOutOfCompliance = new orthosensor.domains.PatientsOutOfCompliance();
            patient4.patientName = 'Midred Ginger';
            patient4.phaseName = 'Pre-Op';
            patient4.issue = 'Survey Missing';
            patient4.lastContactDate = new Date();
            this.patientsOutOfCompliance.push(patient4);
            console.log(this.patientsOutOfCompliance);
        };

        public isHospital(): boolean {
            return true;
        }

        // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
        public initData() {
            switch (this.user.userType) {
                case 'Surgeon Partner Community User':
                    console.log('surgeon Account');
                    this.setSurgeonPartner();
                    break;

                case 'Hospital Partner Community User':
                    console.log('hospital Account');
                    this.setHospitalPartner();
                    break;

                case 'Partner Community User':
                    console.log('practice Account');
                    this.setHospitalPartner();
                    break;

                default:
                    console.log('Admin Account');
                    this.setAdmin();
            }
        }

        public setSurgeonPartner() {
            if (this.surgeons !== undefined) {
                for (let i = 0; i < this.surgeons.length; i++) {
                    if (this.surgeons[i].parentId === this.practice.id) {
                        this.surgeon = this.surgeons[i];
                        this.data = this.getSurgeonData();
                        break;
                    }
                }
            }
        }

        public setHospitalPartner() {
            // console.log(this.practices);
            if (this.practices !== undefined) {
                if (this.practices.length > 0) {
                    this.practice = this.practices[0];
                    this.data = this.getPracticeData();
                }
            }
        }

        public setAdmin() {
            // console.log(this.hospitals);
            // console.log(this.practices);
            // console.log(this.surgeons);
            if (this.hospitals !== undefined) {
                if (this.hospitals.length > 0) {
                    this.hospital = this.hospitals[0];
                    if (this.practices !== undefined) {
                        if (this.practices.length > 0) {
                            this.practice = this.practices[0];
                            if (this.surgeons !== undefined) {
                                for (let i = 0; i < this.surgeons.length; i++) {
                                    if (this.surgeons[i].parentId === this.practice.id) {
                                        this.surgeon = this.surgeons[i];
                                        this.data = this.getSurgeonData();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        public getSurgeonData(): any[] {
            return this.getSurgeonCompliance(this.surgeon.id);
        }

        public getPracticeData(): any[] {
            return this.getPracticeCompliance(this.practice.id);
        }

       
        private validate(): boolean {
            return true;
        }


        private getSurgeonCompliance(surgeonId: string): any[] {
            let surgeon: string = this.surgeon.id;

            
            return [];

        }

        private getPracticeCompliance(practiceId: string): orthosensor.domains.PhaseCount[] {
            let practice: string = this.practice.id;
           
            return [];

        }

        // convert to object - dates come in last day of the prior month
        private convertSFComplianceToObject(data: any[]): orthosensor.domains.PhaseCount[] {
           
           
            return [];
        }

        private createPatientComplianceRecord(id: string, name: string, toDate: Date, sDate: string, surgeonId: string, practiceId: string, count: number): orthosensor.domains.PatientsOutOfCompliance {
            let patient: orthosensor.domains.PatientsOutOfCompliance = new orthosensor.domains.PatientsOutOfCompliance();
            // phaseCount.id = id;
            // phaseCount.phase = name;
            // phaseCount.startDate = toDate;
            // phaseCount.startDateString = sDate;
            // phaseCount.surgeonId = surgeonId;
            // phaseCount.practiceId = practiceId;
            // phaseCount.count = count;
            // console.log(phaseCount);
            return patient;
        }
    }

    class PatientsOutOfCompliance implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                chartType: '@'
            };
            this.controller = ["DashboardService", "PatientListFactory", "UserService", "moment", "config", (DashboardService: any, ChartService: any, PatientListFactory: any, UserService: any, moment: any, $filter: any, $scope: any, config: any) => {
                return new PatientsOutOfComplianceController(DashboardService, PatientListFactory, UserService, moment, config);
            }];
            this.templateUrl = 'app/dashboard/patientsOutOfCompliance/patientsOutOfCompliance.component.html';
        }
    }
    angular
        .module('orthosensor.dashboard')
        .component('osPatientsOutOfCompliance', new PatientsOutOfCompliance());

    
}    
