module orthosensor.dashboard {

    class DashboardController {
        public static $inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state'];
        public title: string;
        public patientsUnderBundleTitle: string;
        public patients: any[];
        public patientsUnderBundleCount: number;
        public DashboardDataService: any;
        public PatientsUnderBundleService: any;
        public patientsScheduled: number;
        public isAdmin: Boolean;
        public logo: any;
        public standardComponentSize: string;

        public PatientService: any;
        public $state: any;
        constructor(private DashboardService: any, DashboardDataService: any, PatientsUnderBundleService: any, PatientService: any, public UserService: orthosensor.services.UserService, $state: any) {
            this.DashboardDataService = DashboardDataService;
            this.PatientService = PatientService;
            this.PatientsUnderBundleService = PatientsUnderBundleService;
            this.$state = $state;
            this.patientsUnderBundleTitle = 'Patients Under Bundle';
            // this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
        }
        public $onInit(): void {
            let practiceId: string = '0017A00000M803ZQAR';
            this.isAdmin = this.UserService.isAdmin;
            console.log(this.isAdmin);
            this.standardComponentSize = 'col-sm-4 col-xs-12';
            if (!this.isAdmin) {
                practiceId = this.UserService.user.accountId;
                //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
            
                this.DashboardDataService.getScheduledPatients(practiceId, 7)
                    .then((data: any) => {
                        if (data !== null) {
                            this.patientsScheduled = data.length;
                        } else {
                            this.patientsScheduled = 0;
                        }   
                        console.log(this.patients);
                    }, (error: any) => {
                        console.log(error);
                        return error;
                    });
                this.DashboardDataService.getPatientsUnderBundle(practiceId)
                    .then((data: any) => {
                        if (data !== null) {
                            this.patientsUnderBundleCount = data.length;
                        } else {
                            this.patientsUnderBundleCount = 0;
                        }   
                        console.log(this.patientsUnderBundleCount);
                    }, (error: any) => {
                        console.log(error);
                        return error;
                    });
            }
        }
    }
    class Dashboard implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                practiceId: '@',
                // dataBinding: '<',
                // functionBinding: '&'
            };
            this.controller = DashboardController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/dashboard.component.html';
        }

    }
    angular
        .module('orthosensor.dashboard')
        .component('osDashboard', new Dashboard());
}