(function () {
    angular
        .module("orthosensor.dashboard")
        .component("promOutcome", {
        template: '<div class="panel panel-default"><div class="panel-heading"><div class="panel-title">PROM Outcome</div></div><div class="panel-body"><div google-chart chart="vm.promOutcomeChart"></div></div></div>',
        controllerAs: 'vm',
        controller: function () {
            var vm = this;
            vm.promOutcomeChart = {};
            vm.promOutcomeChart.type = "LineChart";
            vm.promOutcomeChart.data = {
                "cols": [
                    { id: "t", label: "Topping", type: "string" },
                    { id: "s", label: "Scores", type: "number" }
                ], "rows": [
                    {
                        c: [
                            { v: "3 month " },
                            { v: 5 },
                        ]
                    },
                    {
                        c: [
                            { v: "6 month" },
                            { v: 10 },
                        ]
                    }
                ]
            };
            vm.promOutcomeChart.options = {
                'title': 'PROM Outcome',
                "legend": "none",
                "displayExactValues": true,
                "fill": 20,
                "animation": {
                    "duration": 1000,
                    "easing": "out"
                },
                "tooltip": { "isHtml": true },
                "is3D": true,
                "vAxis": { "title": "Scores", "gridlines": { "count": 6 } },
                "chartArea": { "left": 20, "top": 10, "bottom": 20, "height": "100%" }
            };
        }
    });
})();
