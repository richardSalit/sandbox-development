module orthosensor.dashboard {

    class MeanChangeInPromScoreController extends DashboardChartBaseController {

        public static $inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'moment', 'config', 'DashboardDataService', 'PromDeltaService'];
        public title: string;
        public user: orthosensor.domains.User;
        public hospitals: orthosensor.domains.Hospital[];
        public hospitalPracticeLabel: string;
        public surgeonLabel: string;
        public chartService: orthosensor.services.ChartService;
        public DashboardDataService: orthosensor.services.DashboardDataService;
        public PromDeltaService: orthosensor.dashboard.PromDeltaService;
        public promDeltaChart: orthosensor.domains.ChartType;
        // public surgeons: orthosensor.domains.Surgeon[];
        public chart: orthosensor.domains.ChartType;
        // public surgeon: orthosensor.domains.Surgeon;
        public hospital: orthosensor.domains.Hospital;
        public practices: orthosensor.domains.Practice[];
        public practice: orthosensor.domains.Practice;
        public options: any;
        public data: any[];
        public monthsToChart: string;
        public chartHeight: number;
        
        public startMonth: number;
        public startYear: number;
        
        public chartData: any[];
        public promDeltas: orthosensor.domains.PromDelta[];
        public surgeonSelectClass: string;
        public practiceSelectClass: string;
        public promSelectClass: string;
        public dateRangeSelectClass: string;
        public hospitalSelectClass: string;
        public surveys: any[];
        public prom: {};
        public allData: orthosensor.domains.KneeBalanceAvg[];
        public chartType: string;
        public eventTypes: string[];
        public responseTypes: string[];
        public lineColors: string[];

        // private PatientService: any;
        private UserService: any;
        private promDeltaService: any;

        ///// /* @ngInject */
        // tslint:disable-next-line:max-line-length
        constructor(public DashboardService: any, public ChartService: any, PatientListFactory: any, UserService: any, moment: any, config: any, DashboardDataService: orthosensor.services.DashboardDataService, PromDeltaService: any) {
            super(DashboardService, ChartService, PatientListFactory, UserService, moment, config);
            // this.PatientService = PatientListFactory;
            this.title = 'PROM score deltas';
            this.DashboardDataService = DashboardDataService;
            this.hospitals = [];
            this.practices = [];
            this.practice = new orthosensor.domains.Practice();
            this.surgeonLabel = 'Surgeon';
            this.surgeons = [];
            this.chartHeight = DashboardService.chartHeight;
            this.user = UserService.user;
            this.promDeltaService = PromDeltaService;

            // this.$onInit();
        }

        public $onInit(): void {
            super.$onInit();
            console.log(this.surgeons);

            this.surveys = this.DashboardService.surveys;
            
            this.promDeltas = [];
            this.setDateParams();
            this.initChartParams();
            console.log(this.surveys);
            
            // for now - only KOOS!
            for (let i = 0; i <= this.surveys.length; i++) {
                // console.log(i);
                if (this.surveys[i]) {
                    if (this.surveys[i].name === 'KOOS') {
                        this.prom = this.surveys[i];
                        break; 
                    }
                }    
            }
            
            this.initData();
        }

        getProms(): any[] {
            return [];

        }
        public showPracticeList(): boolean {
            return this.isUserAdmin || this.user.userType === 'Hospital Partner Community User';
        }

        public showSurgeonList(): boolean {
            return this.isUserAdmin || this.user.userType === 'Surgeon Partner Community User';
        }

        public initChartParams(): void {
            this.initChartLookups();
            this.promDeltaChart = new orthosensor.domains.ChartType();
            let service = this.ChartService;
            this.promDeltaChart = service.chart;
            this.options = this.ChartService.getLineChartOptions();
            this.ChartService.chartTitle = 'Prom Delta';
            this.ChartService.yAxisLabel = 'Score';
            this.ChartService.xAxisLabel = 'Events';
            this.setSelectSizes();
            // these values are being hard coded for KOOS and current event types
            this.eventTypes = this.promDeltaService.eventTypes;
            this.responseTypes = ['KOOS Symptoms', 'KOOS ADL', 'KOOS Sport/Rec', 'KOOS Pain', 'KOOS QOL'];
            this.lineColors = ['#ff7f0e', 'green', 'blue', '#787878', 'red'];
        }

        // initializes data to first practice and surgeon (do we want to save after leaving and returning?)
        public initData() {

            switch (this.user.userType) {
                case 'Surgeon Partner Community User':
                    // console.log('surgeon Account');
                    this.setSurgeonPartner();
                    break;

                case 'Hospital Partner Community User':
                    // console.log('hospital Account');
                    this.setHospitalPartner();
                    break;

                default:
                    // console.log('Admin Account');
                    this.setAdmin();
            }
        }

        public setSurgeonPartner() {
            if (this.surgeons !== undefined) {
                for (let i = 0; i <= this.surgeons.length; i++) {
                    if (this.surgeons[i].parentId === this.practice.id) {
                        this.surgeon = this.surgeons[i];
                        this.data = this.getSurgeonData();
                        break;
                    }
                }
            }
        }

        setSelectSizes(): void {
            this.surgeonSelectClass = this.getSurgeonPartnerSurgeonSelectClass();
            // console.log(this.surgeonSelectClass);
            this.practiceSelectClass = this.getHospitalPartnerPracticeSelectClass();
            this.hospitalSelectClass = this.getHospitalPartnerPracticeSelectClass();
            this.promSelectClass = 'col-md-2 col-sm-3 col-xs-4';
            this.dateRangeSelectClass = 'col-md-1 col-sm-2 col-xs-3';
        }
        

        getSurgeonChart(): void {
            this.getSurgeonData();
        }      

        public createChartObject(key: string, color: string): {} {
            let getChartValuesForType: any[] = this.getChartValues(key);

            let chartObj = {
                'key': [key],
                'color': color,
                'values': this.getChartValues(key)
            }
            // console.log(chartObj);
            return chartObj;
        }

        public getChartValues(key: string): any[] {
            let values: any[] = [];
            // console.log(this.promDeltas);
            if (this.promDeltas.length > 0) {
                for (let i = 0; i <= this.promDeltas.length; i++) {
                    // console.log(this.promDeltas[i]);
                    for (let j = 0; j <= this.eventTypes.length; j++) {
                        if (this.promDeltas[i] !== undefined) {
                            if (this.promDeltas[i].stage === this.eventTypes[j]) {
                                if (this.promDeltas[i].section === key) {
                                    let point: {} = { x: j, y: this.promDeltas[i].score };
                                    // console.log(point);
                                    values.push(point);
                                    // console.log(values);
                                }
                            }
                        } else {
                            // console.log(this.promDeltas[i]);
                        }
                    }
                }
                return values;
            }

            // console.log(values);
            return values;
        }

        public getChartData(): any[] {
            // console.log(this.promDeltas);
            this.chartData = [];
            // let chartData: any[] = [];
            for (let i = 0; i <= this.responseTypes.length; i++) {
                // console.log('Response Type' + i);
                this.chartData.push(this.createChartObject(this.responseTypes[i], this.lineColors[i]));
            }

            console.log(this.chartData);
            return this.chartData;
        }

        getPracticeData(): orthosensor.domains.PromDelta[] {
            let practice: string = this.practice.id;

            // loop through months
            console.log(this.monthDiff);
            let startMonth = moment(this.startDate).month() + 1;
            let startYear = moment(this.startDate).year();
            //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
            let endMonth = moment(this.endDate).month() + 1;
            let endYear = moment(this.endDate).year();
            console.log(this.practice);
            this.DashboardDataService.getAggregatePromDeltasByPractice(this.practice.id, startMonth, 0, 0, 0)
                .then((data: any) => {
                    let result = data;
                    console.log(result);
                    if (result !== null) {
                        this.promDeltas = this.promDeltaService.convertDataToObjects(result);
                        console.log(this.promDeltas);
                        this.getChartData();
                    }
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return this.promDeltas;
        }

        getSurgeonData(): orthosensor.domains.PromDelta[] {
            // let surgeon: string = this.surgeon.id;
            console.log(this.monthDiff);
            let startMonth = moment(this.startDate).month() + 1;
            let startYear = moment(this.startDate).year();
            //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
            let endMonth = moment(this.endDate).month() + 1;
            let endYear = moment(this.endDate).year();
            console.log(this.surgeon);
            if (this.surgeon !== null) {
                // let surgeon = this.surgeon.id;
                this.DashboardDataService.getAggregatePromDeltasBySurgeon(this.surgeon.id, 0, 0, 0, 0)
                    .then((data: any) => {
                        let result = data;
                        console.log(result);
                        if (result !== null) {
                            this.promDeltas = this.promDeltaService.convertDataToObjects(result);
                            console.log(this.promDeltas);
                            this.getChartData();
                        }
                    }, (error: any) => {
                        console.log(error);
                        return error;
                    });
            }
            return this.promDeltas;
        }

        private initChartLookups(): void {
            // get hospitals, surgeon 
            // get hospitals
        }

        private validate(): boolean {
            return true;
        }

        private formatDate(data: any): string {
            // console.log(data);
            let dateLabel: Date;
            dateLabel = moment(data).add(1, 'd').toDate();
            let month: number = moment(dateLabel).month() + 1;
            let formattedDate = moment(dateLabel).year() + '-' + month + '-02';
            // console.log(formattedDate);
            return formattedDate;
        }
    }

    angular
        .module('orthosensor.dashboard')
        .component('osMeanChangeInPromScore', {
            controller: MeanChangeInPromScoreController,
            controllerAs: '$ctrl',
            templateUrl: 'app/dashboard/meanChangeInProm/meanChangeInPromScore.component.html',
            bindings: {
                chartType: '@',
                chartType2: '@'
            }
        });
}

