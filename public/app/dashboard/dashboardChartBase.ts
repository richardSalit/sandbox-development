module orthosensor.dashboard {

    export abstract class DashboardChartBaseController extends DashboardBaseController {

        public static $inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'moment', 'config'];

        protected PatientService: any;

        public hospitals: orthosensor.domains.Hospital[];
        public hospital: orthosensor.domains.Hospital;
        public practices: orthosensor.domains.Practice[];
        public practice: orthosensor.domains.Practice;
        public surgeon: orthosensor.domains.Surgeon;
        public surgeons: orthosensor.domains.Surgeon[];

        public isUserAdmin: boolean;
        public monthsToChart: string;
        public monthDiff: number;
        public startDate: any;
        public endDate: any;
        public dateFormat: string = 'MM/DD/YYYY';
        public data: any[];
        public chartData: any[];
        public surgeonSelectClass: string;
        public practiceSelectClass: string;

        constructor(public DashboardService: orthosensor.services.DashboardService, ChartService: any, PatientListFactory: any,
            public UserService: orthosensor.services.UserService, moment: any, config: any) {
            super(UserService);

            this.PatientService = PatientListFactory;
        }
        $onInit(): void {
            super.$onInit();
            this.hospitals = this.DashboardService.hospitals;
            this.monthsToChart = '3';

            // handle surgeon or practice staff
            if (this.user.userType === 'Surgeon Partner Community User') {
                this.hospital = this.DashboardService.hospital;
                this.practice.id = this.user.accountId;
                this.practice.name = this.user.accountName;
            } else {
                console.log(this.hospitals);
                this.practices = this.DashboardService.practices;
                console.log(this.practices);
            }

            this.surgeons = this.DashboardService.surgeons;
            if (this.surgeons && this.surgeons.length) {
                this.surgeon = this.surgeons[0];
            }
            this.surgeonSelectClass = this.getSurgeonPartnerSurgeonSelectClass();
            console.log(this.surgeonSelectClass);
            this.practiceSelectClass = this.getHospitalPartnerPracticeSelectClass();
        }

        setDateParams(): void {
            let today: Date = moment();
            // console.log(today);
            let endDate: Date = moment(today).add((Number(this.monthsToChart) + 1), 'months');
            //  console.log(endDate);
            let sEndDate: string = moment(endDate).format(this.dateFormat);
            // console.log(sEndDate);
            let startMonth: Date = moment(today).subtract((Number(this.monthsToChart) - 1), 'months');
            let startDate: Date = moment(startMonth).startOf('month');
            this.monthDiff = moment(endDate).diff(startDate, 'months');
            // console.log(this.monthDiff);
            this.startDate = moment(startDate).format(this.dateFormat);
            this.endDate = moment(endDate).format(this.dateFormat);
            // console.log(this.startDate);
        }

        abstract initChartParams(): void;
        abstract initData(): void;
        abstract getPracticeData(): any[];
        abstract getSurgeonData(): any[]
        public showPracticeList(): boolean {
            return this.isUserAdmin || this.user.userType === 'Hospital Partner Community User';
        }
        getSurgeonPartnerSurgeonSelectClass(): string {
            if (this.user.userType === 'Surgeon Partner Community User') {
                return "col-md-4 col-sm-6 col-xs-10";
            } else {
                return "col-md-2 col-sm-3 col-xs-6";
            }
        }

        getHospitalPartnerPracticeSelectClass(): string {
            if (this.user.userType === 'Hospital Partner Community User') {
                return "col-md-4 col-sm-6 col-xs-10";
            } else {
                return "col-md-2 col-sm-3 col-xs-6";
            }
        }

        public setHospitalPartner() {
            console.log(this.practices);
            if (this.practices !== undefined) {
                if (this.practices.length > 0) {
                    this.practice = this.practices[0];
                    this.data = this.getPracticeData();
                }
            }
        }

        public setAdmin() {
            if (this.hospitals !== undefined) {
                if (this.hospitals.length > 0) {
                    this.hospital = this.hospitals[0];
                    if (this.practices !== undefined) {
                        if (this.practices.length > 0) {
                            this.practice = this.practices[0];
                            // this.data = this.getPracticeKneeBalanceAvg();
                            this.getPracticeData(); // for testing!!
                        }
                    }
                }
            }
        }
        protected formatDate(data: any): string {
            // console.log(data);
            let dateLabel: Date;
            dateLabel = moment(data).add(1, 'd').toDate();
            let month: number = moment(dateLabel).month() + 1;
            let formattedDate = moment(dateLabel).year() + '-' + month + '-02';
            // console.log(formattedDate);
            return formattedDate;
        }
        ascii(a: number) { return String.fromCharCode(a); }
    }
}
