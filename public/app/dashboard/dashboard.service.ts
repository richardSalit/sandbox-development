module orthosensor.services {

    export class DashboardService {
        // public static $inject = ['PatientListFactory', 'SurveyService', '$q']; 
            
        public chartHeight: number;
        private _hospitals: orthosensor.domains.Hospital[];
        private _hospital: orthosensor.domains.Hospital;
        private PatientService: any;
        private SurveyService: orthosensor.services.SurveyService;
        private _surgeons: orthosensor.domains.Surgeon[];
        private _practices: orthosensor.domains.Practice[];
        private _surveys: Array<orthosensor.domains.Survey>;
        private $q: ng.IQService;
        // private dashboardService: any;

        get practices(): orthosensor.domains.Practice[] {
            return this._practices;
        }

        set practices(newPractices: orthosensor.domains.Practice[]) {
            this._practices = newPractices;
        }

        get surgeons(): orthosensor.domains.Surgeon[] {
            return this._surgeons;
        }

        set surgeons(newSurgeons: orthosensor.domains.Surgeon[]) {
            this._surgeons = newSurgeons;
        }

        get hospitals(): orthosensor.domains.Hospital[] {
            return this._hospitals;
        }
        set hospitals(newHospitals: orthosensor.domains.Hospital[]) {
            this._hospitals = newHospitals;
        }
        get hospital(): orthosensor.domains.Hospital {
            return this._hospital;
        }
        set hospital(newHospital: orthosensor.domains.Hospital) {
            this._hospital = newHospital;
        }
        get surveys(): Array<orthosensor.domains.Survey> {
            return this._surveys;
        }
        set surveys(newSurvey: Array<orthosensor.domains.Survey>) {
            this._surveys = newSurvey;
        }

        /* @ngInject */
        constructor(PatientListFactory: any, SurveyService: orthosensor.services.SurveyService, $q: ng.IQService) {
            this.PatientService = PatientListFactory;
            this.SurveyService = SurveyService;
            this.hospitals = this.getHospitals();
            this.chartHeight = 280;
            this.$q = $q;
            this.surveys = new Array<orthosensor.domains.Survey>();
            // this.dashboardService = IQ_DashboardRepository;
        }

        public getHospitals(): orthosensor.domains.Hospital[] {
            // console.log(this.hospitals);
            if (this.hospitals !== undefined && this.hospitals.length > 0) {
                return this.hospitals;
            } else {
                return this.PatientService.loadHospitals()
                    .then((data: any) => {
                        this.hospitals = data;
                        // console.log(this.hospitals);
                        return this.hospitals;
                    }, (error: any) => {
                        console.log(error);
                    });
            }
        }

        public loadHospitals(): void {
            this.PatientService.loadHospitals()
                .then((data: any) => {
                    this._hospitals = this.convertSFHospitalToObject(data);
                    console.log(this.hospitals);
                }, (error: any) => {
                    console.log(error);
                });
        }

        public loadPractices(hospitalId: string): void {
            this.PatientService.loadPractices(hospitalId)
                .then((data: any[]) => {
                    console.log(data);
                    this._practices = this.convertSFPracticeToObject(data);
                    console.log(this.practices);
                    // return this.practices;
                }, (error: any) => {
                    console.log(error);
                });
            // return this.practices;
        }

        public loadHospitalPractices(hospitalId: string): void {
            this.PatientService.getHospitalPractices(hospitalId)
                .then((data: any[]) => {
                    console.log(data);
                    this.practices = this.convertSFPracticeToObject(data);
                    console.log(this.practices);
                    // return this.practices;
                }, (error: any) => {
                    console.log(error);
                });
            // return this.practices;
        }

        public getSurgeons(hospitalId: string): orthosensor.domains.Surgeon[] {
            return this.surgeons;
        }

        public setSurgeons(hospitalId: string): void {
            let surgeonData: orthosensor.domains.Surgeon[];
            // console.log(hospitalId);
            if (hospitalId !== null) {
                this.PatientService.loadSurgeons(hospitalId)
                    .then((data: any) => {
                        let surg = data;
                        // console.log(surg);
                        for (let i = 0; i < surg.length; i++) {
                            let surgeonR = surg[i].Surgeon__r;
                            let surgeonD: orthosensor.domains.Surgeon;
                            // surgeonD = surgeonModule.Surgeon;
                            // console.log(surgeonR);
                            // surgeonD.id = surg[i].Surgeon__r.Id;
                            // surgeonD.name = surgeonR.Name;
                            // surgeonData.push(surgeonD);
                        }
                        this.surgeons = surgeonData;
                    }, (error: any) => {
                        console.log(error);
                    });
                // return surgeonData;
            }
        }

        public loadAllSurgeons(): void {
            this.PatientService.loadAllSurgeons()
                .then((data: any) => {
                    // console.log(data);
                    this.surgeons = this.convertSFSurgeonToObject(data);
                }, (error: any) => {
                    console.log(error);
                });
            // return surgeonData;
        }

        public loadHospitalPracticeSurgeons(hospitalPracticeId: string): void {
            // let surgeonData: orthosensor.domains.Surgeon[];
            // console.log(hospitalPracticeId);
            if (hospitalPracticeId !== null) {
                this.PatientService.loadHospitalPracticeSurgeons(hospitalPracticeId)
                    .then((data: any) => {
                        this.surgeons = this.convertSFSurgeonToObject(data);
                        console.log(this.surgeons);
                    }, (error: any) => {
                        console.log(error);
                    });
                // return surgeonData;
            }
        };

        public loadHospitalByPractice(hospitalPracticeId: string): void {
            // let surgeonData: orthosensor.domains.Surgeon[];
            // console.log(hospitalPracticeId);
            if (hospitalPracticeId !== null) {
                this.PatientService.getHospitalByPractice(hospitalPracticeId)
                    .then((data: any) => {
                        // console.log(data);
                        this.hospital = {
                            id: data.Id,
                            name: data.Name,
                            recordTypeId: 'HospitalAccount'
                        };
                        // console.log(this.hospital);
                    }, (error: any) => {
                        console.log(error);
                    });
                // return surgeonData;
            }
        }

        public getCaseActivityByPractice(practiceId: string, startDate: string, endDate: string): any[] {
            this.PatientService.getCaseActivityByPractice(practiceId, startDate, endDate)
                .then((data: any) => {
                    let results = data;
                    // console.log(results);
                    return results;
                    // this.surgeons = surgeonData;
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return [];
        }

        public setSurveys(): void {
            this.surveys = [];
            this.SurveyService.loadSurveys()
                .then((data: any) => {
                    for (let i = 0; i <= data.length; i++) {
                        if (data[i] !== undefined) {
                            let survey: orthosensor.domains.Survey = new orthosensor.domains.Survey();
                            survey.id = data[i].Id;
                            survey.name = data[i].Name__c;
                            // console.log(survey);
                            this.surveys.push(survey);
                        }    
                    }
                }, (error: any) => {
                    console.log(error);
                    // return error;
                });
            
        }

        public convertSFHospitalToObject(data: any[]): orthosensor.domains.Hospital[] {
            let hospitals: orthosensor.domains.Hospital[] = [];
            // console.log(data.length);
            for (let i = 0; i < data.length; i++) {
                let hospital: orthosensor.domains.Hospital = {
                    id: data[i].Id,
                    name: data[i].Name,
                };

                // console.log(practice);
                hospitals.push(hospital);
            }
            // console.log(hospitals);
            return hospitals;
        }

        public convertSFPracticeToObject(data: any[]): orthosensor.domains.Practice[] {
            let practices: orthosensor.domains.Practice[];
            practices = [];
            // console.log(data.length);
            for (let i = 0; i < data.length; i++) {
                let practice: orthosensor.domains.Practice = {
                    id: data[i].Id,
                    name: data[i].Name,
                    parentId: data[i].ParentId,
                    parentName: '' //data[i].Parent.Name
                };
                // console.log(practice);
                practices.push(practice);
            }
            // console.log(practices);
            return practices;
        }

        public convertSFSurgeonToObject(data: any[]): orthosensor.domains.Surgeon[] {
            let surgeons: orthosensor.domains.Surgeon[];
            surgeons = [];
            for (let i = 0; i < data.length; i++) {
                let surgeon: orthosensor.domains.Surgeon = {
                    id: data[i].Surgeon__r.AccountId,
                    name: data[i].Surgeon__r.Name,
                    parentId: data[i].Hospital__c,
                    parentName: '' //data[i].Parent.Name //do we need this?
                };

                surgeons.push(surgeon);
            }
            // console.log(surgeons);
            return surgeons;
        }


    }

    DashboardService.$inject = ['PatientListFactory', 'SurveyService', '$q'];

    angular
        .module('orthosensor.dashboard')
        .service('DashboardService', DashboardService);
}
