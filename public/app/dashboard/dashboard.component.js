var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var DashboardController = (function () {
            function DashboardController(DashboardService, DashboardDataService, PatientsUnderBundleService, PatientService, UserService, $state) {
                this.DashboardService = DashboardService;
                this.UserService = UserService;
                this.DashboardDataService = DashboardDataService;
                this.PatientService = PatientService;
                this.PatientsUnderBundleService = PatientsUnderBundleService;
                this.$state = $state;
                this.patientsUnderBundleTitle = 'Patients Under Bundle';
                // this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
            }
            DashboardController.prototype.$onInit = function () {
                var _this = this;
                var practiceId = '0017A00000M803ZQAR';
                this.isAdmin = this.UserService.isAdmin;
                console.log(this.isAdmin);
                this.standardComponentSize = 'col-sm-4 col-xs-12';
                if (!this.isAdmin) {
                    practiceId = this.UserService.user.accountId;
                    //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
                    this.DashboardDataService.getScheduledPatients(practiceId, 7)
                        .then(function (data) {
                        if (data !== null) {
                            _this.patientsScheduled = data.length;
                        }
                        else {
                            _this.patientsScheduled = 0;
                        }
                        console.log(_this.patients);
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                    this.DashboardDataService.getPatientsUnderBundle(practiceId)
                        .then(function (data) {
                        if (data !== null) {
                            _this.patientsUnderBundleCount = data.length;
                        }
                        else {
                            _this.patientsUnderBundleCount = 0;
                        }
                        console.log(_this.patientsUnderBundleCount);
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
            };
            return DashboardController;
        }());
        DashboardController.$inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state'];
        var Dashboard = (function () {
            function Dashboard() {
                this.bindings = {
                    practiceId: '@',
                };
                this.controller = DashboardController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/dashboard/dashboard.component.html';
            }
            return Dashboard;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osDashboard', new Dashboard());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboard.component.js.map