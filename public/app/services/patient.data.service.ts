module orthosensor.services {

    export class PatientDataService {
        static inject: Array<string> = ['$rootScope', '$q'];
        public patient: orthosensor.domains.Patient;

        constructor(public $rootScope: ng.IRootScopeService, public $q: ng.IQService) {}
       
        checkDuplicate(patient: orthosensor.domains.Patient): ng.IPromise<{}> {
            let deferred = this.$q.defer();
            let accountId = null;
            if (patient.anonymous) {
                if (patient.hospitalId !== null) {
                    accountId = patient.hospitalId;
                }
                else if (patient.practice !== undefined && patient.practice.length > 0) {
                    accountId = patient.practice;
                }
                else if (patient.hospital.length > 0) {
                    accountId = patient.hospital;
                }
                IQ_PatientActions.CheckDuplicateAnonymous(accountId, patient.label, patient.birthYear,  (result, event) => {
                    //console.log(result);

                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });

            }
            else {
                var dateofBirthString = moment.utc(patient.dateofBirthString).format('MM/DD/YYYY');
                if (patient.hospitalId !== null) {
                    accountId = patient.hospitalId;
                }
                else if (patient.practice !== undefined && patient.practice.length > 0) {
                    accountId = patient.practice;
                }
                else if (patient.hospital.length > 0) {
                    accountId = patient.hospital;
                }

                /// TO DO: add email to Apex and call
                IQ_PatientActions.CheckDuplicate(accountId, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateofBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, function (result, event) {

                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });

            }
            return deferred.promise;
        }

        createPatient(patient: orthosensor.domains.Patient) {
            var deferred = this.$q.defer();
            var dateofBirthString = moment.utc(patient.dateofBirthString).format('MM/DD/YYYY');
            var accountId = null;
            if (patient.hospitalId != null) {
                accountId = patient.hospitalId;
            }
            else if (patient.practice !== undefined && patient.practice.length > 0) {
                accountId = patient.practice;
            }
            else if (patient.hospital.length > 0) {
                accountId = patient.hospital;
            }
            if (patient.anonymous) {
                // TO DO: add email 
                IQ_PatientActions.CreateNewAnonymousPatient(patient.hospitalId, patient.label, patient.medicalRecordNumber, patient.birthYear, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, patient.email,
                     (result, event) => {
                    //console.log(result);

                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });

            }
            else {
                // console.log(patient.hospitalId+  ', ' + patient.lastName+  ', ' + patient.firstName+  ', ' + patient.medicalRecordNumber+  ', ' + dateofBirthString+  ', ' + patient.gender+  ', ' + patient.socialSecurityNumber+  ', ' + patient.patientNumber+  ', ' + patient.accountNumber+  ', ' + patient.race+  ', ' + patient.language+  ', ' + patient.email);
                IQ_PatientActions.CreateNewPatient(patient.hospitalId, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateofBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, patient.email,  (result, event) => {
                    //console.log(result);

                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });

            }
            return deferred.promise;
        }
        updatePatient(patient) {
            let deferred = this.$q.defer();
            let dateofBirthString = moment.utc(patient.dateofBirthString).format('MM/DD/YYYY');
            if (patient.anonymous) {
                IQ_PatientActions.UpdateAnonymousPatient(patient.Id, patient.Label, patient.BirthYear, patient.MedicalRecordNumber, patient.Gender, patient.SocialSecurityNumber, patient.PatientNumber, patient.AccountNumber, patient.Race, patient.Language, function (result, event) {
                    //console.log(result);
                    $rootScope.$apply(() => {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            $rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
            }
            else {
                // var dateofBirthString = moment.utc(patient.DateofBirthString).format('MM/DD/YYYY');
                // console.log(patient.id + ', ' + patient.lastName + ',' + patient.firstName + ', ' + patient.medicalRecordNumber + ', ' + dateofBirthString + ', ' + patient.gender + ', ' + patient.socialSecurityNumber + ', ' + patient.patientNumber + ', ' + patient.accountNumber + ', ' + patient.race + ', ' + patient.language + ', ' + patient.email);
                IQ_PatientActions.UpdatePatient(patient.id, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateofBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, patient.email,  (result, event) => {
                    //console.log(result);
                    // $rootScope.$apply(() => {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            $rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                // }, { buffer: false, escape: true, timeout: 120000 });
            }
            return deferred.promise;
        }
        getPatient(patientId: string) {
            let deferred = this.$q.defer();
            // console.log(patientId);
            IQ_PatientActions.GetPatient(patientId, function (result, event) {
                //console.log(result);

                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });

            return deferred.promise;
        }

        checkAnonymous(account: any) {
            var deferred = this.$q.defer();
            IQ_PatientActions.checkAnonymous(account, function (result, event) {

                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });

            return deferred.promise;
        }
        getLanguages(): string[] {
            let languages: string[];
            languages = [
                'English', 'Spanish', 'French', 'Chinese', 'Creole', 'Unknown', 'Russian', 'Portuguese'];
            return languages;
        }
        getRaces(): string[] {
            let races: string[];
            races = [
                'White',
                'Black or African American',
                'American Indian',
                'Asian',
                'Native Hawaiian',
                'Hispanic',
                'Other'
            ];
            return races;
        }
    }

        angular
        .module('orthosensor.services')
        .service('PatientDataService', PatientDataService);

    }
