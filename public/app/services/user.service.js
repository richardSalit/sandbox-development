var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var UserService = (function () {
            function UserService() {
            }
            Object.defineProperty(UserService.prototype, "hasPractices", {
                get: function () {
                    return this._hasPractices;
                },
                set: function (hasPractices) {
                    this._hasPractices = hasPractices;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(UserService.prototype, "isAdmin", {
                get: function () {
                    return this._isAdmin;
                },
                set: function (isAdmin) {
                    this._isAdmin = isAdmin;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(UserService.prototype, "user", {
                get: function () {
                    return this._user;
                },
                // get current user from Force.com. this is done in the run.js file.
                // Change if we authenticate differently
                set: function (_user) {
                    // this.sfUser = _user;
                    // this.convertSFToObject(_user);
                    this._user = _user;
                    console.log(this._user.accountName);
                    console.log(this._user.accountId);
                },
                enumerable: true,
                configurable: true
            });
            UserService.prototype.isSurgeon = function () {
                // console.log(this.user.Contact.Account.RecordType.Name);
                // return this.user.Contact.Account.RecordType.Name === 'Surgeon';
                return true;
            };
            UserService.prototype.convertSFToObject = function (data) {
                console.log(data);
                var user = new orthosensor.domains.User();
                user.id = data.Id;
                user.name = data.Name;
                user.userType = data.Profile.Name;
                if (user.userType !== 'System Administrator') {
                    user.contactId = data.ContactId;
                    user.accountName = data.Contact.Account.Name;
                    user.accountId = data.Contact.Account.Id;
                    user.recordType = data.Contact.Account.RecordType.Name;
                    user.anonymousPatients = data.Contact.Account.Anonymous_Patients__c;
                    if (data.Contact.Account.Parent) {
                        user.hospitalId = data.Contact.Account.ParentId;
                        user.hospitalName = data.Contact.Account.Parent.Name;
                    }
                }
                return user;
            };
            return UserService;
        }());
        services.UserService = UserService;
        UserService.$inject = [];
        angular
            .module('orthosensor.services')
            .service('UserService', UserService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=user.service.js.map