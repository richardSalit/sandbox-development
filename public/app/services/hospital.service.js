var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var HospitalService = (function () {
            function HospitalService($q, $rootScope, $http) {
                this.$q = $q;
                this.$rootScope = $rootScope;
            }
            Object.defineProperty(HospitalService.prototype, "hospitals", {
                get: function () {
                    return this._hospitals;
                },
                set: function (hospitals) {
                    this._hospitals = hospitals;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "practices", {
                get: function () {
                    return this._practices;
                },
                set: function (practices) {
                    this._practices = practices;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterHospitalId", {
                get: function () {
                    return this._filterHospitalId;
                },
                set: function (filterHospitalId) {
                    this._filterHospitalId = filterHospitalId;
                    this._filterHospitalName = this.getHospitalName(filterHospitalId);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterPracticeId", {
                get: function () {
                    return this._filterPracticeId;
                },
                set: function (filterPracticeId) {
                    this._filterPracticeId = filterPracticeId;
                    this._filterPracticeName = this.getPracticeName(filterPracticeId);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "currentFilterId", {
                get: function () {
                    return this._currentFilterId;
                },
                set: function (currentFilterId) {
                    this._currentFilterId = currentFilterId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterHospitalName", {
                get: function () {
                    return this._filterHospitalName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterPracticeName", {
                get: function () {
                    return this._filterPracticeName;
                },
                enumerable: true,
                configurable: true
            });
            HospitalService.prototype.getHospitals = function () {
                if (this._hospitals.length > 0) {
                    return _hospitals;
                }
                else {
                    return this.retrieveHospitals()
                        .then(function (data) {
                        return this._hospitals = data;
                    }, function (error) {
                    });
                }
            };
            HospitalService.prototype.retrieveHospitals = function () {
                var deferred = $q.defer();
                IQ_PatientActions.LoadHospitals(function (result, event) {
                    //console.log(result);
                    $rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            $rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            HospitalService.prototype.getPracticeName = function (id) {
                console.log('Searching for practice: ' + id);
                for (var i = 0; i < this._practices.length; i++) {
                    // console.log(this._practices[i].Id);
                    if (this._practices[i].Id === id) {
                        console.log(this._practices[i].Id + ', ' + id + ' found!');
                        return this._practices[i].Name;
                    }
                }
                return '';
            };
            HospitalService.prototype.getHospitalName = function (id) {
                console.log('Searching for hospital: ' + id);
                for (var i = 0; i < this._hospitals.length; i++) {
                    if (this._hospitals[i].Id === id) {
                        console.log(this._hospitals[i].Id + ', ' + id + ' found!');
                        return this._hospitals[i].Name;
                    }
                }
                return '';
            };
            return HospitalService;
        }());
        HospitalService.inject = ['$q', '$rootScope', '$http'];
        services.HospitalService = HospitalService;
        angular
            .module('orthosensor.services')
            .service('HospitalService', HospitalService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=hospital.service.js.map