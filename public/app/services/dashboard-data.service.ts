namespace orthosensor.services {
    'use strict';

    export interface IDashboardDataService {
        getPromDeltas: any;
    }

    export class DashboardDataService {
        static inject: Array<string> = ['$q', '$rootScope', '$http'];
        constructor(private $q: any, private $rootScope: any, private $http: any) { }

        public getAggregatePromDeltasBySurgeon(surgeonId: string, startMonth: number, startYear: number, endMonth: number, endYear: number): any {
            console.log(surgeonId, startMonth, startYear, endMonth, endYear);
            let deferred = this.$q.defer();
            IQ_DashboardRepository.GetAggregatePromDeltasBySurgeon(surgeonId, 1, 2016, 12, 2017,
                function (result: any, event: any) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }

                }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        public getAggregatePromDeltasByPractice(practiceId: string, startMonth: number, startYear: number, endMonth: number, endYear: number): any {
            console.log(practiceId
                , startMonth, startYear, endMonth, endYear);
            let deferred = this.$q.defer();
            IQ_DashboardRepository.GetAggregatePromDeltasByPractice(practiceId, 1, 2016, 12, 2017,
                function (result: any, event: any) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }

                }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        public getPromDeltasBySurgeon(surgeonId: string, startMonth: number, startYear: number, endMonth: number, endYear: number): any {
            console.log(surgeonId, startMonth, startYear, endMonth, endYear);
            let deferred = this.$q.defer();
            IQ_DashboardRepository.GetPromDeltasBySurgeon(surgeonId, 1, 2016, 12, 2017,
                function (result: any, event: any) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }

                }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        public getPromDeltasByPractice(practiceId: string, startMonth: number, startYear: number, endMonth: number, endYear: number): any {
            console.log(practiceId
                , startMonth, startYear, endMonth, endYear);
            let deferred = this.$q.defer();
            IQ_DashboardRepository.GetPromDeltasByPractice(practiceId, 1, 2016, 12, 2017,
                function (result: any, event: any) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }

                }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        public getPatientsUnderBundle(practiceId: string): any {
            console.log(practiceId);
            let deferred = this.$q.defer();
            if (practiceId !== null) {
                IQ_DashboardRepository.GetPatientsUnderBundle(practiceId,
                    (result: any, event: any) => {
                        console.log(result);
                        // this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }

                    }, { buffer: false, escape: true, timeout: 120000 });
            }
            return deferred.promise;
        }
        public getScheduledPatients(practiceId: string, daysInFuture: number): any {
            console.log(practiceId);
            let deferred = this.$q.defer();
            if (practiceId !== null) {
                IQ_DashboardRepository.GetScheduledPatients(practiceId, daysInFuture,
                    (result: any, event: any) => {
                        console.log(result);
                        // this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }

                    }, { buffer: false, escape: true, timeout: 120000 });
            }
            return deferred.promise;
        }
    }

    angular
        .module('orthosensor.services')
        .service('DashboardDataService', DashboardDataService);
}