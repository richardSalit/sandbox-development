module orthosensor.services {
    export class UserService {
        public sfUser: {};
        private _user: orthosensor.domains.User;
        public accountName: string;
        public accountId: string;
        public recordType: string;
        public userType: string;
        // public hasPractices: boolean;
        private _hasPractices: boolean;
        private _isAdmin: boolean;

        get hasPractices(): boolean {
            return this._hasPractices;
        }
        set hasPractices(hasPractices: boolean) {
            this._hasPractices = hasPractices;
        }

        get isAdmin(): boolean {
            return this._isAdmin;
        }

        set isAdmin(isAdmin: boolean) {
            this._isAdmin = isAdmin;
        }

        constructor() { }

        public get user(): orthosensor.domains.User {
            return this._user;
        }
        // get current user from Force.com. this is done in the run.js file.
        // Change if we authenticate differently
        public set user(_user: orthosensor.domains.User) {
            // this.sfUser = _user;
            // this.convertSFToObject(_user);
            this._user = _user;
            console.log(this._user.accountName);
            console.log(this._user.accountId);
        }

        public isSurgeon() {
            // console.log(this.user.Contact.Account.RecordType.Name);
            // return this.user.Contact.Account.RecordType.Name === 'Surgeon';
            return true;
        }
        public convertSFToObject(data: any): orthosensor.domains.User {
            console.log(data);
            let user = new orthosensor.domains.User();
            user.id = data.Id;
            user.name = data.Name;
            user.userType = data.Profile.Name;
            if (user.userType !== 'System Administrator') {
                user.contactId = data.ContactId;
                user.accountName = data.Contact.Account.Name;
                user.accountId = data.Contact.Account.Id;
                user.recordType = data.Contact.Account.RecordType.Name;
                user.anonymousPatients = data.Contact.Account.Anonymous_Patients__c;
                if (data.Contact.Account.Parent) {
                    user.hospitalId = data.Contact.Account.ParentId;
                    user.hospitalName = data.Contact.Account.Parent.Name;
                }    
            }
            return user;
        }
    }

    UserService.$inject = [];
    angular
        .module('orthosensor.services')
        .service('UserService', UserService);
}
