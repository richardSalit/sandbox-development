var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var SurveyService = (function () {
            // public $q: ng.IQService;
            // public $rootScope: ng.IRootScopeService;
            function SurveyService($q, $rootScope, $http) {
                this.$q = $q;
                this.$rootScope = $rootScope;
                this.eventTypes = [];
                this.$q = $q;
                this.$rootScope = $rootScope;
            }
            // return service;
            ////////////////
            SurveyService.prototype.loadSurveysFromCaseType = function (caseId) {
                var deferred = this.$q.defer();
                IQ_SurveyRepository.LoadSurveysFromCaseType(caseId, function (result, event) {
                    // console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.loadSurveysFromEvent = function (eventId) {
                this.retrieveSurveysByEventId(eventId)
                    .then(function (data) {
                    var surveys = data;
                    //console.log(eventTypeId);
                    // if (eventTypeId !== '') {
                    //     retrieveSurveysFromEventType(eventTypeId)
                    //         .then(function (data) {
                    //             survey = data;
                    //             console.log('Retrieving data');
                    //             console.log(data);
                    //             //return surveys;
                    //         });
                    // }
                });
                ///return survey;
            };
            SurveyService.prototype.retrieveSurveysFromEventType = function (eventTypeId) {
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveysByEventType(eventTypeId, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.retrieveSurveysByEventId = function (eventId) {
                //console.log(eventId);
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveysByEvent(eventId, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.getEventTypeIdByEventId = function (eventId) {
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetEventTypeIdByEventId(eventId, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.getSurveyByName = function (surveyName) {
                //console.log("getSurveyByName:" + surveyName)
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveyByName(surveyName, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.getSurveyById = function (id) {
                //console.log("getSurveyByName:" + surveyName)
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveyById(id, function (result, event) {
                    console.log(result);
                    //this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.loadSurveys = function () {
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveys(function (result, event) {
                    console.log(result);
                    // $evalAsync(function () {
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.getSurveys = function () {
                return this.surveys;
            };
            SurveyService.prototype.setSurveys = function (_surveys) {
                this.surveys = _surveys;
            };
            SurveyService.prototype.getSurvey = function () {
                return this.survey;
            };
            // sets up survey parameters based on survey
            SurveyService.prototype.setSurvey = function (_survey) {
                if (this.survey !== _survey) {
                    this.survey = _survey;
                    console.log(this.survey);
                    this.getEventTypes(this.survey.Id);
                    if ((this.survey.Name__c !== null) || (this.survey.Name__c !== '')) {
                        this.setSurveyScoreNames(this.survey.Name__c);
                    }
                }
            };
            SurveyService.prototype.setSurveyScoreNames = function (surveyName) {
                var _this = this;
                return this.retrieveSurveyScoreNames(surveyName)
                    .then(function (data) {
                    _this.surveyScoreNames = data;
                    // console.log(this.surveyScoreNames);
                });
            };
            //retrieve from db
            SurveyService.prototype.retrieveSurveyScoreNames = function (surveyName) {
                var deferred = this.$q.defer();
                //console.log('Survey Name: ' + surveyName)
                IQ_SurveyRepository.GetScoreNames(surveyName, function (result, event) {
                    // console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, {
                    buffer: false, escape: true, timeout: 120000
                });
                return deferred.promise;
            };
            SurveyService.prototype.setCaseId = function (id) {
                this.caseId = id;
            };
            SurveyService.prototype.getCaseId = function () {
                return this.caseId;
            };
            SurveyService.prototype.retrieveCaseTypeId = function () {
                var deferred = this.$q.defer();
                var caseId = this.getCaseId();
                var caseTypeId = IQ_PatientActions.GetCaseTypeIdByCaseId(caseId, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.retrieveSurveyEventTypes = function (surveyId) {
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveyEventTypes(surveyId, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.setEventTypes = function (events) {
                var j = 0;
                for (var i = 0; i < events.length; i++) {
                    //if (events[i].Event_Type_Name__c != 'Procedure') {
                    this.eventTypes[j] = events[i].Event_Type_Name__c;
                    j++;
                }
                //console.log(eventTypes);
                //["Pre-Op", "4-8 Week Follow-up", "6 Month Follow-up", "12 Month Follow-up", "2 Year Follow-up", "4 Year Follow-up"];
            };
            SurveyService.prototype.getEventTypes = function (surveyId) {
                var _this = this;
                console.log(surveyId);
                return this.retrieveSurveyEventTypes(surveyId)
                    .then(function (data) {
                    //just get array of periods
                    //var periods = data;
                    var categories = data;
                    // console.log(categories);
                    _this.chartCategories = []; //clear array
                    for (var i = 0; i < categories.length; i++) {
                        _this.chartCategories[i] = categories[i].Event_Type__r.Name;
                    }
                });
            };
            SurveyService.prototype.getChartCategories = function () {
                return this.chartCategories;
            };
            // to change how survey score names are set, get them in this method
            // as the data is received and then iterate to get the list of score names        
            SurveyService.prototype.getChartData = function (surveyScores) {
                console.log(surveyScores);
                var chartData = this.setChartQuestions();
                console.log(chartData);
                if (this.surveyScoreNames) {
                    chartData = this.clearChart(chartData);
                    //cycle though questions
                    for (var q = 0; q < this.surveyScoreNames.length; q++) {
                        //cycle through chartCategories (Time periods)
                        // console.log(this.chartCategories);
                        for (var i = 0; i < this.chartCategories.length; i++) {
                            //cycle through the scores
                            for (var j = 0; j < surveyScores.length; j++) {
                                if ((surveyScores[j].Event_Form__r.Event__r.Event_Type_Name__c === this.chartCategories[i]) && (surveyScores[j].Survey_Question__c === this.surveyScoreNames[q])) {
                                    if (isNaN(surveyScores[j].Survey_Response__c)) {
                                        chartData[q][i + 1] = surveyScores[j].Survey_Response__c;
                                    }
                                    else {
                                        chartData[q][i + 1] = parseFloat(surveyScores[j].Survey_Response__c).toFixed(2);
                                    }
                                }
                            }
                        }
                    }
                }
                return chartData;
            };
            SurveyService.prototype.clearChart = function (chartData) {
                //console.log(survey.Name__c); //where are we when it fails!!!
                //console.log(surveyScoreNames);
                //console.log(chartData);
                if (this.surveyScoreNames) {
                    //Initialize chartData with Null values - required to draw the chart if there is missing scores.
                    for (var k = 0; k < this.surveyScoreNames.length; k++) {
                        for (var i = 0; i < this.chartCategories.length; i++) {
                            chartData[k][i + 1] = null;
                        }
                    }
                }
                return chartData;
            };
            //sync chart questions with data
            SurveyService.prototype.setChartQuestions = function () {
                console.log(this.surveyScoreNames);
                if (this.surveyScoreNames) {
                    var chartData = new Array(this.surveyScoreNames.length);
                    if (this.chartCategories.length !== undefined) {
                        for (var i = 0; i < this.surveyScoreNames.length; i++) {
                            chartData[i] = new Array(this.chartCategories.length);
                            chartData[i][0] = this.surveyScoreNames[i];
                        }
                    }
                    return chartData;
                }
                else {
                    return [];
                }
            };
            SurveyService.prototype.setGraphParameters = function (anchorTag, chartData) {
                return {
                    bindto: anchorTag,
                    data: {
                        columns: chartData,
                        selection: {
                            enabled: true
                        }
                    },
                    axis: {
                        x: {
                            type: 'category',
                            categories: this.chartCategories,
                            tick: {
                                centered: true
                            }
                        },
                        y: {
                            count: 5
                        },
                        y2: {
                            show: true
                        }
                    },
                    grid: {
                        y: {
                            show: true
                        }
                    },
                    legend: {
                        position: 'left'
                    },
                    tooltip: {
                        show: true
                    }
                };
            };
            return SurveyService;
        }());
        SurveyService.$inject = ['$q', '$rootScope', '$http'];
        services.SurveyService = SurveyService;
        angular
            .module('orthosensor.services')
            .service('SurveyService', SurveyService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=survey.service.js.map