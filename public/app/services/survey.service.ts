module orthosensor.services {
    
    export class SurveyService {
        public static $inject = ['$q', '$rootScope', '$http'];
        public caseId: string;
        public eventTypes: any[];
        public surveys: any[];
        public survey: {};
        public chartCategories: any[];
        public surveyScoreNames: any[];
        // public $q: ng.IQService;
        // public $rootScope: ng.IRootScopeService;
    
    constructor (public $q: ng.IQService, public $rootScope: ng.IRootScopeService, $http: ng.IHttpService) {
        this.eventTypes = [];
        this.$q = $q;
        this.$rootScope = $rootScope;
       
        }
        // return service;
        ////////////////
        loadSurveysFromCaseType(caseId: string ) {
            let deferred = this.$q.defer();
            IQ_SurveyRepository.LoadSurveysFromCaseType(caseId, function (result, event) {
                // console.log(result);
                // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                // });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        
        loadSurveysFromEvent(eventId) {
            this.retrieveSurveysByEventId(eventId)
                .then(function (data) {
                    var surveys = data;
                    //console.log(eventTypeId);
                    // if (eventTypeId !== '') {
                    //     retrieveSurveysFromEventType(eventTypeId)
                    //         .then(function (data) {
                    //             survey = data;
                    //             console.log('Retrieving data');
                    //             console.log(data);
                    //             //return surveys;
                    //         });
                    // }
                });
            ///return survey;
        }
        
        retrieveSurveysFromEventType(eventTypeId) {
            var deferred = this.$q.defer();
            IQ_SurveyRepository.GetSurveysByEventType(eventTypeId, function (result, event) {
                //console.log(result);
                // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
               // });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        
        retrieveSurveysByEventId(eventId) {
            //console.log(eventId);
            var deferred = this.$q.defer();
            IQ_SurveyRepository.GetSurveysByEvent(eventId, function (result, event) {
                //console.log(result);
                // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                // });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        getEventTypeIdByEventId(eventId) {
            var deferred = this.$q.defer();
            IQ_SurveyRepository.GetEventTypeIdByEventId(eventId, function (result, event) {
                //console.log(result);
                // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                // });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getSurveyByName(surveyName) {
            //console.log("getSurveyByName:" + surveyName)
            var deferred = this.$q.defer();
            IQ_SurveyRepository.GetSurveyByName(surveyName, function (result, event) {
                //console.log(result);
                // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                // });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getSurveyById(id: string) {
            //console.log("getSurveyByName:" + surveyName)
            var deferred = this.$q.defer();
            IQ_SurveyRepository.GetSurveyById(id, function (result, event) {
                console.log(result);
                //this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                // });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        loadSurveys() {
            var deferred = this.$q.defer();
            IQ_SurveyRepository.GetSurveys(function (result, event) {
                console.log(result);
                // $evalAsync(function () {
                // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                // });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getSurveys() {
            return this.surveys;
        }

        setSurveys(_surveys) {
            this.surveys = _surveys;
        }

        getSurvey() {
            return this.survey;
        }

        // sets up survey parameters based on survey
        setSurvey(_survey) {
            if (this.survey !== _survey) {
                this.survey = _survey;
                console.log(this.survey);
                this.getEventTypes(this.survey.Id);
                if ((this.survey.Name__c !== null) || (this.survey.Name__c !== '')) {
                    this.setSurveyScoreNames(this.survey.Name__c);
                }
            }
        }

        setSurveyScoreNames(surveyName) {
            return this.retrieveSurveyScoreNames(surveyName)
                .then((data: any[]) => {
                    this.surveyScoreNames = data;
                    // console.log(this.surveyScoreNames);
                });
        }
        //retrieve from db
        retrieveSurveyScoreNames(surveyName) {
            let deferred = this.$q.defer();
            //console.log('Survey Name: ' + surveyName)
            IQ_SurveyRepository.GetScoreNames(surveyName, function (result, event) {
                // console.log(result);
                // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                // });
            }, {
                    buffer: false, escape: true, timeout: 120000
                });
            return deferred.promise;
        }
        setCaseId(id: string) {
            this.caseId = id;
        }
        getCaseId() {
            return this.caseId;
        }
        retrieveCaseTypeId() {
            var deferred = this.$q.defer();
            var caseId = this.getCaseId();
            var caseTypeId = IQ_PatientActions.GetCaseTypeIdByCaseId(caseId, function (result, event) {
                //console.log(result);
                // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                // });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        
        retrieveSurveyEventTypes(surveyId) {
            let deferred = this.$q.defer();
            IQ_SurveyRepository.GetSurveyEventTypes(surveyId, function (result, event) {
                console.log(result);
                // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                // });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        setEventTypes(events) {
            var j = 0;
            for (var i = 0; i < events.length; i++) {
                //if (events[i].Event_Type_Name__c != 'Procedure') {
                this.eventTypes[j] = events[i].Event_Type_Name__c;
                j++;
            }
            //console.log(eventTypes);
            //["Pre-Op", "4-8 Week Follow-up", "6 Month Follow-up", "12 Month Follow-up", "2 Year Follow-up", "4 Year Follow-up"];
        }
        getEventTypes(surveyId) {
            console.log(surveyId);
            return this.retrieveSurveyEventTypes(surveyId)
                .then((data: any[]) => {
                    //just get array of periods
                    //var periods = data;
                    let categories = data;
                    // console.log(categories);
                    this.chartCategories = []; //clear array
                    for (let i = 0; i < categories.length; i++) {
                        this.chartCategories[i] = categories[i].Event_Type__r.Name;
                    }
                });
        }
        
        getChartCategories() {
            return this.chartCategories;
        }
        // to change how survey score names are set, get them in this method
        // as the data is received and then iterate to get the list of score names        
        getChartData(surveyScores) {
            console.log(surveyScores);
            let chartData = this.setChartQuestions();
            console.log(chartData);
            if (this.surveyScoreNames) {
                chartData = this.clearChart(chartData);
                //cycle though questions
                for (var q = 0; q < this.surveyScoreNames.length; q++) {
                    //cycle through chartCategories (Time periods)
                    // console.log(this.chartCategories);
                    for (var i = 0; i < this.chartCategories.length; i++) {
                        //cycle through the scores
                        for (var j = 0; j < surveyScores.length; j++) {
                            if ((surveyScores[j].Event_Form__r.Event__r.Event_Type_Name__c === this.chartCategories[i]) && (surveyScores[j].Survey_Question__c === this.surveyScoreNames[q])) {
                                if (isNaN(surveyScores[j].Survey_Response__c)) {
                                    chartData[q][i + 1] = surveyScores[j].Survey_Response__c;
                                } else {
                                    chartData[q][i + 1] = parseFloat(surveyScores[j].Survey_Response__c).toFixed(2);
                                }
                            }
                        }
                    }
                }
            }
            return chartData;
        }

        clearChart(chartData: any) {
            //console.log(survey.Name__c); //where are we when it fails!!!
            //console.log(surveyScoreNames);
            //console.log(chartData);
            if (this.surveyScoreNames) {
                //Initialize chartData with Null values - required to draw the chart if there is missing scores.
                for (let k = 0; k < this.surveyScoreNames.length; k++) {
                    for (let i = 0; i < this.chartCategories.length; i++) {
                        chartData[k][i + 1] = null;
                    }
                }
            }
            return chartData;
        }
        //sync chart questions with data
        setChartQuestions() {
            console.log(this.surveyScoreNames);
            if (this.surveyScoreNames) {
                let chartData = new Array(this.surveyScoreNames.length);
                if (this.chartCategories.length !== undefined) {
                    for (let i = 0; i < this.surveyScoreNames.length; i++) {
                        chartData[i] = new Array(this.chartCategories.length);
                        chartData[i][0] = this.surveyScoreNames[i];
                    }
                }    
                return chartData;
            } else {
                return [];
            }
        }
        setGraphParameters(anchorTag, chartData) {
            return {
                bindto: anchorTag,
                data: {
                    columns: chartData,
                    selection: {
                        enabled: true
                    }
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: this.chartCategories,
                        tick: {
                            centered: true
                        }
                    },
                    y: {
                        count: 5
                    },
                    y2: {
                        show: true
                    }
                },
                grid: {
                    y: {
                        show: true
                    }
                },
                legend: {
                    position: 'left'
                },
                tooltip: {
                    show: true
                }
            };
        }
    }
        angular
        .module('orthosensor.services')
        .service('SurveyService', SurveyService);

}
