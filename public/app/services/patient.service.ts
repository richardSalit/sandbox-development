module orthosensor.services {

    export class PatientService {
        static inject: Array<string> = ['$rootScope', '$q', 'PatientDataService'];
        private _patient: orthosensor.domains.Patient;
        private _currentFilterId: string;
        get patient(): orthosensor.domains.Patient {
            return this._patient;
        }
        set patient(newPatient: orthosensor.domains.Patient) {
            this._patient = newPatient;
        }
        //public $q: any;

        set currentFilterId(id: string) {
            this._currentFilterId = id;
        }        
        get currentFilterId() {
            return this._currentFilterId;
}        
        constructor(public $rootScope: ng.IRootScopeService, public $q: ng.IQService, public PatientDataService: orthosensor.services.PatientDataService) {
            // this._patient = new orthosensor.domains.Patient();
            this._currentFilterId = '';
        }
        // this should be deprecated and use direct call using object initializer
        getPatient() {
            console.log(this._patient);
            return this._patient;
        }
        public setPatient(_patient: orthosensor.domains.Patient) {
            console.log(_patient);
            this._patient = new orthosensor.domains.Patient();
            this._patient = _patient;
        }

        // this is to handle when no object has been created.
        setPatientId(patientId: string): void {
            // this.patient = new orthosensor.domains.Patient();

            // this.patient.id = patientId;
            this.PatientDataService.getPatient(patientId)
                .then((data: any) => {
                console.log(data);
                let sfPatient = data;
                this.setPatientFromSFObject(sfPatient);
                }, function (error) {
                    console.log(error);
                });    
            }
        isAnonymous() {
            //console.log(patient);
            //console.log(patient.Anonymous__c);
            if (this.patient.anonymous) {
                return this.patient.anonymous;
            } else {
                return false;
            }
        }
        checkDuplicate(patient: orthosensor.domains.Patient): ng.IPromise<{}> {
            let deferred = this.$q.defer();
            let accountId = null;
            if (patient.anonymous) {
                if (patient.hospitalId !== null) {
                    accountId = patient.hospitalId;
                }
                else if (patient.practice !== undefined && patient.practice.length > 0) {
                    accountId = patient.practice;
                }
                else if (patient.hospital.length > 0) {
                    accountId = patient.hospital;
                }
                IQ_PatientActions.CheckDuplicateAnonymous(accountId, patient.label, patient.birthYear, function (result, event) {
                    //console.log(result);

                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });

            }
            else {
                var dateofBirthString = moment.utc(patient.dateofBirthString).format('MM/DD/YYYY');
                if (patient.hospitalId !== null) {
                    accountId = patient.hospitalId;
                }
                else if (patient.practice !== undefined && patient.practice.length > 0) {
                    accountId = patient.practice;
                }
                else if (patient.hospital.length > 0) {
                    accountId = patient.hospital;
                }

                /// TO DO: add email to Apex and call
                IQ_PatientActions.CheckDuplicate(accountId, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateofBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, function (result, event) {

                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });

            }
            return deferred.promise;
        }

        
        checkAnonymous(account: any) {
            var deferred = this.$q.defer();
            IQ_PatientActions.checkAnonymous(account, function (result: any, event: any) {

                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });

            return deferred.promise;
        }
        getSFObject(): orthosensor.domains.sfPatient {
            console.log(this._patient);
            let sfPatient: orthosensor.domains.sfPatient = new orthosensor.domains.sfPatient();
            sfPatient.Id = this._patient.id;
            sfPatient.Anonymous__c = this._patient.anonymous;
            sfPatient.Anonymous_Label__c = this._patient.anonymousLabel;
            sfPatient.Hl7__c = this._patient.hl7;
            sfPatient.HospitalId__c = this._patient.hospitalId;
            sfPatient.hospital = this._patient.hospital;
            sfPatient.Last_Name__c = this._patient.lastName;
            sfPatient.First_Name__c = this._patient.firstName;
            sfPatient.practice = this._patient.practice;
            // sfPatient.Anonymous_Label__c = this._patient.label;
            sfPatient.Medical_Record_Number__c = this.patient.medicalRecordNumber;
            sfPatient.Email__c = this._patient.email;
            sfPatient.Date_Of_Birth__c = this._patient.dateofBirth;
            sfPatient.Anonymous_Year_Of_Birth__c = this._patient.birthYear;
            sfPatient.Gender__c = this._patient.gender;
            sfPatient.Social_Security_Number__c = this._patient.socialSecurityNumber;
            sfPatient.Source_Record_Id__c = this._patient.patientNumber;
            sfPatient.Account_Number__c = this._patient.accountNumber;
            sfPatient.Race__c = this._patient.race;
            sfPatient.Language__c = this._patient.language;

            return sfPatient;
        }
        setPatientFromSFObject(patient: any): void {
            console.log(patient);
            if (!this._patient) {
                this._patient = new orthosensor.domains.Patient();
            }
            this._patient.id = patient.Id ? patient.Id : null;
            this._patient.anonymous = patient.Anonymous__c ? patient.Anonymous__c : null;
            this._patient.hl7 = patient.Hl7__c ? patient.Hl7__c : null;
            this._patient.hospitalId = patient.Hospital__r.ParentId ?  patient.Hospital__r.ParentId : patient.Hospital__c;
            this._patient.hospital = patient.hospital ?  patient.hospital : null;
            this._patient.lastName = patient.Last_Name__c ? patient.Last_Name__c : null;
            this._patient.firstName = patient.First_Name__c ? patient.First_Name__c : null;
            this._patient.practiceId = patient.Hospital__c ? patient.Hospital__c : null;
            this._patient.anonymousLabel = patient.Anonymous_Label__c ? patient.Anonymous_Label__c : null ;
            this._patient.medicalRecordNumber = patient.Medical_Record_Number__c ?patient.Medical_Record_Number__c : null;
            this._patient.email = patient.Email__c ? patient.Email__c : null;
            this._patient.dateofBirth = patient.Date_Of_Birth__c ? patient.Date_Of_Birth__c : null;
            this._patient.birthYear = patient.Anonymous_Year_Of_Birth__c ? patient.Anonymous_Year_Of_Birth__c : null;
            this._patient.gender = patient.Gender__c ? patient.Gender__c : null ;
            this._patient.socialSecurityNumber = patient.Social_Security_Number__c ? patient.Social_Security_Number__c : null;
            this._patient.patientNumber = patient.Source_Record_Id__c ? patient.Source_Record_Id__c : null;
            this._patient.accountNumber = patient.Account_Number__c ? patient.Account_Number__c : null;
            this._patient.race = patient.Race__c ? patient.Race__c : null;
            this._patient.language = patient.Language__c ? patient.Language__c : null;

        }
    }

    angular
        .module('orthosensor.services')
        .service('PatientService', PatientService);

}
