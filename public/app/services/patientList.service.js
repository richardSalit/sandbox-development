(function () {
    'use strict';
    PatientListFactory.$inject = ['$q', '$rootScope', '$http'];
    angular
        .module('orthosensor.services')
        .factory('PatientListFactory', PatientListFactory);
    function PatientListFactory($q, $rootScope, $http) {
        var patient = {};
        // let _filterHospitalId: string;
        // let _filterPracticeId: string;
        var service = {
            getPatients: getPatients,
            // getFilterHospitalId: getFilterHospitalId,
            // setFilterHospitalId: setFilterHospitalId,
            // getFilterPracticeId: getFilterPracticeId,
            // setFilterPracticeId: setFilterPracticeId,
            getHL7Patients: getHL7Patients,
            SearchPatientList: SearchPatientList,
            SearchHL7PatientList: SearchHL7PatientList,
            // checkDuplicate: checkDuplicate,
            // createPatient: createPatient,
            // checkAnonymous: checkAnonymous,
            createCase: createCase,
            loadCaseTypes: loadCaseTypes,
            loadSurgeons: loadSurgeons,
            loadAllSurgeons: loadAllSurgeons,
            loadHospitals: loadHospitals,
            loadPractices: loadPractices,
            getHospitalPractices: getHospitalPractices,
            getCaseActivityByPractice: getCaseActivityByPractice,
            getCaseActivityBySurgeon: getCaseActivityBySurgeon,
            loadHospitalPracticeSurgeons: loadHospitalPracticeSurgeons,
            getHospitalByPractice: getHospitalByPractice,
            getSurgeonPhaseCount: getSurgeonPhaseCount,
            getPracticePhaseCount: getPracticePhaseCount,
            getMonthlyPracticeSensorAverages: getMonthlyPracticeSensorAverages,
            getMonthlySurgeonSensorAverages: getMonthlySurgeonSensorAverages
        };
        return service;
        // hospitalId can be a hospital or practice        
        function getPatients(hospitalId) {
            var deferred = $q.defer();
            console.log(hospitalId);
            if (!hospitalId) {
                console.log('hospitalId is not defined at all!');
                hospitalId = null;
            }
            if (hospitalId === undefined) {
                console.log('hospitalId is not defined!');
                hospitalId = null;
            }
            IQ_PatientActions.LoadPatientList(hospitalId, function (result, event) {
                //console.log(result);
                //$rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getHL7Patients(hospitalId) {
            var deferred = $q.defer();
            IQ_PatientActions.LoadHL7PatientList(hospitalId, function (result, event) {
                console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function SearchPatientList(hospitalId, keyword) {
            var deferred = $q.defer();
            IQ_PatientActions.SearchPatientList(hospitalId, keyword, function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function SearchHL7PatientList(hospitalId, keyword) {
            console.log(hospitalId + ', ' + keyword);
            var deferred = $q.defer();
            IQ_PatientActions.SearchHL7PatientList(hospitalId, keyword, function (result, event) {
                console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        // function getFilterHospitalId() {
        //     return _filterHospitalId;
        // }
        // function setFilterHospitalId(filterHospitalId: string) {
        //     _filterHospitalId = filterHospitalId;
        // }
        // function getFilterPracticeId() {
        //     return _filterPracticeId;
        // }
        // function setFilterPracticeId(filterPracticeId: string) {
        //     _filterPracticeId = filterPracticeId;
        // }
        function getCurrentPatient() {
            return patient;
        }
        function setCurrentPatient(_patient) {
            patient = _patient;
        }
        function createCase(patient, patientCase) {
            console.log(patient);
            console.log(patientCase);
            var deferred = $q.defer();
            var procedureDateString = moment.utc(patientCase.ProcedureDateString).format('MM/DD/YYYY');
            // console.log(procedureDateString);
            IQ_PatientActions.CreateCase(patient.id, patientCase.CaseTypeId, procedureDateString, patientCase.SurgeonID, patientCase.laterality, function (result, event) {
                //console.log(result);
                //$rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadCaseTypes(hospitalId) {
            var deferred = $q.defer();
            console.log(hospitalId);
            IQ_PatientActions.GetCaseTypes(hospitalId, function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadSurgeons(hospitalId) {
            var deferred = $q.defer();
            // console.log(hospitalId);
            // if (hospitalId !== null) {
            IQ_PatientActions.GetSurgeons(hospitalId, function (result, event) {
                // console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadAllSurgeons() {
            var deferred = $q.defer();
            // if (hospitalId !== null) {
            IQ_PatientActions.LoadAllSurgeons(function (result, event) {
                // console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadHospitalPracticeSurgeons(hospitalId) {
            var deferred = $q.defer();
            // console.log(hospitalId);
            // if (hospitalId !== null) {
            IQ_DashboardRepository.LoadHospitalPracticeSurgeons(hospitalId, function (result, event) {
                // console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        //            else {
        //                return null;
        //            }
        //        }
        function loadHospitals() {
            var deferred = $q.defer();
            IQ_PatientActions.LoadHospitals(function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadPractices(hospitalId) {
            var deferred = $q.defer();
            IQ_PatientActions.LoadPractices(function (result, event) {
                console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getHospitalPractices(hospitalId) {
            var deferred = $q.defer();
            // console.log(hospitalId);
            IQ_PatientActions.LoadHospitalPractices(hospitalId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getHospitalByPractice(practiceId) {
            var deferred = $q.defer();
            // console.log(hospitalId);
            IQ_PatientActions.LoadHospitalByPractice(practiceId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getCaseActivityByPractice(practiceId, startDate, endDate) {
            // let IQ_DashboardRepository: any
            var deferred = $q.defer();
            // console.log(startDate);
            // console.log(endDate);
            IQ_DashboardRepository.GetProceduresPerMonthByPractice(practiceId, startDate, endDate, function (result, event) {
                // console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getCaseActivityBySurgeon(surgeonId, startDate, endDate) {
            // let IQ_DashboardRepository: any
            var deferred = $q.defer();
            IQ_DashboardRepository.GetProceduresPerMonthBySurgeon(surgeonId, startDate, endDate, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getSurgeonPhaseCount(id, startDate, endDate) {
            var deferred = $q.defer();
            IQ_DashboardRepository.GetSurgeonPhaseCount(id, startDate, endDate, function (result, event) {
                // console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getPracticePhaseCount(id, startDate, endDate) {
            var deferred = $q.defer();
            IQ_DashboardRepository.GetPracticePhaseCount(id, startDate, endDate, function (result, event) {
                // console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getMonthlyPracticeSensorAverages(id, startMonth, startYear, endMonth, endYear) {
            console.log(id, startMonth, startYear, endMonth, endYear);
            /// change this before deployment - set for test data
            // startMonth = 4;
            // startYear = 2016;
            // endMonth = 8;
            // endYear = 2016;
            var deferred = $q.defer();
            IQ_DashboardRepository.GetPracticeSensorAverages(id, startMonth, startYear, endMonth, endYear, function (result, event) {
                console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getMonthlySurgeonSensorAverages(id, startMonth, startYear, endMonth, endYear) {
            console.log(startMonth, startYear, endMonth, endYear);
            /// change this before deployment - set for test data
            // startMonth = 4;
            // startYear = 2016;
            // endMonth = 8;
            // endYear = 2016;
            var deferred = $q.defer();
            IQ_DashboardRepository.GetSurgeonSensorAverages(id, startMonth, startYear, endMonth, endYear, function (result, event) {
                console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }
})();
//# sourceMappingURL=patientList.service.js.map