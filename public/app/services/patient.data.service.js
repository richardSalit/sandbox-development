var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var PatientDataService = (function () {
            function PatientDataService($rootScope, $q) {
                this.$rootScope = $rootScope;
                this.$q = $q;
            }
            PatientDataService.prototype.checkDuplicate = function (patient) {
                var _this = this;
                var deferred = this.$q.defer();
                var accountId = null;
                if (patient.anonymous) {
                    if (patient.hospitalId !== null) {
                        accountId = patient.hospitalId;
                    }
                    else if (patient.practice !== undefined && patient.practice.length > 0) {
                        accountId = patient.practice;
                    }
                    else if (patient.hospital.length > 0) {
                        accountId = patient.hospital;
                    }
                    IQ_PatientActions.CheckDuplicateAnonymous(accountId, patient.label, patient.birthYear, function (result, event) {
                        //console.log(result);
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                else {
                    var dateofBirthString = moment.utc(patient.dateofBirthString).format('MM/DD/YYYY');
                    if (patient.hospitalId !== null) {
                        accountId = patient.hospitalId;
                    }
                    else if (patient.practice !== undefined && patient.practice.length > 0) {
                        accountId = patient.practice;
                    }
                    else if (patient.hospital.length > 0) {
                        accountId = patient.hospital;
                    }
                    /// TO DO: add email to Apex and call
                    IQ_PatientActions.CheckDuplicate(accountId, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateofBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, function (result, event) {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                return deferred.promise;
            };
            PatientDataService.prototype.createPatient = function (patient) {
                var _this = this;
                var deferred = this.$q.defer();
                var dateofBirthString = moment.utc(patient.dateofBirthString).format('MM/DD/YYYY');
                var accountId = null;
                if (patient.hospitalId != null) {
                    accountId = patient.hospitalId;
                }
                else if (patient.practice !== undefined && patient.practice.length > 0) {
                    accountId = patient.practice;
                }
                else if (patient.hospital.length > 0) {
                    accountId = patient.hospital;
                }
                if (patient.anonymous) {
                    // TO DO: add email 
                    IQ_PatientActions.CreateNewAnonymousPatient(patient.hospitalId, patient.label, patient.medicalRecordNumber, patient.birthYear, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, patient.email, function (result, event) {
                        //console.log(result);
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                else {
                    // console.log(patient.hospitalId+  ', ' + patient.lastName+  ', ' + patient.firstName+  ', ' + patient.medicalRecordNumber+  ', ' + dateofBirthString+  ', ' + patient.gender+  ', ' + patient.socialSecurityNumber+  ', ' + patient.patientNumber+  ', ' + patient.accountNumber+  ', ' + patient.race+  ', ' + patient.language+  ', ' + patient.email);
                    IQ_PatientActions.CreateNewPatient(patient.hospitalId, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateofBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, patient.email, function (result, event) {
                        //console.log(result);
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                return deferred.promise;
            };
            PatientDataService.prototype.updatePatient = function (patient) {
                var deferred = this.$q.defer();
                var dateofBirthString = moment.utc(patient.dateofBirthString).format('MM/DD/YYYY');
                if (patient.anonymous) {
                    IQ_PatientActions.UpdateAnonymousPatient(patient.Id, patient.Label, patient.BirthYear, patient.MedicalRecordNumber, patient.Gender, patient.SocialSecurityNumber, patient.PatientNumber, patient.AccountNumber, patient.Race, patient.Language, function (result, event) {
                        //console.log(result);
                        $rootScope.$apply(function () {
                            if (event.status) {
                                deferred.resolve(result);
                            }
                            else {
                                $rootScope.handleSessionTimeout(event);
                                deferred.reject(event);
                            }
                        });
                    }, { buffer: false, escape: true, timeout: 120000 });
                }
                else {
                    // var dateofBirthString = moment.utc(patient.DateofBirthString).format('MM/DD/YYYY');
                    // console.log(patient.id + ', ' + patient.lastName + ',' + patient.firstName + ', ' + patient.medicalRecordNumber + ', ' + dateofBirthString + ', ' + patient.gender + ', ' + patient.socialSecurityNumber + ', ' + patient.patientNumber + ', ' + patient.accountNumber + ', ' + patient.race + ', ' + patient.language + ', ' + patient.email);
                    IQ_PatientActions.UpdatePatient(patient.id, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateofBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, patient.email, function (result, event) {
                        //console.log(result);
                        // $rootScope.$apply(() => {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            $rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                    // }, { buffer: false, escape: true, timeout: 120000 });
                }
                return deferred.promise;
            };
            PatientDataService.prototype.getPatient = function (patientId) {
                var deferred = this.$q.defer();
                // console.log(patientId);
                IQ_PatientActions.GetPatient(patientId, function (result, event) {
                    //console.log(result);
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                return deferred.promise;
            };
            PatientDataService.prototype.checkAnonymous = function (account) {
                var deferred = this.$q.defer();
                IQ_PatientActions.checkAnonymous(account, function (result, event) {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                return deferred.promise;
            };
            PatientDataService.prototype.getLanguages = function () {
                var languages;
                languages = [
                    'English', 'Spanish', 'French', 'Chinese', 'Creole', 'Unknown', 'Russian', 'Portuguese'
                ];
                return languages;
            };
            PatientDataService.prototype.getRaces = function () {
                var races;
                races = [
                    'White',
                    'Black or African American',
                    'American Indian',
                    'Asian',
                    'Native Hawaiian',
                    'Hispanic',
                    'Other'
                ];
                return races;
            };
            return PatientDataService;
        }());
        PatientDataService.inject = ['$rootScope', '$q'];
        services.PatientDataService = PatientDataService;
        angular
            .module('orthosensor.services')
            .service('PatientDataService', PatientDataService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patient.data.service.js.map