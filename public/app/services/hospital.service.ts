module orthosensor.services {
    export class HospitalService {
        static inject: Array<string> = ['$q', '$rootScope', '$http'];
        private _hospitals: any;

        private _practices: any;
        private _filterHospitalId: string;
        private _filterPracticeId: string;
         private _filterHospitalName: string;
        private _filterPracticeName: string;
        private _currentFilterId: string;
        get hospitals(): any {
            return this._hospitals;
        }
        set hospitals(hospitals: any) {
            this._hospitals = hospitals;
        }

        get practices(): any {
            return this._practices;
        }
        set practices(practices: any) {
            this._practices = practices;
        }
        get filterHospitalId() {
            return this._filterHospitalId;
        }
        set filterHospitalId(filterHospitalId: string) {
            this._filterHospitalId = filterHospitalId;
            this._filterHospitalName = this.getHospitalName(filterHospitalId);
        }

        get filterPracticeId() {
            return this._filterPracticeId;
        }
        set filterPracticeId(filterPracticeId: string) {
            this._filterPracticeId = filterPracticeId;
            this._filterPracticeName = this.getPracticeName(filterPracticeId);
        }
        get currentFilterId(): string {
            return this._currentFilterId;
        }
        set currentFilterId(currentFilterId: string) {
            this._currentFilterId = currentFilterId;
        }

        get filterHospitalName() {
            return this._filterHospitalName;
        }
        get filterPracticeName() {
            return this._filterPracticeName;
        }    
        constructor(public $q: any, public $rootScope: any, $http: any) { }

        getHospitals() {
            if (this._hospitals.length > 0) {
                return _hospitals;
            }
            else {
                return this.retrieveHospitals()
                    .then(function (data) {
                        return this._hospitals = data;
                    }, function (error) {
                    });
            }
        }
        retrieveHospitals() {
            var deferred = $q.defer();
            IQ_PatientActions.LoadHospitals(function (result, event) {
                //console.log(result);
                $rootScope.$apply(() => {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getPracticeName(id: string) {
            console.log('Searching for practice: ' + id);
            for (let i = 0; i < this._practices.length; i++) {
                // console.log(this._practices[i].Id);
                if (this._practices[i].Id === id) {
                    console.log(this._practices[i].Id + ', ' + id + ' found!');
                    return this._practices[i].Name;
                }
            }
            return '';
        }
        getHospitalName(id: string) {
            console.log('Searching for hospital: ' + id);
            for (let i = 0; i < this._hospitals.length; i++) {
                if (this._hospitals[i].Id === id) {
                    console.log(this._hospitals[i].Id + ', ' + id + ' found!');
                    return this._hospitals[i].Name;
                }
            }
            return '';
        }

    }

    angular
        .module('orthosensor.services')
        .service('HospitalService', HospitalService);
}
