(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('CaseService', CaseService);
    CaseService.$inject = ['$q', '$rootScope'];
    function CaseService($q, $rootScope) {
        var currentCase = {};
        var caseEvent = {};
        var currentProcedure = {};
        var physician = {};
        var service = {
            getCurrentCaseEvent: getCurrentCaseEvent,
            setCurrentCaseEvent: setCurrentCaseEvent,
            getCurrentCase: getCurrentCase,
            setCurrentCase: setCurrentCase,
            getSurveyCompletionDates: getSurveyCompletionDates,
            getProcedure: getProcedure,
            setProcedure: setProcedure,
            getPhysician: getPhysician,
            setPhysician: setPhysician
        };
        return service;
        ////////////////
        function getCurrentCaseEvent() {
            return caseEvent;
        }
        function setCurrentCaseEvent(event) {
            caseEvent = event;
        }
        function getCurrentCase() {
            return currentCase;
        }
        function setCurrentCase(_currentCase) {
            currentCase = _currentCase;
        }
        function getProcedure() {
            return currentProcedure;
        }
        function setProcedure(_procedure) {
            currentProcedure = _procedure;
        }
        function getSurveyCompletionDates(caseId) {
            var deferred = $q.defer();
            IQ_CaseActions.GetSurveyCompletionDates(caseId, function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getPhysician() {
            return physician;
        }
        function setPhysician(_physician) {
            physician = _physician;
        }
    }
})();
//# sourceMappingURL=case.service.js.map