var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        'use strict';
        var DashboardDataService = (function () {
            function DashboardDataService($q, $rootScope, $http) {
                this.$q = $q;
                this.$rootScope = $rootScope;
                this.$http = $http;
            }
            DashboardDataService.prototype.getAggregatePromDeltasBySurgeon = function (surgeonId, startMonth, startYear, endMonth, endYear) {
                console.log(surgeonId, startMonth, startYear, endMonth, endYear);
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetAggregatePromDeltasBySurgeon(surgeonId, 1, 2016, 12, 2017, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            DashboardDataService.prototype.getAggregatePromDeltasByPractice = function (practiceId, startMonth, startYear, endMonth, endYear) {
                console.log(practiceId, startMonth, startYear, endMonth, endYear);
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetAggregatePromDeltasByPractice(practiceId, 1, 2016, 12, 2017, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            DashboardDataService.prototype.getPromDeltasBySurgeon = function (surgeonId, startMonth, startYear, endMonth, endYear) {
                console.log(surgeonId, startMonth, startYear, endMonth, endYear);
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetPromDeltasBySurgeon(surgeonId, 1, 2016, 12, 2017, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            DashboardDataService.prototype.getPromDeltasByPractice = function (practiceId, startMonth, startYear, endMonth, endYear) {
                console.log(practiceId, startMonth, startYear, endMonth, endYear);
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetPromDeltasByPractice(practiceId, 1, 2016, 12, 2017, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            DashboardDataService.prototype.getPatientsUnderBundle = function (practiceId) {
                var _this = this;
                console.log(practiceId);
                var deferred = this.$q.defer();
                if (practiceId !== null) {
                    IQ_DashboardRepository.GetPatientsUnderBundle(practiceId, function (result, event) {
                        console.log(result);
                        // this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    }, { buffer: false, escape: true, timeout: 120000 });
                }
                return deferred.promise;
            };
            DashboardDataService.prototype.getScheduledPatients = function (practiceId, daysInFuture) {
                var _this = this;
                console.log(practiceId);
                var deferred = this.$q.defer();
                if (practiceId !== null) {
                    IQ_DashboardRepository.GetScheduledPatients(practiceId, daysInFuture, function (result, event) {
                        console.log(result);
                        // this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    }, { buffer: false, escape: true, timeout: 120000 });
                }
                return deferred.promise;
            };
            return DashboardDataService;
        }());
        DashboardDataService.inject = ['$q', '$rootScope', '$http'];
        services.DashboardDataService = DashboardDataService;
        angular
            .module('orthosensor.services')
            .service('DashboardDataService', DashboardDataService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboard-data.service.js.map