(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('LogFactory', LogFactory);
    LogFactory.$inject = ['$q', '$rootScope', '$http'];
    function LogFactory($q, $rootScope, $http) {
        var service = {
            logReadsObject: logReadsObject
        };
        return service;
        ////////////////
        function logReadsObject(objectId, objectType) {
            var deferred = $q.defer();
            IQ_PatientActions.logReadsObject(objectId, objectType, function (result, event) {
                //console.log(result);
                //No need to confirm
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }
})();
