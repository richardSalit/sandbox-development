var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var PatientService = (function () {
            function PatientService($rootScope, $q, PatientDataService) {
                this.$rootScope = $rootScope;
                this.$q = $q;
                this.PatientDataService = PatientDataService;
                // this._patient = new orthosensor.domains.Patient();
                this._currentFilterId = '';
            }
            Object.defineProperty(PatientService.prototype, "patient", {
                get: function () {
                    return this._patient;
                },
                set: function (newPatient) {
                    this._patient = newPatient;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PatientService.prototype, "currentFilterId", {
                get: function () {
                    return this._currentFilterId;
                },
                //public $q: any;
                set: function (id) {
                    this._currentFilterId = id;
                },
                enumerable: true,
                configurable: true
            });
            // this should be deprecated and use direct call using object initializer
            PatientService.prototype.getPatient = function () {
                console.log(this._patient);
                return this._patient;
            };
            PatientService.prototype.setPatient = function (_patient) {
                console.log(_patient);
                this._patient = new orthosensor.domains.Patient();
                this._patient = _patient;
            };
            // this is to handle when no object has been created.
            PatientService.prototype.setPatientId = function (patientId) {
                // this.patient = new orthosensor.domains.Patient();
                var _this = this;
                // this.patient.id = patientId;
                this.PatientDataService.getPatient(patientId)
                    .then(function (data) {
                    console.log(data);
                    var sfPatient = data;
                    _this.setPatientFromSFObject(sfPatient);
                }, function (error) {
                    console.log(error);
                });
            };
            PatientService.prototype.isAnonymous = function () {
                //console.log(patient);
                //console.log(patient.Anonymous__c);
                if (this.patient.anonymous) {
                    return this.patient.anonymous;
                }
                else {
                    return false;
                }
            };
            PatientService.prototype.checkDuplicate = function (patient) {
                var deferred = this.$q.defer();
                var accountId = null;
                if (patient.anonymous) {
                    if (patient.hospitalId !== null) {
                        accountId = patient.hospitalId;
                    }
                    else if (patient.practice !== undefined && patient.practice.length > 0) {
                        accountId = patient.practice;
                    }
                    else if (patient.hospital.length > 0) {
                        accountId = patient.hospital;
                    }
                    IQ_PatientActions.CheckDuplicateAnonymous(accountId, patient.label, patient.birthYear, function (result, event) {
                        //console.log(result);
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                else {
                    var dateofBirthString = moment.utc(patient.dateofBirthString).format('MM/DD/YYYY');
                    if (patient.hospitalId !== null) {
                        accountId = patient.hospitalId;
                    }
                    else if (patient.practice !== undefined && patient.practice.length > 0) {
                        accountId = patient.practice;
                    }
                    else if (patient.hospital.length > 0) {
                        accountId = patient.hospital;
                    }
                    /// TO DO: add email to Apex and call
                    IQ_PatientActions.CheckDuplicate(accountId, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateofBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, function (result, event) {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                return deferred.promise;
            };
            PatientService.prototype.checkAnonymous = function (account) {
                var deferred = this.$q.defer();
                IQ_PatientActions.checkAnonymous(account, function (result, event) {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                return deferred.promise;
            };
            PatientService.prototype.getSFObject = function () {
                console.log(this._patient);
                var sfPatient = new orthosensor.domains.sfPatient();
                sfPatient.Id = this._patient.id;
                sfPatient.Anonymous__c = this._patient.anonymous;
                sfPatient.Anonymous_Label__c = this._patient.anonymousLabel;
                sfPatient.Hl7__c = this._patient.hl7;
                sfPatient.HospitalId__c = this._patient.hospitalId;
                sfPatient.hospital = this._patient.hospital;
                sfPatient.Last_Name__c = this._patient.lastName;
                sfPatient.First_Name__c = this._patient.firstName;
                sfPatient.practice = this._patient.practice;
                // sfPatient.Anonymous_Label__c = this._patient.label;
                sfPatient.Medical_Record_Number__c = this.patient.medicalRecordNumber;
                sfPatient.Email__c = this._patient.email;
                sfPatient.Date_Of_Birth__c = this._patient.dateofBirth;
                sfPatient.Anonymous_Year_Of_Birth__c = this._patient.birthYear;
                sfPatient.Gender__c = this._patient.gender;
                sfPatient.Social_Security_Number__c = this._patient.socialSecurityNumber;
                sfPatient.Source_Record_Id__c = this._patient.patientNumber;
                sfPatient.Account_Number__c = this._patient.accountNumber;
                sfPatient.Race__c = this._patient.race;
                sfPatient.Language__c = this._patient.language;
                return sfPatient;
            };
            PatientService.prototype.setPatientFromSFObject = function (patient) {
                console.log(patient);
                if (!this._patient) {
                    this._patient = new orthosensor.domains.Patient();
                }
                this._patient.id = patient.Id ? patient.Id : null;
                this._patient.anonymous = patient.Anonymous__c ? patient.Anonymous__c : null;
                this._patient.hl7 = patient.Hl7__c ? patient.Hl7__c : null;
                this._patient.hospitalId = patient.Hospital__r.ParentId ? patient.Hospital__r.ParentId : patient.Hospital__c;
                this._patient.hospital = patient.hospital ? patient.hospital : null;
                this._patient.lastName = patient.Last_Name__c ? patient.Last_Name__c : null;
                this._patient.firstName = patient.First_Name__c ? patient.First_Name__c : null;
                this._patient.practiceId = patient.Hospital__c ? patient.Hospital__c : null;
                this._patient.anonymousLabel = patient.Anonymous_Label__c ? patient.Anonymous_Label__c : null;
                this._patient.medicalRecordNumber = patient.Medical_Record_Number__c ? patient.Medical_Record_Number__c : null;
                this._patient.email = patient.Email__c ? patient.Email__c : null;
                this._patient.dateofBirth = patient.Date_Of_Birth__c ? patient.Date_Of_Birth__c : null;
                this._patient.birthYear = patient.Anonymous_Year_Of_Birth__c ? patient.Anonymous_Year_Of_Birth__c : null;
                this._patient.gender = patient.Gender__c ? patient.Gender__c : null;
                this._patient.socialSecurityNumber = patient.Social_Security_Number__c ? patient.Social_Security_Number__c : null;
                this._patient.patientNumber = patient.Source_Record_Id__c ? patient.Source_Record_Id__c : null;
                this._patient.accountNumber = patient.Account_Number__c ? patient.Account_Number__c : null;
                this._patient.race = patient.Race__c ? patient.Race__c : null;
                this._patient.language = patient.Language__c ? patient.Language__c : null;
            };
            return PatientService;
        }());
        PatientService.inject = ['$rootScope', '$q', 'PatientDataService'];
        services.PatientService = PatientService;
        angular
            .module('orthosensor.services')
            .service('PatientService', PatientService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patient.service.js.map