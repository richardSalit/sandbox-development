var app;
(function (app) {
    angular.module("app.core", [
        // Angular modules 
        "ngAnimate",
        "ngSanitize",
        "ngTouch",
        'ngMessages',
        "ui.router",
 
        // Custom modules
        "common",
        "common.services",

        // 3rd Party Modules       
        "googlechart",
        "monospaced.qrcode",
        "nvd3",
        'ui.bootstrap',
        'ui.bootstrap.datetimepicker',
        'angular-ladda',
        'angularMoment'
    ]);
})(app || (app = {}));
//# sourceMappingURL=app.core.module.js.map 
