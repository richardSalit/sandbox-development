
angular
    .module("app")
    .component("hospitalDetailHeader", {
        templateUrl: "app/components/hospital/hospitalDetailHeader.component.html",
        bindings: {

        },
        controllerAs: "vm",
        controller: function () {
            var vm = this;
            vm.name = "Fulton Orthopedic";
            vm.id = "8920349";
        }
    });
