angular
    .module("app")
    .component("hospitalDetail", {
    template: "app/hospital/hospitalDetail.component.html",
    bindings: {},
    controllerAs: 'vm',
    controller: function () {
        var vm = this;
        vm.name = "Fulton Orthopedic";
        vm.id = "8920349";
    }
});
//# sourceMappingURL=hospitalDetail.component.js.map