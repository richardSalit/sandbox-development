var orthosensor;
(function (orthosensor) {
    var admin;
    (function (admin) {
        // import { Practice } from '../domains/practice';
        // import Modal = 
        var PracticeSummaryController = (function () {
            function PracticeSummaryController($uibModal) {
                this.$uibModal = $uibModal;
                this.title = 'Practices';
                this.practices = [
                    {
                        id: '989098',
                        name: 'Practice A',
                        parentId: '',
                        parentName: '',
                    },
                    {
                        id: '989097',
                        name: 'Practice B',
                        parentId: '',
                        parentName: '',
                    },
                ];
            }
            PracticeSummaryController.prototype.open = function (practice) {
                var options = {
                    template: '<practice-detail-modal></practice-detail-modal>',
                };
                var modalInstance = this.$uibModal.open(options);
            };
            return PracticeSummaryController;
        }());
        PracticeSummaryController.$inject = ['$uibModal'];
        admin.PracticeSummaryController = PracticeSummaryController;
        angular
            .module('app')
            .controller('PracticeSummaryController', PracticeSummaryController);
    })(admin = orthosensor.admin || (orthosensor.admin = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=practiceSummary.controller.js.map