(function () {
    'use strict';
    function HospitalsController(HospitalService) {
        var vm = this;
        //vm.hospitalService = hospitalService;
        vm.title = "Hospitals";
        vm.hospitals = [];
        activate();
        function activate() {
            vm.hospitals = getHospitals();
            console.log(vm.hospitals);
        }
        function getHospitals() {
            var hospitals = [];
            HospitalService.getHospitals()
                .then(function (data) {
                console.log(data);
                hospitals = data;
            }, function (error) {
                console.log(error);
            });
            return hospitals;
        }
    }
    HospitalsController.$inject = ['HospitalService'];
    angular
        .module("orthosensor")
        .controller("HospitalsController", HospitalsController);
})();
