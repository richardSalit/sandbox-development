(function () {
    'use strict';
    angular
        .module('orthosensor')
        .controller('PatientListController', PatientListController);

    // tslint:disable-next-line:max-line-length
    PatientListController.$inject = ['$rootScope', '$state', '$filter', '$uibModal', 'PatientListFactory', 'PatientService', 'CaseDetailsFactory', 'UserService', 'HospitalService', 'CaseService', 'moment'];

    function PatientListController(
        // $scope: ng.IScope,
        $rootScope: ng.IRootScopeService,
        $state: ng.ui.IStateService,
        $filter: any,
        $uibModal: ng.ui.bootstrap.IModalService,
        PatientListFactory: any,
        PatientService: orthosensor.services.PatientService,
        CaseDetailsFactory: any,
        UserService: orthosensor.services.UserService,
        HospitalService: orthosensor.services.HospitalService,
        CaseService: any,
        moment: any) {
        //variables
        let ctrl = this;
        ctrl.patientSearchValue = '';
        ctrl.title = 'Patients';
        ctrl.activeTab = 'existing-patients';
        ctrl.surgeonFilters = [];
        ctrl.sourceFilter = 'Existing';
        ctrl.filterOpen = false;
        ctrl.filterDrillDown = false;
        ctrl.procedureDateFilter = false;
        ctrl.currentHospitalName = '';
        ctrl.currentPracticeName = '';
        ctrl.filterHospitalId = '';
        ctrl.hospitals = [];
        ctrl.practices = [];

        //methods
        ctrl.addNewPatient = addNewPatient;
        ctrl.editPatient = editPatient;
        ctrl.searchPatients = searchPatients;
        ctrl.openHospitalFilter = openHospitalFilter;
        ctrl.currentHospital = currentHospital;
        ctrl.procedureDateFilterValue = null;
        ctrl.canChangeHospitalPracticeView = false;
        ctrl.addPatientCase = addPatientCase;
        ctrl.patientDetails = patientDetails;
        ctrl.caseDetails = caseDetails;
        ctrl.filterClicked = filterClicked;
        ctrl.toggleFilter = toggleFilter;
        ctrl.closeFilter = closeFilter;
        ctrl.drillDownFilter = drillDownFilter;
        ctrl.drillUpFilter = drillUpFilter;
        ctrl.removeSurgeonFilter = removeSurgeonFilter;
        ctrl.removeDateFilter = removeDateFilter;
        ctrl.searchPatientSelected = searchPatientSelected;
        ctrl.resetSearch = resetSearch;
        ctrl.searchHL7PatientSelected = searchHL7PatientSelected;


        activate();

        //add more start up to this as we refactor        
        function activate() {
            getUserRights();
            ctrl.hospitals = HospitalService.hospitals;
            ctrl.practices = HospitalService.practices;
            // getHospitalPracticeSettings();
            console.log('Current filter:' + HospitalService.currentFilterId);
            if (HospitalService.currentFilterId === '' || HospitalService.currentFilterId === null) {
                console.log('no default hospital');
                setUserDefaultHospital();
                // console.log(userDefaultHospital.Id);
                // console.log(UserService.user);
                // console.log('User default hospital: ' + UserService.user.accountId);
                // $rootScope.filterHospitalId = $rootScope.userDefaultHospital.Id;
                // HospitalService.currentFilterId = userDefaultHospital;
            } else {
                console.log('remembering last selected or default hospital');
                getHospitalPracticeSettings();
            }
            getHospitalPracticeSettings();
            consoleLogHospitalSettings();
            getPatients();
            getHL7Patients();
            loadSurgeons();
            if (currentHospital() !== 'All Hospitals') {
                getHospitalPractices();
            }
            console.log('got current');

            // ctrl.currentHospital = currentHospital();
            // getHospitalPractices();

        }

        function consoleLogHospitalSettings() {
            console.log(HospitalService.filterHospitalId);
            console.log(HospitalService.filterPracticeId);
            console.log(ctrl.currentHospitalName);
            console.log(ctrl.currentPracticeName);
            console.log(HospitalService.currentFilterId);
        }

        function getUserRights() {
            ctrl.isUserAdmin = UserService.isAdmin;
            // if user is not an admin, see if there are practices for their hospital
            if (!UserService.isAdmin) {
                ctrl.canChangeHospitalPracticeView = userHasPractices();

            } else {
                ctrl.canChangeHospitalPracticeView = true;
            }
        }

        function setUserDefaultHospital(): void {
            if (UserService.userType !== "System Administrator") {
                HospitalService.filterHospitalId = UserService.user.hospitalId; HospitalService.filterPracticeId = UserService.user.accountId;
                HospitalService.currentFilterId = UserService.user.accountId;
            } else {
                //  no default for admin!!
                return 'All Hospitals';
            }
        }

        function getPatients() {
            // console.log($rootScope.filterHospitalId);
            // if ($rootScope.filterHospitalId === undefined) {
            //     $rootScope.filterHospitalId = null;
            // }
            if (HospitalService.currentFilterId === undefined) {
                console.log('undefining');
                HospitalService.currentFilterId = null;
            }
            console.log('getting patients - filter: ' + HospitalService.currentFilterId);
            PatientListFactory.getPatients(HospitalService.currentFilterId)
                .then((data: any) => {
                    ctrl.patients = data;
                    ctrl.filteredPatients = data;
                    // console.log(ctrl.filteredPatients);
                }, (error: any) => {
                });
        }

        function getHL7Patients() {
            console.log(HospitalService.currentFilterId);
            if (HospitalService.currentFilterId) {
                PatientListFactory.getHL7Patients(HospitalService.currentFilterId)
                    .then((data: any) => {
                        console.log(data);
                        ctrl.hl7patients = data;
                    }, function (error: any) {
                    });
            }
        }
        function loadSurgeons() {
            // get surgeons at start up        
            console.log(HospitalService.currentFilterId);
            // if (PatientService.currentFilterId) {
            PatientListFactory.loadSurgeons(HospitalService.currentFilterId)
                .then((data: any) => {
                    console.log(data);
                    ctrl.surgeons = data;
                }, function (error: any) {
                    console.log(error);
                });
            // }
        }

        function getHospitalPractices() {
            if (PatientService.currentFilterId) {
                PatientListFactory.getHospitalPractices(HospitalService.currentFilterId)
                    .then((data: any) => {
                        // console.log(data);
                        ctrl.hospitalPractices = data;
                        // userHasPractices();
                        if (!ctrl.isUserAdmin) {
                            ctrl.canChangeHospitalPracticeView = userHasPractices();

                        } else {
                            ctrl.canChangeHospitalPracticeView = true;
                        }
                    }, (error: any) => {
                        console.log(error);
                    });
            }
        }

        function userHasPractices() {
            if (ctrl.hospitalPractices) {
                // console.log('should be true if practices exist' 
                return (ctrl.hospitalPractices.length > 0);
            } else {
                return false;
            }
        }

        function openHospitalFilter() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/components/hospitalSelector/hospitalSelector.component.html',
                controller: 'HospitalSelector',
                controllerAs: '$ctrl',
                size: 'small'
            });

            modalInstance.result
                .then(() => {
                    getHospitalPracticeSettings();
                    getPatients();
                    loadSurgeons();

                }, () => {
                    console.log('Modal dismissed at: ' + new Date());
                });
        }
        function getHospitalPracticeSettings() {
            ctrl.currentHospitalId = HospitalService.filterHospitalId;
            ctrl.currentPracticeId = HospitalService.filterPracticeId;
            ctrl.currentHospitalName = HospitalService.filterHospitalName;
            ctrl.currentPracticeName = HospitalService.filterPracticeName;
            console.log(HospitalService.currentFilterId);
        }

        function currentHospital(): string {
            let hospitalName = '';
            // console.log($rootScope.currentUser);
            // console.log(UserService.user);
            // console.log(UserService.user.userType);
            //console.log(
            if (UserService.user.userType !== "System Administrator") {
                setHospitalNameForContact();
            } else {
                setHospitalNameForNonContact();
            }
            return ctrl.currentHospitalName;
        }

        function setHospitalNameForContact(): void {
            let hospitalName = '';
            let contact = $rootScope.currentUser.Contact;
            // console.log(contact);
            if ('ParentId' in contact.Account) {
                if (contact.Account.Parent !== null && contact.Account.Parent.Name !== null) {
                    ctrl.currentHospitalName = contact.Account.Parent.Name;
                    ctrl.currentPracticeName = contact.Account.Name;
                } else {
                    ctrl.currentHospitalName = contact.Account.Name;
                }
            } else {
                ctrl.currentHospitalName = $rootScope.currentUser.Contact.Account.Name;
            }
            // return ctrl.currentHospitalName;
        }

        function setHospitalNameForNonContact() {
            consoleLogHospitalSettings();
            let hospitalName = '';
            if (PatientService.currentFilterId == null) {
                ctrl.currentHospitalName = 'All Hospitals';
            } else {
                if (PatientService.currentFilterId !== '') {
                    ctrl.currentHospitalName = HospitalService.filterHospitalName;
                    ctrl.currentPracticeName = HospitalService.filterPracticeName;
                }
                else {
                    console.log('no filter set!!!');
                    //$rootScope.hospital.Id === getHospitalName($rootScope.filterHospitalId);

                }
            }
            // return hospitalName;
        }

        // ctrl.dateOptions = {
        //     formatYear: 'yy',
        //     startingDay: 1,
        //     showWeeks: false
        // };

        function filterClicked(filterType, filter, evt) {
            let $element = $(evt.currentTarget);
            if (filterType === 'surgeon') {
                let index = ctrl.surgeonFilters.indexOf(filter);
                if (index > -1) {
                    ctrl.surgeonFilters.splice(index, 1);
                }
                else {
                    ctrl.surgeonFilters.push(filter);
                }
                filterPatients();
            }
            else if (filterType === 'source') {
                if (ctrl.sourceFilter !== filter) {
                    ctrl.sourceFilter = filter;
                    filterPatients();
                }
            }
            else if (filterType === 'date') {
                evt.preventDefault();
                evt.stopPropagation();
                ctrl.procedureDateFilter = true;
            }
        };

        // ctrl.$watch('procedureDateFilterValue', function (newValue, oldValue) {
        //     filterPatients();
        // });

        function toggleFilter() {
            if (ctrl.filterOpen) {
                ctrl.filterOpen = false;
                ctrl.filterDrillDown = false;
            }
            else {
                ctrl.filterOpen = true;
            }
        };
        function closeFilter() {
            ctrl.filterOpen = false;
            ctrl.filterDrillDown = false;
        };
        function drillDownFilter() {
            ctrl.filterDrillDown = true;
        };
        function drillUpFilter() {
            ctrl.filterDrillDown = false;
        };
        function removeSurgeonFilter(filter) {
            var index = ctrl.surgeonFilters.indexOf(filter);
            if (index > -1) {
                ctrl.surgeonFilters.splice(index, 1);
            }
            filterPatients();
        };
        function removeDateFilter() {
            ctrl.procedureDateFilterValue = undefined;
            ctrl.procedureDateFilter = false;
            filterPatients();
        };
        function filterPatients() {
            //console.log('Calling filter function');
            var source = [];
            if (ctrl.sourceFilter == 'Existing') {
                source = ctrl.patients;
            }
            else if (ctrl.sourceFilter == 'Scheduled') {
                source = ctrl.hl7patients;
            }
            if (ctrl.surgeonFilters.length > 0) {
                ctrl.filteredPatients = $filter('filter')(source, function (p) {
                    for (var i = 0; i < ctrl.surgeonFilters.length; i++) {
                        if (p.Cases__r) {
                            for (var c = 0; c < p.Cases__r.length; c++) {
                                if (p.Cases__r[c].Physician__c === ctrl.surgeonFilters[i].Surgeon__r.AccountId) {
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                });
                source = ctrl.filteredPatients;
            }
            else {
                ctrl.filteredPatients = source;
            }
            if (ctrl.procedureDateFilterValue) {
                ctrl.filteredPatients = $filter('filter')(source, function (p) {
                    if (p.Cases__r) {
                        var filterDateValue = moment(ctrl.procedureDateFilterValue);
                        var filterDate = new Date(filterDateValue);
                        filterDate = new Date(filterDate.getFullYear(), filterDate.getMonth(), filterDate.getDate());
                        var filterTime = filterDate.getTime();
                        for (var c = 0; c < p.Cases__r.length; c++) {
                            var procedureDateVale = p.Cases__r[c].Procedure_Date__c;
                            var procedureDate = new Date(procedureDateVale);
                            procedureDate = new Date(procedureDate.getFullYear(), procedureDate.getMonth(), procedureDate.getDate());
                            //procedureDate.setMinutes(procedureDate.getTimezoneOffset());
                            var procedureTime = procedureDate.getTime();
                            //console.log(procedureTime);
                            if (filterTime === procedureTime) {
                                return true;
                            }
                        }
                    }
                    return false;
                });
                source = ctrl.filteredPatients;
                console.log(source);
            }
            else {
                ctrl.filteredPatients = source;
            }
        }

        function addNewPatient() {
            PatientService.patient = null;
            $state.go('addPatient');
            // $uibModal.open({
            //     templateUrl: 'app/components/addPatient/addPatient.modal.html',
            //     controller: 'AddPatient',
            //     controllerAs: '$ctrl',
            //     size: 'large'
            // });
        }
        function addPatientCase(patient) {
            console.log(patient);
            PatientService.setPatientFromSFObject(patient);
            $state.go('addPatientCase');
            // $uibModal.open({
            //     //template: '<add-patient-case patient=patient></add-patient-case>',
            //     templateUrl: 'app/components/addPatientCase/addPatientCase.modal.html',
            //     controller: 'AddPatientCase',
            //     controllerAs: '$ctrl',
            //     size: 'small'
            // });
        }

        function editPatient(patient) {
            $uibModal.open({
                templateUrl: 'app/components/editPatient/editPatient.component.html',
                controller: 'editPatientController',
                size: 'large'
            });
        }

        function patientDetails(patient) {
            console.log(patient);
            PatientService.setPatientFromSFObject(patient);
            if (patient.Cases__r) {
                let physician = { name: patient.Cases__r[0].Physician__r.Name };
                CaseService.setPhysician(physician);
            }    
            $state.go('patientDetails');
        }

        function caseDetails(patient, patientCase) {
            PatientService.setPatientFromSFObject(patient);
            if (patient.Cases__r) {
                let physician = { name: patient.Cases__r[0].Physician__r.Name };
                CaseService.setPhysician(physician);
            }    

            $rootScope.case = patientCase;

            CaseDetailsFactory.LoadCaseEvents()
                .then((data: any) => {
                    console.log(data);
                    let events = data;
                    for (let i = 0; i < events.length; i++) {
                        if (events[i].Event_Type_Name__c === 'Procedure') {
                            CaseDetailsFactory.setEvent(events[i]);
                        }
                    }
                    $state.go('caseDetails');
                });
        }
        // ctrl.patientSearchValue = '';
        // ctrl.activeTab = 'existing-patients';
        function searchPatients() {
            if (ctrl.patientSearchValue.length > 2) {
                let hospitalId = HospitalService.currentFilterId;
                console.log(hospitalId);
                PatientListFactory.SearchPatientList(hospitalId, ctrl.patientSearchValue).then(function (data) {
                    console.log(data);
                    ctrl.searchPatientResults = data;
                }, function (error) {
                });
                PatientListFactory.SearchHL7PatientList(hospitalId, ctrl.patientSearchValue).then((data) => {
                    console.log(data);
                    ctrl.searchHL7PatientResults = data;
                }, (error) => {
                });
            }
        }
        function searchPatientSelected(result: any) {
            //$rootScope.patient = result;
            console.log(result);
            PatientService.setPatientFromSFObject(result);
            $state.go('patientDetails');
        };
        function resetSearch() {
            ctrl.patientSearchValue = '';
        };
        function searchHL7PatientSelected(result: any) {
            PatientService.patient = initPatientRecord(result);
            console.log(PatientService);
            $state.go('addPatient');
        }
        function initPatientRecord(result: any): orthosensor.domains.Patient {
            let patient: orthosensor.domains.Patient = new orthosensor.domains.Patient();

            patient.hl7 = true;

            // $ctrl.patient.hospital = result.SFDC_Account__c; //For Internal Users only - read-only
            patient.practice = ''; //For Internal Users only - will not render
            //patient.hospital.id = result.SFDC_Account__c;
            patient.hospitalId = result.SFDC_Account__c ? result.SFDC_Account__c : null;
            patient.firstName = result.Patient_First_Name__c ? result.Patient_First_Name__c : null;
            patient.lastName = result.Patient_Last_Name__c ? result.Patient_Last_Name__c : null;
            patient.gender = result.Patient_Gender__c ? result.Patient_Gender__c : null;
            patient.dateofBirthString = result.Patient_Date_Of_Birth__c ? result.Patient_Date_Of_Birth__c : null;

            let dob: Date = new Date(result.Patient_Date_Of_Birth__c);
            console.log(dob);
            patient.dateofBirth = dob;
            // patient.dateofBirthString = new Date(moment(dob).year(), moment(dob).month(), moment(dob).date());
            patient.dateofBirthString = new Date(dob.getUTCFullYear(), dob.getUTCMonth(), dob.getUTCDate(), dob.getUTCHours(), dob.getUTCMinutes(), dob.getUTCSeconds());
            console.log(patient.dateofBirthString);
            // $ctrl.patient.DateofBirthString = moment(result.Patient_Date_Of_Birth__c).format('MM/DD/YYYY');
            patient.race = angular.isDefined(result.Patient_Race__c) ? result.Patient_Race__c : '';
            patient.language = angular.isDefined(result.Patient_Language__c) ? result.Patient_Language__c : '';
            patient.patientNumber = '';
            patient.medicalRecordNumber = angular.isDefined(result.Medical_Record_Number__c) ? result.Medical_Record_Number__c : '';
            patient.accountNumber = angular.isDefined(result.Account_Number__c) ? result.Account_Number__c : '';
            patient.socialSecurityNumber = angular.isDefined(result.Patient_SSN__c) ? result.Patient_SSN__c : '';
            patient.email = '';
            return patient;
            // console.log('$ctrl.patient.hospital = ' + $ctrl.patient.hospital);
            // console.log('$ctrl.patient.HospitalId = ' + $ctrl.patient.HospitalId);
            // console.log('$ctrl.patient.FirstName = ' + $ctrl.patient.FirstName);
        }
    }
})();
