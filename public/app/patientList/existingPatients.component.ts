(function() {
'use strict';

    // Usage:
    // 
    // Creates:
    // 

    angular
        .module('orthosensor')
        .component('osExistingPatients', {
            
            templateUrl: 'app/patientList/existingPatients.component.html',
            controller: ExistingPatientsController,
            bindings: {
                Binding: '=',
            },
        });

    ExistingPatientsController.inject = [];
    function ExistingPatientsController() {
        var ctrl = this;
        
        ctrl.addPatientCase = addPatientCase;
        ctrl.filteredPatients = [];
        ////////////////

        ctrl.onInit = function() { };
        ctrl.onChanges = function(changesObj) { };
        ctrl.onDestory = function () { };
        
        function addPatientCase() {

        }
    }
})();