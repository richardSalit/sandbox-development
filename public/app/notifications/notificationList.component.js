var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var NotificationListController = (function () {
            function NotificationListController(UserService) {
                // this.PatientService = PatientListFactory;
                this.title = 'Notifications';
                this.user = UserService.user;
                // console.log(this.user);
                // console.log(config);
                this.$onInit();
            }
            NotificationListController.prototype.$onInit = function () {
                this.notifications = [
                    {
                        patientName: 'Fred Demarco', id: 29347, message: 'Something here'
                    },
                    {
                        patientName: 'Harry Demarco', id: 29532, message: 'Nothing here'
                    },
                    {
                        patientName: 'Sarah Fairy', id: 21237, message: 'Something there'
                    },
                ];
            };
            ;
            return NotificationListController;
        }());
        NotificationListController.$inject = ['UserService'];
        angular
            .module('orthosensor')
            .component('osNotifications', {
            controller: NotificationListController,
            controllerAs: '$ctrl',
            templateUrl: 'app/notifications/notificationList.component.html',
            bindings: {}
        });
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=notificationList.component.js.map