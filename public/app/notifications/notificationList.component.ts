module orthosensor.dashboard {

    class NotificationListController  {

        public static $inject: any[] = ['UserService'];

        public title: string;
        public user: orthosensor.domains.User;
        
        private UserService: any;
        public notifications: any[];

        constructor( UserService: any) {
            
            // this.PatientService = PatientListFactory;
            this.title = 'Notifications';
            this.user = UserService.user;
           
            // console.log(this.user);
            
            // console.log(config);
            this.$onInit();
        }

        public $onInit(): void {
            this.notifications = [
                {
                    patientName: 'Fred Demarco', id: 29347, message: 'Something here'
                },
                {
                    patientName: 'Harry Demarco', id: 29532, message: 'Nothing here'
                },
                {
                    patientName: 'Sarah Fairy', id: 21237, message: 'Something there'
                },
            ];
        };

    }

    angular
        .module('orthosensor')
        .component('osNotifications', {
            controller: NotificationListController,
            controllerAs: '$ctrl',
            templateUrl: 'app/notifications/notificationList.component.html',
            bindings: {
             
            }
        });
}


