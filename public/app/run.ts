(function () {
    angular
        .module('orthosensor')
        .run(runBlock);
    runBlock.$inject = ['$state', '$rootScope', 'InitFactory', '$window', 'UserService', 'DashboardService', 'PatientListFactory', 'HospitalService'];
    function runBlock($state, $rootScope, InitFactory, $window,
        UserService: orthosensor.services.UserService,
        DashboardService: orthosensor.services.DashboardService,
        PatientListFactory: any,
        HospitalService: orthosensor.services.HospitalService) {

        InitFactory.isUserAdmin()
            .then(function (data: any) {
                console.log(data);
                $rootScope.isUserAdmin = data;
                UserService.isAdmin = data;

            }, function (error: any) {
            });

        InitFactory.getCurrentUser()
            .then(function (data: any) {
                console.log(data);
                $rootScope.currentUser = data;
                let sfUser = data;
                let user = UserService.convertSFToObject(sfUser);
                UserService.user = user;
                //let startPage = 'dashboard';
                let startPage = 'patientList';
                let hospitalPracticeId: string = '';
                // change if we get user info!
                switch (UserService.user.userType) {
                    case 'Surgeon Partner Community User':
                        console.log('surgeon Account');
                        DashboardService.loadHospitalPracticeSurgeons(UserService.user.accountId);
                        console.log(DashboardService.surgeons);
                        
                        DashboardService.loadHospitalByPractice(UserService.user.accountId);
                        startPage = 'patientList';
                        break;

                    case 'Hospital Partner Community User':
                        hospitalPracticeId = user.accountId;
                        console.log('hospital Account');
                        DashboardService.loadHospitalPractices(hospitalPracticeId);
                        DashboardService.loadHospitalPracticeSurgeons(UserService.user.accountId);
                        DashboardService.loadHospitalByPractice(UserService.user.accountId);
                        startPage = 'dashboard';
                        break;

                    case 'Partner Community User':
                        hospitalPracticeId = user.accountId;
                        console.log('practice Account');
                        DashboardService.loadHospitalPractices(hospitalPracticeId);
                        DashboardService.loadHospitalPracticeSurgeons(UserService.user.accountId);
                        DashboardService.loadHospitalByPractice(UserService.user.accountId);
                        startPage = 'patientList';
                        break;

                    default:
                        console.log('Admin Account');
                        DashboardService.loadHospitals();
                        console.log(DashboardService.hospitals);
                        DashboardService.loadPractices(hospitalPracticeId);
                        DashboardService.loadAllSurgeons();

                }

                $state.go(startPage);

            }, function (error: any) {
            });

        PatientListFactory.loadHospitals()
            .then((data: any) => {
                // console.log(data);
                HospitalService.hospitals = data;
            }, (error) => {
                console.log(error);
            });
        PatientListFactory.loadPractices()
            .then((data: any) => {
                // console.log(data);
                HospitalService.practices = data;
            }, (error) => {
                console.log(error);
            });
        DashboardService.setSurveys();
        // this assumes that the dashboard will be eventually be the start up module
        // also, we may want to move these to a different service that would be
        // shared for all modules (e.g. DefaultService)


        //move this to a service at some point!
        $rootScope.dateformat = dateformat;
        function dateformat(milliseconds: number) {
            console.log('- using date format in run js!!! -');
            if (!milliseconds) {
                console.log('Not milliseconds');
                return '--';
            }
            else {
                var dateValue = moment.utc(milliseconds).format('LL');
                console.log(dateValue);
                return dateValue;
            }
        }
    }
})();
