(function () {
    'use strict';
    var app = angular.module('orthosensor');
    // Configure Toastr
    toastr.options.timeOut = 5000;
    //toastr.options.positionClass = 'toast-bottom-right';
    toastr.options.positionClass = 'toast-top-center';
    var events = {
        controllerActivateSuccess: 'controller.activateSuccess',
        spinnerToggle: 'spinner.toggle'
    };
    var config = {
        appErrorPrefix: '[HT Error] ',
        docTitle: 'OrthoLogIQ',
        events: events,
        //remoteServiceName: remoteServiceName,
        version: '0.1.0',
        'userType' : {
            "practice": "Partner Partner Community User",
            "hospital": "Hospital Partner Community User",
            "surgicalTech": "IQ Surgical Tech", 
            "admin": "Administrative Account"
        }    
    };
    app.constant('dateFormat', 'shortDate');
    app.value('config', config);
    app.config(['$logProvider', function ($logProvider) {
        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
    }]);
    //#region Configure the common services via commonConfig
    app.config(['commonConfigProvider', function (cfg) {
        cfg.config.controllerActivateSuccessEvent = config.events.controllerActivateSuccess;
        cfg.config.spinnerToggleEvent = config.events.spinnerToggle;
    }]);
    app.config(function ($locationProvider) {
        // $locationProvider.html5Mode({
        //     enabled: true,
        //     requireBase: false
        // });
    });
    app.config(function (laddaProvider) {
        laddaProvider.setOption({
            style: 'zoom-in',
            spinnerSize: 35,
            spinnerColor: '#ffffff'
        });
    });
    app.value('$routerRootComponent', 'orthosensor');
    //#endregion
})();
