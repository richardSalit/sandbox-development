var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var DashboardTestController = (function () {
            function DashboardTestController(DashboardService, DashboardDataService, PatientsUnderBundleService, PatientService, UserService, $state) {
                this.DashboardService = DashboardService;
                this.UserService = UserService;
                this.DashboardDataService = DashboardDataService;
                this.PatientService = PatientService;
                this.PatientsUnderBundleService = PatientsUnderBundleService;
                this.$state = $state;
                this.patientsUnderBundleTitle = 'Patients Under Bundle';
                // this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
            }
            DashboardTestController.prototype.$onInit = function () {
                var _this = this;
                var practiceId = '0017A00000M803ZQAR';
                practiceId = this.UserService.user.accountId;
                //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
                this.DashboardDataService.getScheduledPatients(practiceId)
                    .then(function (data) {
                    _this.patientsScheduled = data.length;
                    console.log(_this.patients);
                }, function (error) {
                    console.log(error);
                    return error;
                });
                this.DashboardDataService.getPatientsUnderBundle(practiceId)
                    .then(function (data) {
                    _this.patientsUnderBundleCount = data.length;
                    console.log(_this.patientsUnderBundleCount);
                }, function (error) {
                    console.log(error);
                    return error;
                });
            };
            return DashboardTestController;
        }());
        DashboardTestController.$inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state'];
        var DashboardTest = (function () {
            function DashboardTest() {
                this.bindings = {
                    practiceId: '@',
                };
                this.controller = DashboardTestController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/testPage.html';
            }
            return DashboardTest;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osDashboardTest', new DashboardTest());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=testPage.js.map