var app;
(function (app) {
    angular
        .module("orthosensor")
        .component("app", {
        controller: "ShellCtrl",
        templateUrl: "app/layout/shell.html"
    });
})(app || (app = {}));
