(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osCaseInteroperativeData', {
        //template:'htmlTemplate',
        templateUrl: 'app/components/caseInteroperativeData/caseImages.component.html',
        controller: CaseInteroperativeDataController,
        bindings: {
            event: '<'
        }
    });
    CaseInteroperativeDataController.$inject = ['$window', 'CaseDetailsFactory', 'LogFactory', 'PatientService', 'SensorService', '$confirm'];
    function CaseInteroperativeDataController($window, CaseDetailsFactory, LogFactory, PatientService, SensorService, $confirm) {
        //var $ctrl = this;
        var linkStationsIDs = [];
        this.caseEvent = {};
        this.showImages = false;
        this.sensorDataLines = [];
        ////////////////
        this.$onInit = function () {
            this.patient = PatientService.getPatient();
            //console.log(caseEvent);
            this.event = CaseDetailsFactory.getEvent();
            //console.log(this.caseEvent);
            console.log(this.event);
            if (this.event.Id !== null) {
                this.clinicalData = this.event.Clinical_Data__r[0];
                this.showImages = this.event.OS_Device_Events__r.length > 0;
                loadSensorDataLines(this.event.Id);
            }
            else {
                console.log("Event not yet defined");
            }
        };
        this.$onChanges = function (changesObj) { };
        this.$onDestory = function () { };
        // get list of link stations for the hospital and then call the routines to retrive the data       
        function getLinkStationData() {
            var hospitalId = this.patient.Hospital__c;
            console.log(hospitalId);
            if (hospitalId !== null) {
                var l = Ladda.create(document.querySelector('.retrieve-btn'));
                l.start();
                CaseDetailsFactory.LoadLinkStationsIDs(hospitalId)
                    .then(function (data) {
                    //console.log(data);
                    linkStationsIDs = data;
                    loadData();
                }, function (error) {
                    console.log(error);
                });
            }
        }
        // start retrieving linked images from sensors. CheckSoftwareVersion acutally retrieves data once
        // it determines which version of Verasense is being used. It then retrieves the images
        function loadData() {
            if (linkStationsIDs.length > 0) {
                for (var i = 0; i < linkStationsIDs.length; i++) {
                    var procedureId = this.event.Id;
                    console.log(event.Id);
                    var serialNo = linkStationsIDs[i].Serial_Number__c;
                    console.log(serialNo);
                    var laterality = this.clinicalData.Laterality__c;
                    console.log(laterality);
                    if (serialNo !== null) {
                        CaseDetailsFactory.CheckSoftwareVersion(procedureId, serialNo, laterality)
                            .then(function (data) {
                            console.log(data);
                            getImages(data);
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
            }
            else {
                $window.alert('No images available');
            }
        }
        // images are retrieved and then added as attachments in Force.com
        function getImages(linkStationsID) {
            //console.log(linkStationsID);
            var deviceEvents = event.OS_Device_Events__r;
            console.log(deviceEvents);
            if (deviceEvents !== null) {
                if (deviceEvents.length > 0) {
                    for (var i = 0; i < deviceEvents.length; i++) {
                        //console.log(deviceEvents[i]);
                        CaseDetailsFactory.GetImageList(event.Id, deviceEvents[i].OS_Device__r.Id, linkStationsID)
                            .then(function (data) {
                            //console.log(data);
                            var result = JSON.parse(data.replace(/&quot;/g, '"'));
                            console.log(result);
                            if ((typeof result.data !== "undefined") && (result.status !== 'error')) {
                                var imageURLArray = result.data.images;
                                var imageURLArrayCaptures = [];
                                var recId = this.procedure.Id;
                                var ii = 0;
                                for (var k = 0; k < imageURLArray.length; k++) {
                                    //console.log('imageURLArray[i].indexOf(45) = ' + imageURLArray[k].indexOf('45.'));
                                    //console.log('imageURLArray.length = ' + imageURLArray.length);
                                    if (imageURLArray[k].indexOf('2.') > -1 || imageURLArray[k].indexOf('10.') > -1 || imageURLArray[k].indexOf('45.') > -1 || imageURLArray[k].indexOf('90.') > -1) {
                                        imageURLArrayCaptures.push(imageURLArray[k]);
                                    }
                                }
                                for (var j = 0; j < imageURLArrayCaptures.length; j++) {
                                    //console.log(imageURLArrayCaptures[j]);
                                    createImageAttachment(recId, imageURLArrayCaptures[j]);
                                    ii++;
                                }
                                console.log(imageURLArrayCaptures.length);
                                console.log(ii);
                                if (ii === imageURLArrayCaptures.length) {
                                    //console.log('about to match');
                                    matchSDwithAttachment(recId);
                                }
                            }
                            else {
                                console.log(event);
                            }
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
                else {
                    console.log('Nothing to do hide the spinner');
                }
            }
            else {
                console.log('Nothing to do hide the spinner');
            }
            Ladda.stopAll();
        }
        function createImageAttachment(recordId, imageURL) {
            CaseDetailsFactory.SaveImageToRecord(recordId, imageURL)
                .then(function (data) {
                //console.log(data);
                //console.log('Image was saved');
            }, function (error) {
                console.log('No Images Captured.');
                console.log(error);
            });
        }
        function matchSDwithAttachment(recordId) {
            //Show spinner
            CaseDetailsFactory.MatchSensorDataAndAttachment(recordId)
                .then(function (data) {
                //console.log(data);
                //console.log('Sensor Data Match Complete');
                loadSensorDataLines(recordId);
            }, function (error) {
                //console.log('No Images Captured.');
                console.log(error);
            });
        }
        function loadSensorDataLines(procedureId) {
            console.log('Procedure Id: ' + procedureId);
            CaseDetailsFactory.loadSensorDataLines(procedureId)
                .then(function (data) {
                console.log(data);
                this.sensorDataLines = data;
                if (this.sensorDataLines.length > 0) {
                    if (this.sensorDataLines[0].AttachmentID === "") {
                        //if attachment ids are not found
                        //console.log("Getting attachments again!");
                        console.log(this.sensorDataLines);
                    }
                }
                //console.log(this.sensorDataLines);
                Ladda.stopAll();
            }, function (error) {
                console.log('No Images Captured.');
                console.log(error);
            });
        }
        function deleteSensorImage(line) {
            $confirm({ text: 'Are you sure you want to delete this image?', title: 'Delete Sensor Image' })
                .then(function () {
                CaseDetailsFactory.DeleteSensorLine(line, this.event.Id).then(function (data) {
                    //console.log(data);
                    loadSensorDataLines(this.event.Id);
                }, function (error) {
                    console.log(error);
                });
            });
        }
    }
})();
