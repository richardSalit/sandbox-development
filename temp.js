var xyz;
(function (xyz) {
    var dashboard;
    (function (dashboard) {
        var SomethingPhaseCountController = (function () {
            ///// /* @ngInject */
            function SomethingPhaseCountController() {
                this.title = 'Patient Phase Count';
                console.log(this.chartType);
                this.$onInit();
            }
            SomethingPhaseCountController.prototype.$onInit = function () {
                console.log(this);
            };
            ;
            return SomethingPhaseCountController;
        }());
        SomethingPhaseCountController.$inject = [];
        var SomethingPhaseCount = (function () {
            function SomethingPhaseCount() {
                this.bindings = {
                    chartType: '@'
                };
                this.controller = SomethingPhaseCountController;
                this.templateUrl = 'app/dashboard/patientPhaseCount/patientPhaseCount.component.html';
            }
            return SomethingPhaseCount;
        }());
    })(dashboard = xyz.dashboard || (xyz.dashboard = {}));
})(xyz || (xyz = {}));
//# sourceMappingURL=temp.js.map